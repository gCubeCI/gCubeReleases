![logo](img/gcube_logo.png)

# gCube Versions in 2023

## 5.16.0 
* Released on 21-09-2023
* [Build Configuration](closed/5.16.0/gcube-5.16.0.yaml)
* [Build Report](closed/5.16.0/build_commits.1593.csv)
* [Tag Report](closed/5.16.0/tags.214.csv)
* [Release Notes](closed/5.16.0/release_notes.708.md)


## 5.15.5 
* Released on 01-08-2023
* [Build Configuration](closed/5.15.5/gcube-5.15.5.yaml)
* [Build Report](closed/5.15.5/build_commits.1550.csv)
* [Tag Report](closed/5.15.5/tags.212.csv)
* [Release Notes](closed/5.15.5/release_notes.680.md)


## 5.15.4 
* Released on 26-06-2023
* [Build Configuration](closed/5.15.4/gcube-5.15.4.yaml)
* [Build Report](closed/5.15.4/build_commits.1535.csv)
* [Tag Report](closed/5.15.4/tags.210.csv)
* [Release Notes](closed/5.15.4/release_notes.665.md)


## 5.15.3 
* Released on 31-05-2023
* [Build Configuration](closed/5.15.3/gcube-5.15.3.yaml)
* [Build Report](closed/5.15.3/build_commits.1491.csv)
* [Tag Report](closed/5.15.3/tags.208.csv)
* [Release Notes](closed/5.15.3/release_notes.622.md)


## 5.15.2 
* Released on 26-04-2023
* [Build Configuration](closed/5.15.2/gcube-5.15.2.yaml)
* [Build Report](closed/5.15.2/build_commits.1444.csv)
* [Tag Report](closed/5.15.2/tags.206.csv)
* [Release Notes](closed/5.15.2/release_notes.555.md)


## 5.15.1 
* Released on 03-04-2023
* [Build Configuration](closed/5.15.1/gcube-5.15.1.yaml)
* [Build Report](closed/5.15.1/build_commits.1433.csv)
* [Tag Report](closed/5.15.1/tags.204.csv)
* [Release Notes](closed/5.15.1/release_notes.545.md)


## 5.15.0 
* Released on 21-03-2023
* [Build Configuration](closed/5.15.0/gcube-5.15.0.yaml)
* [Build Report](closed/5.15.0/build_commits.1415.csv)
* [Tag Report](closed/5.15.0/tags.202.csv)
* [Release Notes](closed/5.15.0/release_notes.536.md)


## 5.14.4 
* Released on 13-02-2023
* [Build Configuration](closed/5.14.4/gcube-5.14.4.yaml)
* [Build Report](closed/5.14.4/build_commits.1382.csv)
* [Tag Report](closed/5.14.4/tags.200.csv)
* [Release Notes](closed/5.14.4/release_notes.519.md)


## 5.14.3 
* Released on 02-02-2023
* [Build Configuration](closed/5.14.3/gcube-5.14.3.yaml)
* [Build Report](closed/5.14.3/build_commits.1375.csv)
* [Tag Report](closed/5.14.3/tags.198.csv)
* [Release Notes](closed/5.14.3/release_notes.512.md)


## 5.14.2 
* Released on 25-01-2023
* [Build Configuration](closed/5.14.2/gcube-5.14.2.yaml)
* [Build Report](closed/5.14.2/build_commits.1368.csv)
* [Tag Report](closed/5.14.2/tags.196.csv)
* [Release Notes](closed/5.14.2/release_notes.507.md)


## 5.14.1 
* Released on 17-01-2023
* [Build Configuration](closed/5.14.1/gcube-5.14.1.yaml)
* [Build Report](closed/5.14.1/build_commits.1354.csv)
* [Tag Report](closed/5.14.1/tags.194.csv)
* [Release Notes](closed/5.14.1/release_notes.495.md)
