![logo](img/gcube_logo.png)

**Configurations, reports and chronicles of gCube Releases.**

The following documentation is available for each release: 
* Released on_: the date in which the release was made publicly available
* _Build Configuration_: the configuration used to build the release
* _Build Report_: this report lists all the released components with their version, location, and commit ID on the source control system.
* _Tag Report_: this report is the fingerprint of the release with the  commit IDs and tags
* _Release Notes_: the aggregated release notes reporting all the major new features and fixed issues for each released component

## Versions

## 6.0.1

* Released on 06-03-2025
* [Build Configuration](closed/6.0.1/gcube-6.0.1.yaml)
* [Build Report](closed/6.0.1/build_commits.1809.csv)
* [Tag Report](closed/6.0.1/tags.235.csv)
* [Release Notes](closed/6.0.1/release_notes.811.md)

## 6.0.0

* Released on 03-12-2024
* [Build Configuration](closed/6.0.0/gcube-6.0.0.yaml)
* [Build Report](closed/6.0.0/build_commits.1796.csv)
* [Tag Report](closed/6.0.0/tags.233.csv)
* [Release Notes](closed/6.0.0/release_notes.799.md)

## 5.17.3

* Released on 14-10-2024
* [Build Configuration](closed/5.17.3/gcube-5.17.3.yaml)
* [Build Report](closed/5.17.3/build_commits.1693.csv)
* [Tag Report](closed/5.17.3/tags.225.csv)
* [Release Notes](closed/5.17.3/release_notes.754.md)

## 5.17.2

* Released on 23-07-2024
* [Build Configuration](closed/5.17.2/gcube-5.17.2.yaml)
* [Build Report](closed/5.17.2/build_commits.1685.csv)
* [Tag Report](closed/5.17.2/tags.223.csv)
* [Release Notes](closed/5.17.2/release_notes.746.md)

## 5.17.1

* Released on 20-06-2024
* [Build Configuration](closed/5.17.1/gcube-5.17.1.yaml)
* [Build Report](closed/5.17.1/build_commits.1664.csv)
* [Tag Report](closed/5.17.1/tags.221.csv)
* [Release Notes](closed/5.17.1/release_notes.740.md)

## 5.17.0

* Released on 10-05-2024
* [Build Configuration](closed/5.17.0/gcube-5.17.0.yaml)
* [Build Report](closed/5.17.0/build_commits.1644.csv)
* [Tag Report](closed/5.17.0/tags.218.csv)
* [Release Notes](closed/5.17.0/release_notes.733.md)

## 5.16.1

* Released on 20-03-2024
* [Build Configuration](closed/5.16.1/gcube-5.16.1.yaml)
* [Build Report](closed/5.16.1/build_commits.1629.csv)
* [Tag Report](closed/5.16.1/tags.216.csv)
* [Release Notes](closed/5.16.1/release_notes.724.md)

## Previous Versions

## [2023](2023.md)

## [2022](2022.md)

## [2021](2021.md)

## [2020](2020.md)

## [2019](2019.md)
