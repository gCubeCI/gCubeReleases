# Release Notes for gCube 4.23.0

---
## maven-parent 1.1.0
Tagged commit: _[943956db98e143ce0be77a5236b54c417fee28e3](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/943956db98e143ce0be77a5236b54c417fee28e3)_

 2020-01-27

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## common-authorization 2.2.1
Tagged commit: _[1f0f6096818337a191b838d990b127c2fafa78ec](https://code-repo.d4science.org/gCubeSystem/common-authorization/commit/1f0f6096818337a191b838d990b127c2fafa78ec)_

Tags 2.2.1 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/common-authorization
## uri-resolver-manager 1.4.1
Tagged commit: _[c2b787b19c3a5090a47bcb856d1269b42f263027](https://code-repo.d4science.org/gCubeSystem/uri-resolver-manager/commit/c2b787b19c3a5090a47bcb856d1269b42f263027)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/uri-resolver-manager
## geonetwork 3.4.3
Tagged commit: _[ade2d8440ff0b118579d2d10432a4e2840ef49f1](https://code-repo.d4science.org/gCubeSystem/geonetwork.git/commit/ade2d8440ff0b118579d2d10432a4e2840ef49f1)_

Tags 3.4.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/geonetwork.git
## gis-viewer 4.5.0
Tagged commit: _[820f7c5d27c6354b21a7de5c03a94cd38c22a1ae](https://code-repo.d4science.org/gCubeSystem/gis-viewer/commit/820f7c5d27c6354b21a7de5c03a94cd38c22a1ae)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gis-viewer
## dataminer 1.6.0
Tagged commit: _[0a1de08b27eec3a6493d6136e15dfc530824409d](https://code-repo.d4science.org/gCubeSystem/dataminer/commit/0a1de08b27eec3a6493d6136e15dfc530824409d)_

 2020-05-12

### Fixes

- Added storagehub retry in InputsManager class, getLocalFile method [#19253]




## storagehub-client-wrapper 0.7.1
Tagged commit: _[eb703801a89d3e2c0b3dd6a29b46ad1b807739a8](https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper/commit/eb703801a89d3e2c0b3dd6a29b46ad1b807739a8)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper
## github-connector 1.5.3
Tagged commit: _[55ffdb3c62d81111bd0b5b342d90d43219e24dd0](https://code-repo.d4science.org/gCubeSystem/github-connector/commit/55ffdb3c62d81111bd0b5b342d90d43219e24dd0)_

 2020-05-20

### Fixes

- Updated library org.eclipse.egit.github.core to open-jdk8 and TLSv1.2 [#18318]




## workspace-uploader 2.0.5
Tagged commit: _[d5c979fe260d21dc6fc7941de7598163de9f2898](https://code-repo.d4science.org/gCubeSystem/workspace-uploader/commit/d5c979fe260d21dc6fc7941de7598163de9f2898)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/workspace-uploader
## workspace-tree-widget 6.30.0
Tagged commit: _[297226343cd67b2b697dbdf9583ff4dc84608994](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/297226343cd67b2b697dbdf9583ff4dc84608994)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget
## acegwt-widget 1.1.1
Tagged commit: _[9fea872bbb245faa9fd90bdb5fd9c249012f20a2](https://code-repo.d4science.org/gCubeSystem/acegwt-widget/commit/9fea872bbb245faa9fd90bdb5fd9c249012f20a2)_

 2020-05-20

### Fixes

- Updated GitHub client library [#18318]




## gis-viewer-app 1.8.0
Tagged commit: _[d77030043fd326549616cd701b1d0d53d8ae5ceb](https://code-repo.d4science.org/gCubeSystem/gis-viewer-app/commit/d77030043fd326549616cd701b1d0d53d8ae5ceb)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gis-viewer-app
## uri-resolver 2.3.2
Tagged commit: _[257682bed87df4be2869b3dccd5a7176f6ea149c](https://code-repo.d4science.org/gCubeSystem/uri-resolver/commit/257682bed87df4be2869b3dccd5a7176f6ea149c)_

Tags 2.3.2 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/uri-resolver
## sdi-service 1.4.1
Tagged commit: _[c71d7a7de3deae60d82f93e3fe68feab1982676f](https://code-repo.d4science.org/gCubeSystem/sdi-service/commit/c71d7a7de3deae60d82f93e3fe68feab1982676f)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/sdi-service
## gCat-Feeder-Suite 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## commons 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## collectors-plugin-framework 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## catalogue-plugin-framework 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## gCat-Feeder 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## test-commons 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## DataMinerAlgorithmsCrawler 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## gCat-Controller 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## oai-harvester 1.0.3
Tagged commit: _[52ce536f6962e735aec59e2b69c0b2d43ff53d04](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/52ce536f6962e735aec59e2b69c0b2d43ff53d04)_

Tags 1.0.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## accounting-dashboard-harvester-se-plugin 1.6.0
Tagged commit: _[fc7ef68845da61c5b085e6b34b5b168413ae2f0a](https://code-repo.d4science.org/gCubeSystem/accounting-dashboard-harvester-se-plugin/commit/fc7ef68845da61c5b085e6b34b5b168413ae2f0a)_

 2020-05-22

### Added

**Features**

- [#19047] Added core services accesses 



## gcat 1.4.3
Tagged commit: _[24797135b0f913ef51aa3794eb26f8796f1a92f3](https://code-repo.d4science.org/gCubeSystem/gcat/commit/24797135b0f913ef51aa3794eb26f8796f1a92f3)_

Tags 1.4.3 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcat
## storagehub 1.2.1
Tagged commit: _[84cec8aa76e819275f85c5c25656efccdd9656c8](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/84cec8aa76e819275f85c5c25656efccdd9656c8)_

Tags 1.2.1 / 4.23.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/storagehub
## species-discovery 3.10.0
Tagged commit: _[38ecd99ad45897c23fa9d94d9c26e86cc1fd7328](https://code-repo.d4science.org/gCubeSystem/species-discovery/commit/38ecd99ad45897c23fa9d94d9c26e86cc1fd7328)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/species-discovery
## statistical-algorithms-importer 1.14.1
Tagged commit: _[b2be52e523bc9de0b233ae5cb565c1b7aeea1271](https://code-repo.d4science.org/gCubeSystem/statistical-algorithms-importer/commit/b2be52e523bc9de0b233ae5cb565c1b7aeea1271)_

 2020-05-20

### Fixes

- Updated GitHub client library [#18318]



## workspace 6.24.0
Tagged commit: _[3708f0a6c0685cae0d6716d3efc63f442ab9e85b](https://code-repo.d4science.org/gCubeSystem/workspace/commit/3708f0a6c0685cae0d6716d3efc63f442ab9e85b)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/workspace
## performfish-analytics-portlet 1.0.0
Tagged commit: _[6a4d266aadbc9495538cab610c13cd134827a64f](https://code-repo.d4science.org/gCubeSystem/performfish-analytics-portlet/commit/6a4d266aadbc9495538cab610c13cd134827a64f)_

 2020-05-28

### Features

- Added PDF support [#17249]



## ldap-export-servlet 1.2.1
Tagged commit: _[aeb14d2fabefedddb133f6636e1c175210eee787](https://code-repo.d4science.org/gCubeSystem/ldap-export-servlet/commit/aeb14d2fabefedddb133f6636e1c175210eee787)_

 2020-05-29

### Fixes
- This release fixes a potential bug in the export of the users to LDAP. See Incident #19348


## join-vre 3.7.0
Tagged commit: _[a3ffb98c56b40135a2acae049540dd00acd3aeb1](https://code-repo.d4science.org/gCubeSystem/join-vre/commit/a3ffb98c56b40135a2acae049540dd00acd3aeb1)_

 2020-06-03

Fixes

Bug #19415, Enhance Join VRE portlet performance when many VREs are present on a gateway and the user is logged in.



## smartgears-distribution 2.5.4
Tagged commit: _[753ba120766ef33b6a4f51fab725afa0266f6a2e](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/753ba120766ef33b6a4f51fab725afa0266f6a2e)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/smartgears-distribution
## smartgears-distribution-bundle 2.5.4
Tagged commit: _[7523806a1eccfd4718afd687b8a5c8398450e5c4](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution-bundle/commit/7523806a1eccfd4718afd687b8a5c8398450e5c4)_

Missing CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/smartgears-distribution-bundle

---
*generated by the gCube-ReleaseNotes pipeline from report 709*

*last update Fri Jun 19 21:11:40 CEST 2020*
