# Release Notes for gCube 4.24.0

---
## maven-parent 1.1.0
Tagged commit: _[a3f39056723dd5b9a45acecfa3239d9e0b82a781](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/a3f39056723dd5b9a45acecfa3239d9e0b82a781)_

 [r4.23.0] 2020-01-27

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## detachedres-library 1.1.0
Tagged commit: _[eb4f094d168b53ac13333301b8c92433cdff2823](https://code-repo.d4science.org/gCubeSystem/detachedres-library/commit/eb4f094d168b53ac13333301b8c92433cdff2823)_

  - 2020-06-05

### Features

- Added the property cataloguePortletURL and the method [#19440]


## grsf-common-library 1.3.0
Tagged commit: _[8f8fd6bc74e244ed680be2db13a95edb5f23c6b9](https://code-repo.d4science.org/gCubeSystem/grsf-common-library/commit/8f8fd6bc74e244ed680be2db13a95edb5f23c6b9)_

  - 2020-06-19

### Changed

- [#19166] Added support for GRSF_pre VRE with the behaviour of GRSF Admin


## ckan-util-library 2.12.1
Tagged commit: _[ad81920ab3c84d9a625fdad937d5bcbb7d6e03cc](https://code-repo.d4science.org/gCubeSystem/ckan-util-library/commit/ad81920ab3c84d9a625fdad937d5bcbb7d6e03cc)_

  - 2020-06-18

**Bug fixes**

[#19479] Add Resource facility does not work



## ecological-engine 1.13.0
Tagged commit: _[363c79bc45ae706d3b148df151febf3e52ab9538](https://code-repo.d4science.org/gCubeSystem/ecological-engine/commit/363c79bc45ae706d3b148df151febf3e52ab9538)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]




## ecological-engine-smart-executor 1.6.3
Tagged commit: _[2e44545dfb11e1d8ba8c0cd8d99f60fcda18d6f6](https://code-repo.d4science.org/gCubeSystem/ecological-engine-smart-executor/commit/2e44545dfb11e1d8ba8c0cd8d99f60fcda18d6f6)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]




## ecological-engine-geospatial-extensions 1.5.1
Tagged commit: _[d76acf5236731b16485669f16707f3003572d35f](https://code-repo.d4science.org/gCubeSystem/ecological-engine-geospatial-extensions/commit/d76acf5236731b16485669f16707f3003572d35f)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]



## database-resource-manager 1.4.1
Tagged commit: _[417d3baec9aec9f23e703e3541d5a1eef094a51a](https://code-repo.d4science.org/gCubeSystem/database-resource-manager/commit/417d3baec9aec9f23e703e3541d5a1eef094a51a)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]


## ecological-engine-wps-extension 1.0.5
Tagged commit: _[4e7c2466717ca5022d296e0b40996fe6eca30f83](https://code-repo.d4science.org/gCubeSystem/ecological-engine-wps-extension/commit/4e7c2466717ca5022d296e0b40996fe6eca30f83)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]




## database-resource-manager-algorithm 1.5.1
Tagged commit: _[880b971c2746e530ed9b3063472339df70a1adc0](https://code-repo.d4science.org/gCubeSystem/database-resource-manager-algorithm/commit/880b971c2746e530ed9b3063472339df70a1adc0)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]


## seadatanet-connector 1.2.1
Tagged commit: _[4cf872f8884085f306d67fb84ed8b127fd865ab6](https://code-repo.d4science.org/gCubeSystem/seadatanet-connector/commit/4cf872f8884085f306d67fb84ed8b127fd865ab6)_

 - 2020-06-10

### Features

- Updated for support https protocol [#19423]




## ecological-engine-external-algorithms 1.2.1
Tagged commit: _[e62aaaa952c62d58adb736839f1d9108630f871d](https://code-repo.d4science.org/gCubeSystem/ecological-engine-external-algorithms/commit/e62aaaa952c62d58adb736839f1d9108630f871d)_

  - 2020-06-10

### Features

- Updated for support https protocol [#19423]




## uri-resolver 2.4.0
Tagged commit: _[d5565e045b20cc970f1fe7dd56cd6d4ba78e9fbc](https://code-repo.d4science.org/gCubeSystem/uri-resolver/commit/d5565e045b20cc970f1fe7dd56cd6d4ba78e9fbc)_

 [r4-24-0]- 2020-06-18

**New Features**

[#18967] Extend the URIResolver behaviour to deal with Catalogue items belonging to Dismissed VREs



## authorization-service 2.1.3
Tagged commit: _[53d7393d2a53fdaba14fcad5df073cc53fa53182](https://code-repo.d4science.org/gCubeSystem/authorization-service/commit/53d7393d2a53fdaba14fcad5df073cc53fa53182)_

  - 2020-06-22

### Fixes
- bug on ApiKey Management (https://support.d4science.org/issues/19487)
## grsf-publisher-ws 1.12.0
Tagged commit: _[aa80ff380095b69b8f4aab436c2ed93481aac674](https://code-repo.d4science.org/gCubeSystem/grsf-publisher-ws/commit/aa80ff380095b69b8f4aab436c2ed93481aac674)_

  - 2020-06-19

### Added

**Features**

- [#19166] Added support for GRSF_pre VRE with the behaviour of GRSF Admin


## wps 1.1.5
Tagged commit: _[ef9432c914c2b2d8abe06d1f4269b3d835e31c53](https://code-repo.d4science.org/gCubeSystem/wps/commit/ef9432c914c2b2d8abe06d1f4269b3d835e31c53)_

  - 2020-06-05

### Fixes

- Updated ExecuteResponseBuilder for support protocol option [#19423]
- Updated to Git and Jenkins [#18120]




## ckan-metadata-publisher-widget 1.6.1
Tagged commit: _[d235b81231728808f174a2cd23a57404d636073f](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/d235b81231728808f174a2cd23a57404d636073f)_

  - 2020-06-18

**New Features**

[#18700] Do not skip regex validation on catalogue item when a field is empty (UI side)



## workspace-tree-widget 6.30.1
Tagged commit: _[ef2f0344786d7cd10ae3f10fd3ab5c66ee5829a9](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/ef2f0344786d7cd10ae3f10fd3ab5c66ee5829a9)_

  - 2020-06-25

**Fixes**

[Task #19544] update the unsharing messages in the accounting history



## workspace 6.24.1
Tagged commit: _[86365fb795cfddca8a05f43ebaec9cfdd179ba8f](https://code-repo.d4science.org/gCubeSystem/workspace/commit/86365fb795cfddca8a05f43ebaec9cfdd179ba8f)_

  - 2020-06-22

[#19493] Just to include the ckan-util-library patched



## gcube-ckan-datacatalog 1.9.0
Tagged commit: _[2855d073405e3729254513a3eed110669db24e99](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/2855d073405e3729254513a3eed110669db24e99)_

  - 2020-07-03

** Enhancements **

[#19559] Check and try to avoid the view per VRE configuration for Public and Gateway Catalogue

Just to include the dependencies [#18700] and [#19479]



## accounting-dashboard 1.2.1
Tagged commit: _[a4edbbd9cb07f60613fa8f825bb96423c9504ff9](https://code-repo.d4science.org/gCubeSystem/accounting-dashboard/commit/a4edbbd9cb07f60613fa8f825bb96423c9504ff9)_

  - 2020-06-18

### Features

- Updated to support new detachedres-library [#19440]

### Fixes

- Disabled Zoom for charts [#19161]




---
*generated by the gCube-ReleaseNotes pipeline from report 739*

*last update Tue Jul 07 15:46:16 CEST 2020*
