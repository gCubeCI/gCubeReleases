# Release Notes for gCube 4.25.0

---
## maven-parent 1.1.0
Tagged commit: _[5a56fed612a5dbc2389e2417bafc4344d973be06](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/5a56fed612a5dbc2389e2417bafc4344d973be06)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## storagehub-client-library 1.2.1
Tagged commit: _[913fa3c9c0d128130b41ad452b44448b579b1668](https://code-repo.d4science.org/gCubeSystem/storagehub-client-library/commit/913fa3c9c0d128130b41ad452b44448b579b1668)_

 - [2020-07-17]

### Feature

method for description update added
## sh-fuse-integration 1.1.0
Tagged commit: _[6e934d763370f99fe8a9d08e0d2b0471defae84f](https://code-repo.d4science.org/gCubeSystem/sh-fuse-integration/commit/6e934d763370f99fe8a9d08e0d2b0471defae84f)_

 2020-09-07

### Fixes

- Solved issue on data download (https://support.d4science.org/issues/19651)
## storagehub-client-wrapper 1.0.0
Tagged commit: _[658c89dfeec5ab500e5dfe2745d80e0629f6c0a1](https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper/commit/658c89dfeec5ab500e5dfe2745d80e0629f6c0a1)_

 [r4-25-0] - 2020-07-15

#### Enhancements

[#19317] component moved from 0.y.z to 1.y.z version

[#19668] add the method updateDescriptionForItem



## workspace-uploader 2.0.6
Tagged commit: _[b3e50eb26bd29114643ab1e02c95a438fb4396f2](https://code-repo.d4science.org/gCubeSystem/workspace-uploader/commit/b3e50eb26bd29114643ab1e02c95a438fb4396f2)_

 - 2020-07-15

[#19317] Just to include the storagehub-client-wrapper at 1.0.0 version

#### Fixes 

[#19243] upload archive facility does not work properly with Windows OS

[#19232] manage file names that use special characters



## usermanagement-core 2.5.1
Tagged commit: _[c1a5a756eaff818c2aced3c580cbbfe5d22cce26](https://code-repo.d4science.org/gCubeSystem/usermanagement-core/commit/c1a5a756eaff818c2aced3c580cbbfe5d22cce26)_

  - 2020-07-10

[#19603]Enhance performance of VRE membership requests retrieval by adding dedicated method for retrieving pending requests by groupid


## ckan2zenodo-library 0.0.2
Tagged commit: _[379f4e2b1752a192bbd6a6c8f5a2576615a1bb2f](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-library/commit/379f4e2b1752a192bbd6a6c8f5a2576615a1bb2f)_

 2020-06-30

### Enhancements
- Default Ckan2Zenodo translation to comply with mandatory Zenodo fields (https://support.d4science.org/issues/19489)
- Ckan2Zenodo library to provide means to apply default translation only (https://support.d4science.org/issues/19490)
- Support to split on source values (https://support.d4science.org/issues/19534) 
- Support to append to target elements (https://support.d4science.org/issues/19535)
- Multiple Date Format parsing (https://support.d4science.org/issues/19540)
- Automatically set item URL if missing.

### Fixes 
- Default filter resource is applied in class Ckan2ZenodoImpl.java (https://support.d4science.org/issues/19528)
- Fixed error message for missing profile.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## workspace-task-executor-library 0.3.0
Tagged commit: _[713935b8542d04a54fbbfd37bb6b794852981df7](https://code-repo.d4science.org/gCubeSystem/workspace-task-executor-library/commit/713935b8542d04a54fbbfd37bb6b794852981df7)_

 - 2020-07-27

[#19677] Migrated to git/jenkins



## ws-thredds-sync-widget 1.2.0
Tagged commit: _[28fb9a66100c788dc69c8eca75de4476be7e9eb9](https://code-repo.d4science.org/gCubeSystem/ws-thredds-sync-widget/commit/28fb9a66100c788dc69c8eca75de4476be7e9eb9)_

 - 2020-07-21

[#19676] Migrated to git/jenkins



## ws-task-executor-widget 0.3.0
Tagged commit: _[242c8c4c971dbaf9e2188e5a7bff1a23188e9de3](https://code-repo.d4science.org/gCubeSystem/ws-task-executor-widget/commit/242c8c4c971dbaf9e2188e5a7bff1a23188e9de3)_

  2020-07-29

[#19724] Migrated to git/jenkins



## ckan2zenodo-publisher-widget 1.0.0
Tagged commit: _[6c6082b4d0708072241a88399f030ab829cb9555](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-publisher-widget/commit/6c6082b4d0708072241a88399f030ab829cb9555)_

 - 2020-08-26

#### New Features

[#19528] Ckan GUI to always allow zenodo publishing



## workspace-tree-widget 6.31.0
Tagged commit: _[b2872ba474dc08c9a11c3ec97c172954cde50bd3](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/b2872ba474dc08c9a11c3ec97c172954cde50bd3)_

  - 2020-09-29

#### Enhancements

[#19600] revisit the Get Info Dialog in a modern view

#### New Features

[#19695] Show the file preview via Google Docs Viewer

#### Bug Fixes

[#19759#note-12] Fixed updating the description of a folder


## workspace-explorer-app 1.3.1
Tagged commit: _[c5cf9534613115d1be0603de0e07bac5fb3afffb](https://code-repo.d4science.org/gCubeSystem/workspace-explorer-app/commit/c5cf9534613115d1be0603de0e07bac5fb3afffb)_

 - 2020-09-25

[#19317] Just to include the storagehub-client-wrapper at 1.0.0 version

#### Fixes

[#19671#note-10] Resolve public link on private folder with at least one shared folder as descendant



## storagehub 1.2.2
Tagged commit: _[2732b05426f6b30d2aaca8542257eff6bafd291c](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/2732b05426f6b30d2aaca8542257eff6bafd291c)_

 - [2020-06-20]

method for description update added


## workspace 6.25.0
Tagged commit: _[0ad2ea761b7801e60291769f2f1c99898ea69390](https://code-repo.d4science.org/gCubeSystem/workspace/commit/0ad2ea761b7801e60291769f2f1c99898ea69390)_

  - 2020-08-25

#### Enhancements

[#19600] revisit the Get Info Dialog in a modern view



## gcube-ckan-datacatalog 1.9.1
Tagged commit: _[f06c00449f4512039be84fbaed0a5b934df91d84](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/f06c00449f4512039be84fbaed0a5b934df91d84)_

  - 2020-07-08

Just to include the dependency [#19528]



## resource-management-portlet 7.0.0
Tagged commit: _[d144b5d7b6252339306e2ce39723fe6f8ae25045](https://code-repo.d4science.org/gCubeSystem/resource-management-portlet/commit/d144b5d7b6252339306e2ce39723fe6f8ae25045)_

 - 2020-07-21

Ported to git

[#19471] Please provide a simple way to get the full Context



---
*generated by the gCube-ReleaseNotes pipeline from report 754*

*last update Wed Sep 30 10:59:48 CEST 2020*
