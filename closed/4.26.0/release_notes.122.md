# Release Notes for gCube 4.26.0

---
## maven-parent 1.1.0
Tagged commit: _[8c56a22daa941c0d993bcda7fd9496ea54323fa8](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8c56a22daa941c0d993bcda7fd9496ea54323fa8)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## gcube-bom 2.0.0
Tagged commit: _[cc8cb4537d1cce433dee8a52062d311a4d567f61](https://code-repo.d4science.org/gCubeSystem/gcube-bom/commit/cc8cb4537d1cce433dee8a52062d311a4d567f61)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19283]



## gcube-smartgears-bom 2.0.0
Tagged commit: _[5460df519ad0b19abc1245c2717cd527b07e537f](https://code-repo.d4science.org/gCubeSystem/gcube-smartgears-bom/commit/5460df519ad0b19abc1245c2717cd527b07e537f)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19283]



## information-system-model 4.0.0
Tagged commit: _[f40b563297dbf738305ba967a81731990ddb2492](https://code-repo.d4science.org/gCubeSystem/information-system-model/commit/f40b563297dbf738305ba967a81731990ddb2492)_

 [r5.0.0]

- Switched JSON management to gcube-jackson [#19116]



## gcube-model 3.0.0
Tagged commit: _[24c444c050da0ac20385903cfa0cfb507384277a](https://code-repo.d4science.org/gCubeSystem/gcube-model/commit/24c444c050da0ac20385903cfa0cfb507384277a)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19116]



## data-transfer-model 1.2.5
Tagged commit: _[a714bc1030cd074b716ea4231a82b0cc9859f110](https://code-repo.d4science.org/gCubeSystem/data-transfer-model.git/commit/a714bc1030cd074b716ea4231a82b0cc9859f110)_

 2020-07-16

### Fixes

- Integration with gcube distribution (boms 2.0.0)
## document-store-lib 3.0.0
Tagged commit: _[f85774840a74dacb8e42a279bf3736fa5e8d86cb](https://code-repo.d4science.org/gCubeSystem/document-store-lib/commit/f85774840a74dacb8e42a279bf3736fa5e8d86cb)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19115]
- Changed the way to manage scheduled thread termination [#18547]



## resource-registry-api 4.0.0
Tagged commit: _[bd02dc4508f67da3d144bdfc015ce02ae93be6cb](https://code-repo.d4science.org/gCubeSystem/resource-registry-api/commit/bd02dc4508f67da3d144bdfc015ce02ae93be6cb)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19116]



## geonetwork 3.4.4
Tagged commit: _[a9c4dc4a8966febd21c518708325ee4be4a83556](https://code-repo.d4science.org/gCubeSystem/geonetwork.git/commit/a9c4dc4a8966febd21c518708325ee4be4a83556)_

 - 2020-05-15

### Fixes
- gCube bom 2.0.0 integration (https://support.d4science.org/issues/19612)
- NOT NULL constraints removed from legacy methods (https://support.d4science.org/issues/19965)


## resource-registry-client 4.0.0
Tagged commit: _[ad84724a3de10bf854063fcf69ac26543d3b13d6](https://code-repo.d4science.org/gCubeSystem/resource-registry-client/commit/ad84724a3de10bf854063fcf69ac26543d3b13d6)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19116]


## data-transfer-library 1.2.2
Tagged commit: _[f2cf7c47ad80de7b5365320c6673d5fbfd6506d5](https://code-repo.d4science.org/gCubeSystem//data-transfer-library.git/commit/f2cf7c47ad80de7b5365320c6673d5fbfd6506d5)_

 2020-07-16

### Fixes

- Integration with gcube distribution (boms 2.0.0)
- TransfererBuilder.getTransfererByHost now follows redirects
## accounting-lib 4.0.0
Tagged commit: _[26bc2e21284d55b503b3bbfe2638a0bb57f8677a](https://code-repo.d4science.org/gCubeSystem/accounting-lib/commit/26bc2e21284d55b503b3bbfe2638a0bb57f8677a)_

 [r.5.0.0] - 

- Switched JSON management to gcube-jackson [#19115]
- Properly terminated RegexRulesAggregator scheduled thread [#18547]
- Added improved version of calledMethod rewrite for aggregation [#10645]



## resource-registry-publisher 4.0.0
Tagged commit: _[553232a8413666eeffad90a99c14df10597dd3cf](https://code-repo.d4science.org/gCubeSystem/resource-registry-publisher/commit/553232a8413666eeffad90a99c14df10597dd3cf)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19116]



## gis-interface 2.4.4
Tagged commit: _[8a9f331c408d571b87aec1374ddf71e57e1be342](https://code-repo.d4science.org/gCubeSystem/gis-interface.git/commit/8a9f331c408d571b87aec1374ddf71e57e1be342)_

 2020-09-03

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## sdi-interface 1.2.0
Tagged commit: _[6cee0c17500228181f085fa7108dda3e13b8443b](https://code-repo.d4science.org/gCubeSystem//sdi-interface.git/commit/6cee0c17500228181f085fa7108dda3e13b8443b)_

 - 2020-09-03

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## document-store-lib-accounting-service 2.0.0
Tagged commit: _[65f5e32a7317ba90e1fbb2112f4f8d00ae232333](https://code-repo.d4science.org/gCubeSystem/document-store-lib-accounting-service/commit/65f5e32a7317ba90e1fbb2112f4f8d00ae232333)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19115]
- Added more logs to provide bugs investigation



## metadata-profile-discovery 1.0.0
Tagged commit: _[e0d5c00e5fca5aa76fef9ceadd41e9a718bfec32](https://code-repo.d4science.org/gCubeSystem/metadata-profile-discovery/commit/e0d5c00e5fca5aa76fef9ceadd41e9a718bfec32)_

 - 2020-09-30

#### First release

[#19880] Create the library metadata-profile-discovery
## gis-interface 2.4.4
Tagged commit: _[8a9f331c408d571b87aec1374ddf71e57e1be342](https://code-repo.d4science.org/gCubeSystem/gis-interface.git/commit/8a9f331c408d571b87aec1374ddf71e57e1be342)_

 2020-09-03

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## ecological-engine-smart-executor 1.6.4
Tagged commit: _[cc4cd69c47c87ff70fb3983bd11b9dd3c449aa5c](https://code-repo.d4science.org/gCubeSystem/ecological-engine-smart-executor/commit/cc4cd69c47c87ff70fb3983bd11b9dd3c449aa5c)_

 - 2020-10-15

### Features

- Updated pom.xml for support gcube-bom-2.0.0-SNAPSHOT [#19790]




## sdi-library 1.2.0
Tagged commit: _[3d53a84e4bc4f04c0eef399ce64cb68ff480bc1c](https://code-repo.d4science.org/gCubeSystem/sdi-library.git/commit/3d53a84e4bc4f04c0eef399ce64cb68ff480bc1c)_

 2020-07-21

## Enhancements

- Application Profile (https://support.d4science.org/issues/18939)


### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## common-smartgears 3.0.0
Tagged commit: _[fd6857ab60cc6ec3b65ed63b0c6e227b744b56d2](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/fd6857ab60cc6ec3b65ed63b0c6e227b744b56d2)_

 [r5.0.0] - 

- Switched container JSON management to gcube-jackson [#19283]



## geoportal-logic 1.0.2
Tagged commit: _[64522f05273e908cb1ed46f5d9b2ceaea692d3ec](https://code-repo.d4science.org/gCubeSystem/geoportal-logic.git/commit/64522f05273e908cb1ed46f5d9b2ceaea692d3ec)_

 - 2020-11-4

PublicationReport
Fix style publication [File not found]
Fix getManagerByID



## data-transfer-plugin-framework 1.0.3
Tagged commit: _[7b5dbedbcf96a85f1f07607d14ff32401d6f1d2a](https://code-repo.d4science.org/gCubeSystem/data-transfer-plugin-framework.git/commit/7b5dbedbcf96a85f1f07607d14ff32401d6f1d2a)_

 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## decompress-archive-plugin 1.0.2
Tagged commit: _[37fdb9c198ae68f7063971417c3a8c1c46fb31f5](https://code-repo.d4science.org/gCubeSystem/decompress-archive-plugin.git/commit/37fdb9c198ae68f7063971417c3a8c1c46fb31f5)_

 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## sis-geotk-plugin 1.1.3
Tagged commit: _[ae2025e03ef31ab2ecd39d6a1f9c4a59cb1b1ec0](https://code-repo.d4science.org/gCubeSystem/sis-geotk-plugin.git/commit/ae2025e03ef31ab2ecd39d6a1f9c4a59cb1b1ec0)_

 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## metadata-profile-form-builder-widget 1.0.0
Tagged commit: _[051e7c606396f609bdc6b32424dcfd3d5205ae91](https://code-repo.d4science.org/gCubeSystem/metadata-profile-form-builder-widget/commit/051e7c606396f609bdc6b32424dcfd3d5205ae91)_

 - 2020-10-08

#### First release

[#19884] Create widget to build a data-entry form by using metadata-profile-discovery 

[#19878] Create a data entry facility to get (meta)data object defined by gCube Metada Profile
## data-transfer-service 2.0.7
Tagged commit: _[b4b719a91eaa4d02660a9ca348a362be180a0541](https://code-repo.d4science.org/gCubeSystem/data-transfer-service.git/commit/b4b719a91eaa4d02660a9ca348a362be180a0541)_

 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## sdi-service 1.4.2
Tagged commit: _[18d7f31ec626308c4e1f7c9b4916bd473f4c271b](https://code-repo.d4science.org/gCubeSystem/sdi-service.git/commit/18d7f31ec626308c4e1f7c9b4916bd473f4c271b)_

 2020-05-15

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
## wps 1.1.6
Tagged commit: _[c324701620aaafb5278c7ee784d0f6a8374a1a39](https://code-repo.d4science.org/gCubeSystem/wps/commit/c324701620aaafb5278c7ee784d0f6a8374a1a39)_

 - 2020-10-15

### Features

- Updated pom.xml for support gcube-bom-2.0.0-SNAPSHOT [#19790]



## geoportal-data-entry-app 1.0.1
Tagged commit: _[053bb4d72d56ee61b41fa7d7d379b90914933656](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/053bb4d72d56ee61b41fa7d7d379b90914933656)_

 - 2020-11-04

#### Bug fixes

[#20063] Fixes for data-entry components



## smartgears-distribution 3.0.0
Tagged commit: _[412471a48c59bdc1052932493a0745f73a426dc2](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/412471a48c59bdc1052932493a0745f73a426dc2)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19283]
- Fixed distro files and pom according to new release procedure



## smartgears-distribution-bundle 3.0.0
Tagged commit: _[91fbe234029cfb0508abe7450dd1e4709dd80fb8](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution-bundle/commit/91fbe234029cfb0508abe7450dd1e4709dd80fb8)_

 [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19283]
- Fixed distro files and pom according to new release procedure




---
*generated by the gCube-ReleaseNotes pipeline from report 795*

*last update Wed Nov 11 10:45:27 CET 2020*
