# Release Notes for gCube 4.28.0

---
## maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## geoportal-common 1.0.5
Tagged commit: _[74ccb666bc89c2b5673f24dede5d5ad72f903306](https://code-repo.d4science.org/gCubeSystem/geoportal-common.git/commit/74ccb666bc89c2b5673f24dede5d5ad72f903306)_

 - 2020-12-9
Mongo Id in record
Mongo Concessioni interface 
Added Files.fixFileNAme
ValidationReport as field
Updated Model (#20357)


## geoportal-client 1.0.3
Tagged commit: _[c72decd1af8b6c1ee3b7ba38564c79abe8e7d407](https://code-repo.d4science.org/gCubeSystem/geoportal-client.git/commit/c72decd1af8b6c1ee3b7ba38564c79abe8e7d407)_

 - 2020-11-11
Stateful Concessioni Manager client over mongo 


## ecological-engine 1.14.0
Tagged commit: _[9456af2cf487676c5a7d82c1fc1a316dcffe72bf](https://code-repo.d4science.org/gCubeSystem/ecological-engine/commit/9456af2cf487676c5a7d82c1fc1a316dcffe72bf)_

 - 2021-01-20

### Features

- maryTTS removed
- upgrade repository definition
- added log4j-over-sl4j dependency


## dataminer-invocation-model 1.0.0
Tagged commit: _[d35429e5021c82171525c7ede66214d31355fe88](https://code-repo.d4science.org/gCubeSystem/dataminer-invocation-model/commit/d35429e5021c82171525c7ede66214d31355fe88)_

 - 2021-01-13

[Task #20426] Migrate the dataminer-invocation-model component to git/jenkins and fix the jackson version



## ecological-engine-smart-executor 1.6.5
Tagged commit: _[d83df6c225197461b7418efa3e61fde3bbdee561](https://code-repo.d4science.org/gCubeSystem/ecological-engine-smart-executor/commit/d83df6c225197461b7418efa3e61fde3bbdee561)_

 - 2021-01-20

### Features

- added commons-io-2.6 dependency
- update ecological-engine lower bound range



## ecological-engine-geospatial-extensions 1.5.2
Tagged commit: _[93613b27243d57da9e9d01c1c4395d74eaa3d7b9](https://code-repo.d4science.org/gCubeSystem/ecological-engine-geospatial-extensions/commit/93613b27243d57da9e9d01c1c4395d74eaa3d7b9)_

 2021-01-18
 - update gis-interface lower bound range 
 - replaced repositories defined into the pom  with the new nexus url (nexus.d4science.org). update version to 1.5.2-SNAPSHOT
 - update ecological-engine-smart-executor lower range
 - removed maryTTS classes #20135



## seadatanet-connector 1.2.2
Tagged commit: _[ac3cb2b3a732cb5c1e36386c650e4b7d60cced5c](https://code-repo.d4science.org/gCubeSystem/seadatanet-connector/commit/ac3cb2b3a732cb5c1e36386c650e4b7d60cced5c)_

- 2021-01-20

### Features

- update ecological-engine-geospatial-extensions lower bound range


## ecological-engine-external-algorithms 1.2.2
Tagged commit: _[eb58ff74c9c1df5b90501b2ee309eecad9d49675](https://code-repo.d4science.org/gCubeSystem/ecological-engine-external-algorithms/commit/eb58ff74c9c1df5b90501b2ee309eecad9d49675)_

 - 2021-01-21

### Features

- update repository declaration to new nexus url
- update seadatanet-connector lower bound range
- update ecological-engine-geospatial-extension lower bound range


## dataminer 1.7.0
Tagged commit: _[6334bb4224d4f895d0b830fb49bc159d40445e47](https://code-repo.d4science.org/gCubeSystem/dataminer/commit/6334bb4224d4f895d0b830fb49bc159d40445e47)_

 - 2020-11-20

- import range modified to resolve old repositories invalid url


## geoportal-logic 1.0.14
Tagged commit: _[108a61730a0f98ed1b2de08927a254999645ac82](https://code-repo.d4science.org/gCubeSystem/geoportal-logic.git/commit/108a61730a0f98ed1b2de08927a254999645ac82)_

 - 2020-12-11
Fixes no transaction issue



## rmp-common-library 2.8.4
Tagged commit: _[df1b05123303f0a72b07bcc820e109eaf120dc32](https://code-repo.d4science.org/gCubeSystem/rmp-common-library/commit/df1b05123303f0a72b07bcc820e109eaf120dc32)_

 - 2020-12-14

Fix Bug #20337 infrastructure-monitor: the searchById doesn't work anymore


## social-util-library 1.7.3
Tagged commit: _[dfa541264ed859a7622cef2d6cf28afa939fd5f6](https://code-repo.d4science.org/gCubeSystem/social-util-library/commit/dfa541264ed859a7622cef2d6cf28afa939fd5f6)_

 - 2019-12-19

[#18356] Bug Fixed: Social networking ... error in parsing URLs ending with )


## workspace-task-executor-library 1.0.0
Tagged commit: _[59ae400b1fb4fe53366f609987c4700cdff09063](https://code-repo.d4science.org/gCubeSystem/workspace-task-executor-library/commit/59ae400b1fb4fe53366f609987c4700cdff09063)_

 - 2021-01-18

#### Bug Fixes

[#20457] Fixed incident



## sh-fuse-integration 1.1.1
Tagged commit: _[97184640f14e75eb65b0349d3e6ece3c61a7431f](https://code-repo.d4science.org/gCubeSystem/sh-fuse-integration/commit/97184640f14e75eb65b0349d3e6ece3c61a7431f)_

 2021-01-15

### Fixes
	
- issue on right displayed 
- https://support.d4science.org/issues/20417 


## dataminer-algorithms-importer 1.2.2
Tagged commit: _[02130ef4ec3920571766009e2da77f4efa2b539a](https://code-repo.d4science.org/gCubeSystem/dataminer-algorithm-importer/commit/02130ef4ec3920571766009e2da77f4efa2b539a)_

 - 2020-11-11

### Fix
 - Config path in addAlgorithm


## social-data-indexer-se-plugin 2.0.0
Tagged commit: _[22dd7fabb36bd9568602948073b11f580d38a654](https://code-repo.d4science.org/gCubeSystem/social-data-indexer-se-plugin/commit/22dd7fabb36bd9568602948073b11f580d38a654)_

 

- Switched to smart-executor-api 2.0.0


## geoportal-service 1.0.4
Tagged commit: _[b37cd4233f416b401e27a4bdec9f8cdb7c2f6c2f](https://code-repo.d4science.org/gCubeSystem/geoportal-service.git/commit/b37cd4233f416b401e27a4bdec9f8cdb7c2f6c2f)_

 2020-11-11
Mongo integration with Concessione
Project interface
TempFile management
WorkspaceContent and publication for Concessioni-over-mongo 


## wps 1.1.7
Tagged commit: _[762142b629b4082d9a6806b4b1cb3edbfb84bad7](https://code-repo.d4science.org/gCubeSystem/wps/commit/762142b629b4082d9a6806b4b1cb3edbfb84bad7)_

 - 2020-11-11

### Fix
 - Added a printStackTrace in executeRequest
 - config Path configurable via web.xml
 - fix repository declarations


## gFeed-Suite 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## commons 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## catalogue-plugin-framework 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## collectors-plugin-framework 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## test-commons 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## gCat-Feeder 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## DataMinerAlgorithmsCrawler 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## gCat-Controller 1.0.4
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

 - 2020-12-15
- Dependency management
- Fixed naming


## oai-harvester 1.0.5
Tagged commit: _[b4fc6a5edebe5baff9679ac7719848bd9e083d4e](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/b4fc6a5edebe5baff9679ac7719848bd9e083d4e)_

Tags 1.0.5 / 4.28.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## ws-task-executor-widget 1.0.0
Tagged commit: _[b548ffdc96fef808b989b5d783edc16a1ed2c78c](https://code-repo.d4science.org/gCubeSystem/ws-task-executor-widget/commit/b548ffdc96fef808b989b5d783edc16a1ed2c78c)_

 - 2021-01-18

#### Bug Fixes

[#20457] Just including patched library



## workspace-tree-widget 6.31.2
Tagged commit: _[3d8cf49f2e852c16148d4db2356c33607a737be9](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/3d8cf49f2e852c16148d4db2356c33607a737be9)_

 - 2021-01-18

#### Bug Fixes

[#20457] Just including patched library


## geoportal-data-entry-app 1.2.0
Tagged commit: _[e221838586bb8d9884ff2c583c9140f23e28d707](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/e221838586bb8d9884ff2c583c9140f23e28d707)_

 - 2020-12-18

#### Enhancements

[#20357] new requirements



## geoportal-data-viewer-app 1.1.0
Tagged commit: _[23be72d69fef8933a6fa51f547ac7a79dc3e5061](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/23be72d69fef8933a6fa51f547ac7a79dc3e5061)_

 - 2020-12-21

#### Enhancements

[#20357] Improvements feedback-driven 


## workspace 6.25.2
Tagged commit: _[1fd8867630ee2dab6c9762a27d4bc3d5f8b89b87](https://code-repo.d4science.org/gCubeSystem/workspace/commit/1fd8867630ee2dab6c9762a27d4bc3d5f8b89b87)_

 - 2021-01-18

#### Bug Fixes

[#20457] Just including patched library


## workspace-widget-portlet 1.4.1
Tagged commit: _[a2d522d7e8df2958cc5a57d4b83b1ddc0560fb02](https://code-repo.d4science.org/gCubeSystem//workspace-widget-portlet/commit/a2d522d7e8df2958cc5a57d4b83b1ddc0560fb02)_

 - 2021-01-26

- Fix Bug #20552 Workspace widget item counter no longer works


## workspace-widget-portlet 1.4.1
Tagged commit: _[a2d522d7e8df2958cc5a57d4b83b1ddc0560fb02](https://code-repo.d4science.org/gCubeSystem//workspace-widget-portlet/commit/a2d522d7e8df2958cc5a57d4b83b1ddc0560fb02)_

 - 2021-01-26

- Fix Bug #20552 Workspace widget item counter no longer works



---
*generated by the gCube-ReleaseNotes pipeline from report 888*

*last update Mon Feb 01 18:23:44 CET 2021*
