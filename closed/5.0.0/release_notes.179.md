# Release Notes for gCube 5.0.0

---
## maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## gcube-bom 2.0.1
Tagged commit: _[2ba8e74ea6a19aceac736f9c6efe1175fd5be05a](https://code-repo.d4science.org/gCubeSystem/gcube-bom/commit/2ba8e74ea6a19aceac736f9c6efe1175fd5be05a)_

 

- updated resource registry clients versions


## gcube-smartgears-bom 2.1.0
Tagged commit: _[cf36031d256700f642e3547c1ad866358c65d13a](https://code-repo.d4science.org/gCubeSystem/gcube-smartgears-bom/commit/cf36031d256700f642e3547c1ad866358c65d13a)_



- Added missing new IS libs


## oidc-library 1.2.0
Tagged commit: _[2e0fb63ee67ab664b16b2a351405140e7759c973](https://code-repo.d4science.org/gCubeSystem/oidc-library/commit/2e0fb63ee67ab664b16b2a351405140e7759c973)_


- Site is now comparable. Added JTI value getter and essentials info dump in JWT. Revised get avatar helper.


## event-publisher-library 1.0.2
Tagged commit: _[3f01b590f28bc6f02e4f3a34d909f29d73e805cb](https://code-repo.d4science.org/gCubeSystem/event-publisher-library/commit/3f01b590f28bc6f02e4f3a34d909f29d73e805cb)_


- Refactored/extracted event sender that uses OIDC token only and extended it for the UMA version.


## common-authorization 2.3.0
Tagged commit: _[3737d4dcc2342585e2e092a1306ad49b32fce903](https://code-repo.d4science.org/gCubeSystem/common-authorization/commit/3737d4dcc2342585e2e092a1306ad49b32fce903)_

 - [2010-11-17]

support for the new IAM added


## gcat-api 1.2.2
Tagged commit: _[a54a67f6fe1471f87239ff1013c61fdd4433e396](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/a54a67f6fe1471f87239ff1013c61fdd4433e396)_



- Added count method for Item collection [#20627]
- Added count method for Organization, Group and Profile collection [#20629]
- Switched JSON management to gcube-jackson [#19735]



## information-system-model 4.1.0
Tagged commit: _[7763dd4abbc668b88cff7fe20e6092b02d2d64a0](https://code-repo.d4science.org/gCubeSystem/information-system-model/commit/7763dd4abbc668b88cff7fe20e6092b02d2d64a0)_



- Added support to include additional properties in Property types [#20012]



## storagehub-model 1.0.9
Tagged commit: _[4c129ed26448be05e565727cb3116df72029732a](https://code-repo.d4science.org/gCubeSystem/storagehub-model/commit/4c129ed26448be05e565727cb3116df72029732a)_

 - [2020-10-07]

StorageBackendFacory to support different backend added
## resource-registry-api 4.1.0
Tagged commit: _[58e0e38300f8f96b9c8a1e61426d934ce9ba1c05](https://code-repo.d4science.org/gCubeSystem/resource-registry-api/commit/58e0e38300f8f96b9c8a1e61426d934ce9ba1c05)_

 

- Added ContextCache utilities 
- Added support to provide APIs to get the list of instance contexts [#20012] [#20013]


## common-gcube-calls 1.2.0
Tagged commit: _[be74dac1124561d64b0700128382d85ba3fb78a2](https://code-repo.d4science.org/gCubeSystem/common-gcube-calls/commit/be74dac1124561d64b0700128382d85ba3fb78a2)_

 - 2020-11-18

- interceptor for UmaToken added
## common-smartgears 3.0.1
Tagged commit: _[0ab8e327cfaefff25bb7f9a4e30d25a8719f48ae](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/0ab8e327cfaefff25bb7f9a4e30d25a8719f48ae)_

 - 2020-11-18

- new Uma Token integration


## resource-registry-client 4.1.0
Tagged commit: _[58b1c9881a6750201466dd75875986c45a057f47](https://code-repo.d4science.org/gCubeSystem/resource-registry-client/commit/58b1c9881a6750201466dd75875986c45a057f47)_

 

- Used ContextCache to make the client more efficient
- Added APIs to get instance contexts [#20013]
- Added support to request contexts in instances header [#20012]


## gcat-client 1.2.2
Tagged commit: _[9f8cae41ac79b8f70f4780b55a246c041c79522a](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/9f8cae41ac79b8f70f4780b55a246c041c79522a)_



- Added count method for Item collection [#20627]
- Added count method for Organization, Group and Profile collection [#20629]



## storagehub-client-library 1.2.2
Tagged commit: _[74281ed64fd03d5f5a12e05890ccaeb5c027cf7a](https://code-repo.d4science.org/gCubeSystem/storagehub-client-library/commit/74281ed64fd03d5f5a12e05890ccaeb5c027cf7a)_

 - [2021-02-08]

### Feature

method for user existence check added



## resource-registry-publisher 4.1.0
Tagged commit: _[450a131875691a9986dc234e7c0b786c142da10c](https://code-repo.d4science.org/gCubeSystem/resource-registry-publisher/commit/450a131875691a9986dc234e7c0b786c142da10c)_

 

- Used ContextCache to make the client more efficient
- Added APIs to get instance contexts [#20013]
- Added support to request contexts in instances header [#20012]


## common-smartgears-app 2.0.3
Tagged commit: _[1855c9e8725f52b3b231a2be934bc9135fa1cd7c](https://code-repo.d4science.org/gCubeSystem/common-smartgears-app/commit/1855c9e8725f52b3b231a2be934bc9135fa1cd7c)_

 - [2020-11-03]

removed WARNING log on Vfs.Dir at startup (https://support.d4science.org/issues/18551)
## resource-registry-schema-client 4.0.0
Tagged commit: _[c29d91541f15efd1b53afe74c52fe5f4df148194](https://code-repo.d4science.org/gCubeSystem/resource-registry-schema-client/commit/c29d91541f15efd1b53afe74c52fe5f4df148194)_

  - 

- Switched JSON management to gcube-jackson [#19116]



## resource-registry-context-client 4.0.0
Tagged commit: _[5b0d18ff62d998c4faed267f6762e11f3b034051](https://code-repo.d4science.org/gCubeSystem/resource-registry-context-client/commit/5b0d18ff62d998c4faed267f6762e11f3b034051)_



- Used ContextCache to make the client more efficient
- Switched JSON management to gcube-jackson [#19116]



## oidc-library-portal 1.2.0
Tagged commit: _[db8887e3b1fba12b95439d40cfdf7f10ff2f99ce](https://code-repo.d4science.org/gCubeSystem/oidc-library-portal/commit/db8887e3b1fba12b95439d40cfdf7f10ff2f99ce)_


- New context roles mapper that not rely on User or Groups objects but in their ID as strings. New revised version of the cache proxy with mutex and session id used in place of session object, logs revised and scheduled log added that dumps cache contents. Marked as deprecated all utils methods for session objects get/set. (#20445)


## event-publisher-portal 1.0.2
Tagged commit: _[4ae6933607070af57aa5f71627a202fd6401e043](https://code-repo.d4science.org/gCubeSystem/event-publisher-portal/commit/4ae6933607070af57aa5f71627a202fd6401e043)_


- OIDC token only is now used to send events to the orchestrator endpoint (UMA token is no more involved).


## landing-page-library 1.4.0
Tagged commit: _[ff83d9a603e86c28184f6ec99afaa8049b6ccba2](https://code-repo.d4science.org/gCubeSystem/landing-page-library/commit/ff83d9a603e86c28184f6ec99afaa8049b6ccba2)_

 - 2021-01-07

Feature #20395 - Remove user automatic registration to SoBigDataLab VRE from landing-page hook


## email-templates-library 1.4.4
Tagged commit: _[2577a7dc6cfdd1fd61b78086446bd252671b5614](https://code-repo.d4science.org/gCubeSystem/email-templates-library/commit/2577a7dc6cfdd1fd61b78086446bd252671b5614)_

 - 2021-01-26

### Bug Fix

Welcome create account sign in url pointed to gateway home instead of VRE home page


## oidc-enrollment-hook 1.1.2
Tagged commit: _[77e62a0eeb5146e8f23f65aa4dc1c37dd2d003ce](https://code-repo.d4science.org/gCubeSystem/oidc-enrollment-hook/commit/77e62a0eeb5146e8f23f65aa4dc1c37dd2d003ce)_


- Added some info of the user is about to create in the logs expecially for screen name (auto-generated or externally provided) (#20413). Restored per-session token removal. Logs revised. (#20445)


## event-publisher-hook 1.0.0
Tagged commit: _[e978a8b737e4a728393a03c2203f57cf52e9fff0](https://code-repo.d4science.org/gCubeSystem/event-publisher-hook/commit/e978a8b737e4a728393a03c2203f57cf52e9fff0)_


- First release (#19461)


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## user-registration-hook 2.0.2
Tagged commit: _[bf740de4f64247f35c32674802634e5fc2636289](https://code-repo.d4science.org/gCubeSystem/user-registration-hook/commit/bf740de4f64247f35c32674802634e5fc2636289)_

 - 2020-11-13

Feature #20108, remove user deletion from ldap upon remove account (offloaded to keycloak)


## VREFolder-hook 6.8.0
Tagged commit: _[77dc43ca4786cbe6bf62567833407403763c8553](https://code-repo.d4science.org/gCubeSystem/VREFolder-hook/commit/77dc43ca4786cbe6bf62567833407403763c8553)_

 - 2021-02-04

Now uses storagehub methods and new auth (UMA tokens) to perform user adding and removal form VRE Folder and Roles (VRE-Manager) to set VRE Folder Administrators



## landing-page-hook 7.0.0
Tagged commit: _[e53b60c6a16c6275624fd693580d2d9788f137f5](https://code-repo.d4science.org/gCubeSystem/landing-page-hook/commit/e53b60c6a16c6275624fd693580d2d9788f137f5)_

 - 2020-07-21

Task #19696 LR User Account page should redirect to KC user Account page, this components implements the redirection

Ported to git


## social-dockbar-hook 7.0.0
Tagged commit: _[dd3eee7b447a832f439296d75b7d17d7fd634f64](https://code-repo.d4science.org/gCubeSystem/social-dockbar-hook/commit/dd3eee7b447a832f439296d75b7d17d7fd634f64)_

 - 2020-10-01

ported to git and added patch for my account URL pointing to keycloak user acount URL


## keycloak-d4science-spi-parent 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## avatar-storage 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## avatar-realm-resource 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## avatar-importer 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## event-listener-provider 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## delete-account 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## identity-provider-mapper 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## ldap-storage-mapper 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## keycloak-d4science-bundle 1.0.1
Tagged commit: _[19e98cef4b8f63781cc5a389e49171a23af428e7](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/19e98cef4b8f63781cc5a389e49171a23af428e7)_


user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar


## oidc-keycloak-library 1.0.0
Tagged commit: _[7cefca5d5f239fb32030ab1d4eb72fa774e42d48](https://code-repo.d4science.org/gCubeSystem/oidc-keycloak-library/commit/7cefca5d5f239fb32030ab1d4eb72fa774e42d48)_


- First release (#19143, #19891)


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## invites-common-library 1.5.1
Tagged commit: _[24207fd25aa536078b9a12dd7b156d8b2e859e45](https://code-repo.d4science.org/gCubeSystem/invites-common-library/commit/24207fd25aa536078b9a12dd7b156d8b2e859e45)_

 - 2020-07-21

Ported to git

Feature #19708 send a temporary password in the invite email if the account does not exists


## threadlocal-vars-cleaner 2.2.1
Tagged commit: _[6e67b8dae2494a7f6a51307d30d6a6d9148ac4b1](https://code-repo.d4science.org/gCubeSystem/threadlocal-vars-cleaner/commit/6e67b8dae2494a7f6a51307d30d6a6d9148ac4b1)_

 - 2020-12-17

Added support for OIDC related communications

Fixed wrong token put in cache after refresh. Rationalized logs.

Ported to git


## storagehub 1.2.4
Tagged commit: _[1e916b21b6f370fcaad9138f7e26f34acf6c5419](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/1e916b21b6f370fcaad9138f7e26f34acf6c5419)_

 - [2020-01-11]

mimetype set also on non parsable pdf

method add and remove admin on a VREFolder enabled also for admin role

fixed bug on unsharing folder on user removal 

added method exist user


## gcat 1.4.4
Tagged commit: _[bbf1755f7240604f0f0b0251088da45ff9abe72f](https://code-repo.d4science.org/gCubeSystem/gcat/commit/bbf1755f7240604f0f0b0251088da45ff9abe72f)_

 

- Added count method for Item collection [#20627]
- Added count method for Organization, Group and Profile collection [#20629]
- Switched JSON management to gcube-jackson [#19735]



## uri-resolver 2.4.1
Tagged commit: _[7a28d437684846e3a08a692ffbabc3aa1617b954](https://code-repo.d4science.org/gCubeSystem/uri-resolver/commit/7a28d437684846e3a08a692ffbabc3aa1617b954)_

 - 2021-01-13

**Bug Fixes**

[Task #19942] Fixing master build fails



## gCat-Controller 1.0.5
Tagged commit: _[903171222a6d48ba725fa2aee8b24c833a4adba0](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/903171222a6d48ba725fa2aee8b24c833a4adba0)_

Tags 1.0.5 / 5.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gFeed
## ckan-metadata-publisher-widget 1.6.2
Tagged commit: _[b1e1cf5b4d9ed82fe3fb35ec491575542b605253](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/b1e1cf5b4d9ed82fe3fb35ec491575542b605253)_

 - 2021-02-08

**Bug Fixes**

[#20446] Catalogue Publishing Widget: field value unexpectedly added in case of optional field 
[#20663] Fixing Time_Interval placeholder 



## workspace-sharing-widget 1.11.0
Tagged commit: _[eee58c7e72db16f4cc61df9648c9719504459d97](https://code-repo.d4science.org/gCubeSystem/workspace-sharing-widget/commit/eee58c7e72db16f4cc61df9648c9719504459d97)_

  - 2021-02-02

### Features

- Updated the set administrator method for VRE folders [#19952]




## accept-invite-portlet 2.0.0
Tagged commit: _[35eea55480b28dda0e1801fc2f6bb118a6762616](https://code-repo.d4science.org/gCubeSystem/accept-invite-portlet/commit/35eea55480b28dda0e1801fc2f6bb118a6762616)_

 - 2020-07-21

Feature #19463, gestione Inviti include Keycloak per quanto riguarda la creazione dello user account

Ported to git


## invite-members 2.7.0
Tagged commit: _[da0560b98273bd79c8c728a5e2e8a7e6b4a104ad](https://code-repo.d4science.org/gCubeSystem/invite-members/commit/da0560b98273bd79c8c728a5e2e8a7e6b4a104ad)_

 - 2020-12-11

Feature #19463, gestione Inviti include Keycloak per quanto riguarda la creazione dello user account

Ported to git

Ported to GWT 2.8.2


## social-profile 2.4.0
Tagged commit: _[6ceb1d5afa5ef589cecbe8ba1923fc6c235c21d5](https://code-repo.d4science.org/gCubeSystem/social-profile/commit/6ceb1d5afa5ef589cecbe8ba1923fc6c235c21d5)_

 - 2020-12-14

 - Removed the import from linked in button


## create-users-portlet 3.0.0
Tagged commit: _[ba257fe7ff0f29b60140c2497d4c618046148fbd](https://code-repo.d4science.org/gCubeSystem/create-users-portlet/commit/ba257fe7ff0f29b60140c2497d4c618046148fbd)_

 - 2020-09-24

Ported to git

[#19812] modified to support the new IAM Keycloak based



## vre-deploy 4.3.1
Tagged commit: _[773450dd7a5b971aba5669e276961124981c41fc](https://code-repo.d4science.org/gCubeSystem/vre-deploy/commit/773450dd7a5b971aba5669e276961124981c41fc)_

 - 2021-02-02

Ported to git

modified to support the new IAM Keycloak based



## workspace 6.25.3
Tagged commit: _[ef0d950deba9935ca60219b8bcc9c3eb23a6577a](https://code-repo.d4science.org/gCubeSystem/workspace/commit/ef0d950deba9935ca60219b8bcc9c3eb23a6577a)_

 - 2021-02-02

Just to include new SHUB model


## gcube-ckan-datacatalog 1.9.2
Tagged commit: _[c3c5d04504b6c2421aa83b99531ea346abc5b85a](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/c3c5d04504b6c2421aa83b99531ea346abc5b85a)_

 - 2021-02-08

#### Bug fixes

Just to include the fix reported at [#20446]



## gcube-portal-bundle 5.0.0
Tagged commit: _[bdb13562c48da75a9f7501297c3bbd1b0e90dd2d](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/bdb13562c48da75a9f7501297c3bbd1b0e90dd2d)_

 - 2020-11-16

Added support for keycloak as IAM


## smartgears-distribution 3.2.0
Tagged commit: _[81a3b5630696e5ff94503291bfd7279836b656b1](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/81a3b5630696e5ff94503291bfd7279836b656b1)_



- clean-container script uses proxy
 


---
*generated by the gCube-ReleaseNotes pipeline from report 906*

*last update Wed Feb 24 16:31:41 CET 2021*
