# Release Notes for gCube 5.0.1

---
## maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


## oidc-enrollment-hook 1.1.3
Tagged commit: _[bb43178b29cc5bb8a2eb1824c90e2976b568dce9](https://code-repo.d4science.org/gCubeSystem/oidc-enrollment-hook/commit/bb43178b29cc5bb8a2eb1824c90e2976b568dce9)_


- Now user reconciliation/identification from OIDC token after the login is performed no more checking by using the email address but by using the User's username, the Liferay . (#20827) (#20840)


## login-hook 1.5.0
Tagged commit: _[6296d890dd3c75e1486eac8e91f326816a9e5060](https://code-repo.d4science.org/gCubeSystem/login-hook/commit/6296d890dd3c75e1486eac8e91f326816a9e5060)_

 - 2021-02-25

- Ported to git

- Disabled the possibility to login via Liferay. Shows a Button pointing to keycloak login if a user is redirected there by old pages. 



---
*generated by the gCube-ReleaseNotes pipeline from report 912*

*last update Thu Mar 04 17:16:15 CET 2021*
