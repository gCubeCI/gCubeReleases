# Release Notes for gCube 5.11.0

---
###  common-authorization 2.5.0
Tagged commit: _[e68d49fc646c119ec561b3e607a8903760ada6b9](https://code-repo.d4science.org/gCubeSystem/common-authorization/commit/e68d49fc646c119ec561b3e607a8903760ada6b9)_

 - [2022-04-20]

- Deprecated AccessTokenProvider, AuthorizationProvider and SecurityTokenProvider [#22871]
- Added roles to ExternalService Client info


###  keycloak-client 1.2.0
Tagged commit: _[f7e5c29c5498321f43a0e9dba832227a7a9f4dee](https://code-repo.d4science.org/gCubeSystem/keycloak-client/commit/f7e5c29c5498321f43a0e9dba832227a7a9f4dee)_


- Added OIDC token retrieve for clients [#23076] and UMA token from OIDC token as bearer auth, instead of credentials only (basic auth)


###  grsf-common-library 1.3.2
Tagged commit: _[91c0103dc54421d36d69ddc5ab476e7fa448800c](https://code-repo.d4science.org/gCubeSystem/grsf-common-library/commit/91c0103dc54421d36d69ddc5ab476e7fa448800c)_



- Updated Labels [#23167]


###  authorization-utils 2.0.0
Tagged commit: _[3e91121f09fe3a1a46babfc60a9cca97474d757f](https://code-repo.d4science.org/gCubeSystem/authorization-utils/commit/3e91121f09fe3a1a46babfc60a9cca97474d757f)_

 

- Refactored code to be integrated in Smartgears  [#22871] 
- Fixed getRoles for JWTSecret [#22754]


###  gcat-api 2.2.0
Tagged commit: _[155709bcf84e3a2235299a48d0d59f5643c8d78d](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/155709bcf84e3a2235299a48d0d59f5643c8d78d)_



- Added support to manage configurations [#22658]
- Migrated to ServiceClass corresponding to Maven groupId


###  gcat-client 2.2.0
Tagged commit: _[73efe34e05218b2d33b63a1e75e18bbdc78fd07b](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/73efe34e05218b2d33b63a1e75e18bbdc78fd07b)_



- Added support to manage configurations [#22658]
- Migrated to ServiceClass corresponding to Maven groupId


###  common-smartgears 3.1.5
Tagged commit: _[0bf0708efd887e2de1635acc529416318c0d42a6](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/0bf0708efd887e2de1635acc529416318c0d42a6)_

 - 2022-04-20

- Added roles to ExternalService Info on request handler verification


###  storagehub-application-persistence 3.1.0
Tagged commit: _[ba60734b1cb1fec85824549d59cf6c8427366181](https://code-repo.d4science.org/gCubeSystem/storagehub-application-persistence/commit/ba60734b1cb1fec85824549d59cf6c8427366181)_



- Enhanced range of storagehub-client-library to 2.0.0,3.0.0-SNAPSHOT [#22777]



###  storagehub-client-wrapper 1.2.0
Tagged commit: _[050c554845f85aac92d3d3320833536dd9c6eb18](https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper/commit/050c554845f85aac92d3d3320833536dd9c6eb18)_

 - 2022-05-02

#### Enhancements

- [#23225] Updated the method to read the members of (VRE or Simple) shared folders


###  workspace-uploader 2.1.0
Tagged commit: _[0c903981d57b79486d6e12337d500dd6773ec0b3](https://code-repo.d4science.org/gCubeSystem/workspace-uploader/commit/0c903981d57b79486d6e12337d500dd6773ec0b3)_

 - 2022-05-03

#### Enhancements

- [#23225] Updated the method to read the members of (VRE or Simple) shared folders



###  gcat 2.2.0
Tagged commit: _[c477d8f200b30061d2a4313353a67ffb63b5382d](https://code-repo.d4science.org/gCubeSystem/gcat/commit/c477d8f200b30061d2a4313353a67ffb63b5382d)_



- Switched gcat credentials to new IAM authz [#21628][#22727]
- Added support to manage configurations [#22658][#22742]
- Migrated service to SecretManagerProvider [#22871]
- Migrated to ServiceClass corresponding to Maven groupId
- Added Enunciate to automatically create REST APIs documentation [#23096]
- Fixed 'offset' parameter behaviuor in item listing [#22999]
- Moderation message are sent using gcube messaging system via Social Service [#23117]
- Remove enforcement on approved item for Catalogue-Editor added enforcement to email [#23154]
- ClientID requests are now properly supported [#21903] 



###  grsf-publisher-ws 1.13.1
Tagged commit: _[2f284188c24573adff5e649023d04a8215e54f79](https://code-repo.d4science.org/gCubeSystem/grsf-publisher-ws/commit/2f284188c24573adff5e649023d04a8215e54f79)_



- Aligned code and wiki to the new requirements [#23167]
- Changed group assign strategy [#23211] [#23215]
- Tag are added also to legacy records [#23216]
- Fixed code which generated groups id from name [#23215]


###  dataminer-pool-manager 2.7.1
Tagged commit: _[aa92ba1769796228d1922cc8b103da41e06d0b52](https://code-repo.d4science.org/gCubeSystem/dataminer-pool-manager/commit/aa92ba1769796228d1922cc8b103da41e06d0b52)_

 - 2022-04-06

- Updated to gcube-smartgears-bom.2.1.1 [#23133]



###  workspace-tree-widget 6.35.0
Tagged commit: _[c9e3430b830e58af48209b5a647f68bf8432030a](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/c9e3430b830e58af48209b5a647f68bf8432030a)_

 - 2022-05-03

#### Enhancements

- [#23225] Updated the method to read the members of (VRE or Simple) shared folders


###  workspace 6.28.3
Tagged commit: _[ca30bd4e653fa522efc09fe57ca1a38bdd7805ae](https://code-repo.d4science.org/gCubeSystem/workspace/commit/ca30bd4e653fa522efc09fe57ca1a38bdd7805ae)_

 - 2022-05-02

- [#23225] Just including the enhancement #23225


###  smartgears-distribution 3.4.6
Tagged commit: _[dbeaf97783f656ce3d6de2e2a623b484bdafc576](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/dbeaf97783f656ce3d6de2e2a623b484bdafc576)_

 - 2022-04-20

- added roles to ExternalService Client



---
*generated by the gCube-ReleaseNotes pipeline from report 1189*

*last update Thu May 12 10:03:32 CEST 2022*
