# Release Notes for gCube 5.13.0

---
###  oidc-library 1.3.1
Tagged commit: _[fccbedb299804c0dd2f0bc1fbc6783735687ebc1](https://code-repo.d4science.org/gCubeSystem/oidc-library/commit/fccbedb299804c0dd2f0bc1fbc6783735687ebc1)_


Added  and  roles to the enum (#23623)


###  usermanagement-core 2.5.4
Tagged commit: _[1ec67218a2c4e1be233da2764086690a2a08180c](https://code-repo.d4science.org/gCubeSystem/usermanagement-core/commit/1ec67218a2c4e1be233da2764086690a2a08180c)_

 - 2022-07-06

 - Feature #23622 added Catalogue-Manager and Moderator roles
 

###  storage-manager-core 2.9.1
Tagged commit: _[83bc9fbc2e0e8248eb4f8585efb5d94622c17744](https://code-repo.d4science.org/gCubeSystem/storage-manager-core/commit/83bc9fbc2e0e8248eb4f8585efb5d94622c17744)_

 2022-06-28
  * update to version 2.9.1 in order to have a fixed bom in the latest version of the range


###  social-service-model 1.1.7
Tagged commit: _[588a99d64e29b3f3d745af83a9dfef3928223d9f](https://code-repo.d4science.org/gCubeSystem/social-service-model/commit/588a99d64e29b3f3d745af83a9dfef3928223d9f)_

 - 2022-05-02

 - Added support for workspace and catalogue notifications


###  storage-manager-wrapper 2.5.4
Tagged commit: _[9d4f0a750fb2e82c1c898b691292f13afb0f0882](https://code-repo.d4science.org/gCubeSystem/storage-manager-wrapper/commit/9d4f0a750fb2e82c1c898b691292f13afb0f0882)_

 2022-07-07
  * remove latest from bom
  * replace slf4j-log4j dep with slf4j api



###  social-service-client 1.1.0
Tagged commit: _[fe0e51ac4405b3cb6ca33e7e4055e99ac99ea558](https://code-repo.d4science.org/gCubeSystem/social-service-client/commit/fe0e51ac4405b3cb6ca33e7e4055e99ac99ea558)_

 - 2022-04-06

 - First release
 - Feature #23186 - Full notifications support for social service


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
###  social-networking-library 1.17.0
Tagged commit: _[bb8550dd2233c38bb3d3e6c3140804ab99dee46e](https://code-repo.d4science.org/gCubeSystem/social-networking-library/commit/bb8550dd2233c38bb3d3e6c3140804ab99dee46e)_

 - 2022-05-13

 - Added support for Catalogue notifications
 - Ported to git


###  rmp-common-library 2.8.6
Tagged commit: _[eb617ee395173cb4d8ed47ee123956f4a5ba46b5](https://code-repo.d4science.org/gCubeSystem/rmp-common-library/commit/eb617ee395173cb4d8ed47ee123956f4a5ba46b5)_

 - 2022-06-17

 - Fixed pom for HL portal removal


###  aslsocial 1.8.0
Tagged commit: _[2495cc56ff4709279756c9313c64244e06861cc1](https://code-repo.d4science.org/gCubeSystem/aslsocial/commit/2495cc56ff4709279756c9313c64244e06861cc1)_

 - 2022-05-05

 - fixed some notification methods


###  notifications-common-library 1.5.0
Tagged commit: _[20c03e92e129d3d606fc4b05a3fe08dced6637f7](https://code-repo.d4science.org/gCubeSystem/notifications-common-library/commit/20c03e92e129d3d606fc4b05a3fe08dced6637f7)_

 - 2022-06-25
 
 - Ported to git


###  social-networking-library-ws 2.6.0
Tagged commit: _[ac9a5f39ee38fc56c97e9d3473cd49cfe9110096](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/ac9a5f39ee38fc56c97e9d3473cd49cfe9110096)_

 - 2022-05-16

- Feature #23186, Full notifications support for social service


###  gcat 2.3.0
Tagged commit: _[d40dcc2f95e499a90d652de2ff0264cacc3b97fe](https://code-repo.d4science.org/gCubeSystem/gcat/commit/d40dcc2f95e499a90d652de2ff0264cacc3b97fe)_



- Switched moderation messages to notification [#23317]
- Item listing returns items in the default organization and not in all supported organization



###  ckan-metadata-publisher-widget 2.1.0
Tagged commit: _[166c580dd35951471df4cbc841d735e86fc31697](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/166c580dd35951471df4cbc841d735e86fc31697)_

 - 2022-06-28

**Enhancements**

- [#23491] The final URL provided to go to the item is the Catalogue Portlet URL
- Moved to maven-portal-bom 3.6.4


###  ckan-content-moderator-widget 1.0.1
Tagged commit: _[730ababb45b9f094b061d2e4b385ba5c326580a3](https://code-repo.d4science.org/gCubeSystem/ckan-content-moderator-widget/commit/730ababb45b9f094b061d2e4b385ba5c326580a3)_

 - 2022-06-27

- [#23525] Removed the scope of xml-apis dependency
- Moved to maven-portal-bom v3.6.4


###  grsf-manage-widget 1.6.1
Tagged commit: _[8d9e63b5608ebd40b5590db2a118c34bd49a840b](https://code-repo.d4science.org/gCubeSystem/grsf-manage-widget/commit/8d9e63b5608ebd40b5590db2a118c34bd49a840b)_

 - 2022-06-22

#### Bug fixes

- [#23549] Fixed serialization issue on the GRSFRecordAlreadyManagedStatusException
- [#23561] Fixed Merging Request throws a Null Pointer exception


###  invite-friends-widget 1.6.2
Tagged commit: _[33de64ebec8895e96b455dd39ab2213a6f4a6558](https://code-repo.d4science.org/gCubeSystem/invite-friends-widget/commit/33de64ebec8895e96b455dd39ab2213a6f4a6558)_

 - 2022-07-12

 - Ported to git and GWT 2.8.2


###  workspace-tree-widget 6.35.1
Tagged commit: _[506d129cbc719a6e4bec39690f78c9fda2464a36](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/506d129cbc719a6e4bec39690f78c9fda2464a36)_

 - 2022-06-27

- [#23523] Updated to maven-portal-bom 3.6.4


###  notifications 2.6.0
Tagged commit: _[83c6712d45950e7048fdb7f4bc767544d1b20607](https://code-repo.d4science.org/gCubeSystem/notifications/commit/83c6712d45950e7048fdb7f4bc767544d1b20607)_

 - 2022-05-05

 - Added support for Catalogue notifications
 - Removed support for Tabular Data and Calendar Notifications


###  gcube-ckan-datacatalog 2.2.2
Tagged commit: _[fcc77cec2e4ec8d43d56154d5552cd661812f3b3](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/fcc77cec2e4ec8d43d56154d5552cd661812f3b3)_

 - 2022-06-27

#### Enhancements

- Just to release the Publishing Widget enhancement [#23491]
- [#23525] Removed the scope of xml-apis dependency
- Moved to maven-portal-bom v3.6.4

#### Bug fixes

- Just to release the GRSF Manage Widget bug fixes [#23549], [#23561]


###  share-updates 2.8.1
Tagged commit: _[cc95dc53440170b74502229b9f2c35de4382bff7](https://code-repo.d4science.org/gCubeSystem/share-updates/commit/cc95dc53440170b74502229b9f2c35de4382bff7)_

 -2022-06-15

- Fixed soem deps


###  news-feed 2.8.4
Tagged commit: _[2ed469c066f94c4502eaf3e71de09ee5d89381a0](https://code-repo.d4science.org/gCubeSystem/news-feed/commit/2ed469c066f94c4502eaf3e71de09ee5d89381a0)_

 - 2022-05-15

 - fixed changelog and README


###  create-users-portlet 3.0.2
Tagged commit: _[f1f46e52b81a97a46c86ef28c112666bb4d697ad](https://code-repo.d4science.org/gCubeSystem/create-users-portlet/commit/f1f46e52b81a97a46c86ef28c112666bb4d697ad)_

 - 2022-06-08

 - Released for HL removal in portal


###  messages 2.5.2
Tagged commit: _[cfeef31ec15234ad16c16debcb09e199dc9aec35](https://code-repo.d4science.org/gCubeSystem/messages/commit/cfeef31ec15234ad16c16debcb09e199dc9aec35)_

 -2022-06-16

- Released for removal HL from portal


###  accept-invite-portlet 2.0.1
Tagged commit: _[742e5ca99cd1122376924f166a61d92f0f3db8e1](https://code-repo.d4science.org/gCubeSystem/accept-invite-portlet/commit/742e5ca99cd1122376924f166a61d92f0f3db8e1)_

 - 2022-06-16

 - Release for HL portal removal


###  resource-management-portlet 7.1.1
Tagged commit: _[21234a31613a061dbf4d8ded6a0db58c283fa73e](https://code-repo.d4science.org/gCubeSystem/resource-management-portlet/commit/21234a31613a061dbf4d8ded6a0db58c283fa73e)_

 - 2022-06-15

- Relased due to HL portal removal


###  workspace 6.28.5
Tagged commit: _[9e29ec6c0081ef0657ae17aa2b7883e6269a2c4a](https://code-repo.d4science.org/gCubeSystem/workspace/commit/9e29ec6c0081ef0657ae17aa2b7883e6269a2c4a)_

 - 2022-06-27

- [#23523] Updated to maven-portal-bom 3.6.4
- [#23491] Just to release the Publishing Widget enhancement #23491


###  geoportal-data-entry-app 2.2.1
Tagged commit: _[df9f34be49d542f2213a24e87f288649d505a6ca](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/df9f34be49d542f2213a24e87f288649d505a6ca)_

 - 2022-06-29

#### Enhancements

- [#23593] Shown the published/unpublished field in the table
- Passed to maven-portal-bom 3.6.4


###  invite-members 2.7.1
Tagged commit: _[420e472f0b980aabb36fd8e20808e444fbd4dc20](https://code-repo.d4science.org/gCubeSystem/invite-members/commit/420e472f0b980aabb36fd8e20808e444fbd4dc20)_

 - 2020-12-11

 - Fix for issue on test #23646


###  gcube-portal-bundle 5.1.0
Tagged commit: _[aab899f2f5e0563046d47a4303ce4dc13e714af8](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/aab899f2f5e0563046d47a4303ce4dc13e714af8)_

 - 2022-06-15

-  Removed deprecated home library dependency



---
*generated by the gCube-ReleaseNotes pipeline from report 1232*

*last update Fri Jul 22 16:36:02 CEST 2022*
