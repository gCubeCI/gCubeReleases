# Release Notes for gCube 5.13.1

---
###  registry-publisher 1.3.2
Tagged commit: _[ff7ec8d0b5c77dd640893d36aa0147d9f235b758](https://code-repo.d4science.org/gCubeSystem/registry-publisher/commit/ff7ec8d0b5c77dd640893d36aa0147d9f235b758)_

 2020-07-26
### Fixes
- fix updateVo method. if a generic resource was updated by monitor at root-vo level, the change wasn't propagated properly to the others VOs (#19690)




###  gcat-api 2.3.0
Tagged commit: _[1d1e260104f4f44398e052f94bb3c16b30fcb59e](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/1d1e260104f4f44398e052f94bb3c16b30fcb59e)_



- Aligned constants name used as query parameters
- Added query parameter constant used in item listing to get the whole item instead of just the name [#23691]



###  common-gcube-calls 1.3.1
Tagged commit: _[cb29bb38833b81e7baa6d16e7463aa248b0a7e45](https://code-repo.d4science.org/gCubeSystem/common-gcube-calls/commit/cb29bb38833b81e7baa6d16e7463aa248b0a7e45)_

 - 2020-11-18

- added d4s-user as header



###  storage-manager-core 2.9.2
Tagged commit: _[ac95563480d57fc1b883ee585e5249aa3085a800](https://code-repo.d4science.org/gCubeSystem/storage-manager-core/commit/ac95563480d57fc1b883ee585e5249aa3085a800)_

 2022-09-07
  * restored close() method to IClient
  * add slf4j-simple dependency with test scope
  * update gcube-bom to 2.0.2


###  gcat-client 2.3.0
Tagged commit: _[61d57e8360d509d4ff8f66f3dc4053a3a97ce680](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/61d57e8360d509d4ff8f66f3dc4053a3a97ce680)_



- Removed service discovery to to old service class
- Using renamed constant from gcat-api



###  uri-resolver-manager 1.6.0
Tagged commit: _[a99e6dce8693a9971325d0fcb4396fe75fb49f60](https://code-repo.d4science.org/gCubeSystem/uri-resolver-manager/commit/a99e6dce8693a9971325d0fcb4396fe75fb49f60)_

 - 2022-07-26

**New**

- [#23157] Enhanced to manage the CatalogueResolver with input query string
- Moved to gcube-bom.2.0.2


###  geoportal-client 1.0.8
Tagged commit: _[34a180e84330b33e4ffb8bbb63895fea9cff7906](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/34a180e84330b33e4ffb8bbb63895fea9cff7906)_

Tags 1.0.8 / 5.13.1 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  geoportal-data-common 1.4.1
Tagged commit: _[ff7387299e3bf10d7e5307e269d9d671873f95cf](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/ff7387299e3bf10d7e5307e269d9d671873f95cf)_

 - 2022-07-27

#### New

- [#23704] New maven version range required for (the old) geoportal-client
- Moved to gcube-bom.2.0.2


###  catalogue-util-library 1.2.0
Tagged commit: _[d2970f212da83ec69c2d1dbcb8e11cf40956a150](https://code-repo.d4science.org/gCubeSystem/catalogue-util-library/commit/d2970f212da83ec69c2d1dbcb8e11cf40956a150)_

 - 2022-08-01

**Enhancements**

- [#23692] Optimized the listing and the paging of catalogue items
- Moved to maven-portal-bom 3.6.4


###  ecological-engine-wps-extension 1.1.0
Tagged commit: _[73ec4929a66da05a5283784bf04d16eff764184f](https://code-repo.d4science.org/gCubeSystem/ecological-engine-wps-extension/commit/73ec4929a66da05a5283784bf04d16eff764184f)_

 - 2022-08-26

- Updated bom to latest version [#23769]




###  gcat 2.4.0
Tagged commit: _[02db577bfc9329b7d72c1a724a8027e59f5f5bf8](https://code-repo.d4science.org/gCubeSystem/gcat/commit/02db577bfc9329b7d72c1a724a8027e59f5f5bf8)_



- Added moderation link in moderation message [#23142]
- Added query parameter in item listing to get the whole item instead of just the name [#23691]
- Using renamed constant from gcat-api
- Explict request for approved items return only moderated and approved [#23696]



###  social-networking-library-ws 2.7.0
Tagged commit: _[ea37bcb93ca02561cd971069e94664a88f356d86](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/ea37bcb93ca02561cd971069e94664a88f356d86)_

 - 2022-09-12

- Sphinx documentation added


###  social-networking-library-ws 2.7.0
Tagged commit: _[ea37bcb93ca02561cd971069e94664a88f356d86](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/ea37bcb93ca02561cd971069e94664a88f356d86)_

 - 2022-09-12

- Sphinx documentation added


###  wps 1.3.0
Tagged commit: _[65f53b7769c62d9aecc3cf95964effed0857f919](https://code-repo.d4science.org/gCubeSystem/wps/commit/65f53b7769c62d9aecc3cf95964effed0857f919)_

 - 2022-08-26

- Updated service to common-gcube-calls-1.3.1 [#23769]


###  ckan-content-moderator-widget 1.1.0
Tagged commit: _[0c74612ad76a4c7ddd35d7e4900ff82ca3f7c887](https://code-repo.d4science.org/gCubeSystem/ckan-content-moderator-widget/commit/0c74612ad76a4c7ddd35d7e4900ff82ca3f7c887)_

 - 2022-08-01

#### Enhancements

- [#23692] Optimized the listing and the paging of catalogue items


###  workspace-tree-widget 6.35.2
Tagged commit: _[a7bed05f4c252133c1b02c078cb1f7d11fde4f12](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/a7bed05f4c252133c1b02c078cb1f7d11fde4f12)_

 - 2022-09-14

#### Bug fixed

- [#23676] Fixed - Added extension .txt to ASC files when dowloaded 
- [#23789] Fixed - Notebook files (.ipynb) are downloaded with .txt attached.
- [#23862] Fixed - Downloading folders containing a . in the name, the .zip is appended to file name


###  accounting-dashboard 1.2.2
Tagged commit: _[358add45aac049f71c0def6d0b8640d4bb87814d](https://code-repo.d4science.org/gCubeSystem/accounting-dashboard/commit/358add45aac049f71c0def6d0b8640d4bb87814d)_

 - 2022-06-15

- Updated the scope of xml-apis [#23510]




###  geoportal-data-viewer-app 2.4.0
Tagged commit: _[549086b448ef1f07602187acd4eca195debea695](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/549086b448ef1f07602187acd4eca195debea695)_

 - 2022-09-07

#### Enhancement

- [#23819] Adding layers to principal map belonging to more centroids with the same spatial position


###  gcube-ckan-datacatalog 2.2.3
Tagged commit: _[45b01130aa4e27bc09b6e22942564b97d5155f71](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/45b01130aa4e27bc09b6e22942564b97d5155f71)_

 - 2022-08-01

#### Enhancements

- Just to release the optimization implemented for the Moderation Panel [#23692]


###  workspace 6.28.6
Tagged commit: _[034812180208f3f6a99cb630d4aa03444d1943d9](https://code-repo.d4science.org/gCubeSystem/workspace/commit/034812180208f3f6a99cb630d4aa03444d1943d9)_

 - 2022-09-05

- Just to release the fixes #23676, #23789 implemented in the ws-tree


###  gcube-portal-bundle 5.1.1
Tagged commit: _[3f08450287b1f12fe8deb2bd287939775969a6ee](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/3f08450287b1f12fe8deb2bd287939775969a6ee)_

 - 2022-09-06

-  Updated dependencies in Common CP


###  gcube-portal-bundle 5.1.1
Tagged commit: _[3f08450287b1f12fe8deb2bd287939775969a6ee](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/3f08450287b1f12fe8deb2bd287939775969a6ee)_

 - 2022-09-06

-  Updated dependencies in Common CP



---
*generated by the gCube-ReleaseNotes pipeline from report 1277*

*last update Fri Sep 16 18:21:52 CEST 2022*
