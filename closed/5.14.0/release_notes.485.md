# Release Notes for gCube 5.14.0

## What's New in gCube 5.14

* IAM: the Identity and Access Management service has been upgraded with Keycloak v19.0.2 now supporting 2 Factors Authentication for end users
* Social Service: several features have been added, it is now possible for an IAM client to send notifications, to send attachments through notification / message as well as  setting a Message read / unread. The getPosts API has been enriched  to get posts by PostId and range.
* gCube CMS Suite : The new implementation fully supports Use Case Descriptors driven Project management, from dynamic GUIs to both service and plugins.



---
###  gcube-bom 2.1.0
Tagged commit: _[fb3857ee1f0296d2eaec296396ed32b5b982adf0](https://code-repo.d4science.org/gCubeSystem/gcube-bom/commit/fb3857ee1f0296d2eaec296396ed32b5b982adf0)_



- Enhanced gcube-model version range
- Enhanced information-system-model version range
- Added json-simple library version 1.1.1 [#21692]
- Added storagehub dependencies in range 2,3 [#22730] [#22872]
- Removed authorization-utils
- Enhanced logback-classic version to 1.2.4



###  gcube-smartgears-bom 2.2.0
Tagged commit: _[7028887c4622e94792f55d3212a706c76349a6c7](https://code-repo.d4science.org/gCubeSystem/gcube-smartgears-bom/commit/7028887c4622e94792f55d3212a706c76349a6c7)_



- enhanced gcube-model version range
- enhanced information-system-model version range
- Upgraded gcube-bom version to 2.1.0-SNAPSHOT


###  smart-executor-bom 3.1.0
Tagged commit: _[f9e33b2db171c8b870c54bccdafd9fdfbffd05fc](https://code-repo.d4science.org/gCubeSystem/smart-executor-bom/commit/f9e33b2db171c8b870c54bccdafd9fdfbffd05fc)_



- Upgraded gcube-smartgears-bom to 2.2.0-SNAPSHOT
- added slf4j-api as provided [#23518]
- added libs declared in gcube-smartgears-bom to set them as provided


###  common-authorization 2.5.1
Tagged commit: _[c91630d9215bea867a55094929431d5a86f080fe](https://code-repo.d4science.org/gCubeSystem/common-authorization/commit/c91630d9215bea867a55094929431d5a86f080fe)_



- Added library to support Java 11 JDK


###  information-system-model 5.0.0
Tagged commit: _[87afd8e5a25572269b48b6cd5508381f52ab4fe9](https://code-repo.d4science.org/gCubeSystem/information-system-model/commit/87afd8e5a25572269b48b6cd5508381f52ab4fe9)_



- Added the possibility to get AccessType from Type definition [#20695]
- Solved issue on unmarshalling of unknown types using best known type looking superclasses property [#20694]
- Aligned model with the model defined in Luca Frosini PhD dissertation [#20367]
- Using annotation to document the types with name, description, version and changelog [#20306][#20315]
- Generalise the way the type of a property is defined [#20516]
- Added reserved UUID management
- Added support for context names included in header among UUIDs [#22090]
- Added support for Json Query [#22047]
- Added support for Query Templates [#22091]


###  social-service-model 1.2.0
Tagged commit: _[59f2bab36cf0ba780d260eccc7445e06ef1810b5](https://code-repo.d4science.org/gCubeSystem/social-service-model/commit/59f2bab36cf0ba780d260eccc7445e06ef1810b5)_

 - 2022-05-02

- Feature #23995 dd support for set Message read / unread


###  gcube-model 4.0.0
Tagged commit: _[d086838c6c31804a9d2ee7507bce8ad3230305d2](https://code-repo.d4science.org/gCubeSystem/gcube-model/commit/d086838c6c31804a9d2ee7507bce8ad3230305d2)_



- Aligned model with the model defined in Luca Frosini PhD dissertation [#20367]
- Using annotation to document the types with name, description, version and changelog [#20306][#20315]
- Upgraded gcube-bom version to 2.0.1
- Added STRING as option for IdentificationType in IdentificationFacet 



###  resource-registry-api 4.2.0
Tagged commit: _[fda1b553f65a635247aef1d31d5f614ba82cdb42](https://code-repo.d4science.org/gCubeSystem/resource-registry-api/commit/fda1b553f65a635247aef1d31d5f614ba82cdb42)_



- Added INCLUDE_RELATION_PARAM param in AccessPath used by query APIs [#20298]
- Added support for context names included in header among UUIDs [#22090]
- Added support for Json Query [#22047]
- Added support for Query Templates [#22091]
- Added class to get service URL [#23658]



###  resource-registry-query-template-client 1.0.0
Tagged commit: _[f2b6abb6f0d8a67ba22575033168b936acdf103a](https://code-repo.d4science.org/gCubeSystem/resource-registry-query-template-client/commit/f2b6abb6f0d8a67ba22575033168b936acdf103a)_



- First Release
###  resource-registry-client 4.2.0
Tagged commit: _[973d60eb04e8c8df1bafe2b6d033555f4e9d2fc2](https://code-repo.d4science.org/gCubeSystem/resource-registry-client/commit/973d60eb04e8c8df1bafe2b6d033555f4e9d2fc2)_



- Aligned APIs to other clients [#22011]
- Moved Direction class in information-system-model
- Added support for context names included in header among UUIDs [#22090]
- Added JSON Query API [#22047][#22815]
- Added QueryTemplate safe APIs [#22091][#22815]
- Client gets service URL using resource-registry-api lib utility [#23658]



###  resource-registry-context-client 4.0.1
Tagged commit: _[0cb5a3137630f1294cf316d6e1df04dd7fbe1893](https://code-repo.d4science.org/gCubeSystem/resource-registry-context-client/commit/0cb5a3137630f1294cf316d6e1df04dd7fbe1893)_



- Uniformed raised exception [#21993]
- Client gets service URL using resource-registry-api lib utility [#23658]



###  resource-registry-schema-client 4.1.0
Tagged commit: _[26e2eb5e6dc4b8e5e0347d79aa9615a113913793](https://code-repo.d4science.org/gCubeSystem/resource-registry-schema-client/commit/26e2eb5e6dc4b8e5e0347d79aa9615a113913793)_



- Restrict the interface to accept ERElement classes and not all the Element classes [#21973]
- Client gets service URL using resource-registry-api lib utility [#23658]



###  resource-registry-publisher 4.2.0
Tagged commit: _[40acf5f865bb29894443754d7cb01a9dc1a6e96c](https://code-repo.d4science.org/gCubeSystem/resource-registry-publisher/commit/40acf5f865bb29894443754d7cb01a9dc1a6e96c)_



- Aligned code to Luca Frosini dissertation theory 
- Add/Remove to/from Context return the list of affected instances and not just success or failure code [#20555]
- Aligned client with changes of Sharing REST collection redesign [#20530][#20555]
- Aligned overrided APIs [#21979]
- Getting all the relations of a type it is returned a list of relations and not the list of Resources sources of such relation [#22003]
- Added check for reserved UUID
- Added support for context names included in header among UUIDs [#22090]
- Client gets service URL using resource-registry-api lib utility [#23658]



###  resource-registry-handlers 2.1.0
Tagged commit: _[579aebe298c5d69b977e00bc634cc1b77befb35c](https://code-repo.d4science.org/gCubeSystem/resource-registry-handlers/commit/579aebe298c5d69b977e00bc634cc1b77befb35c)_



- Aligned model with the model defined in Luca Frosini PhD dissertation [#20367]
- Added support for context names included in header among UUIDs [#22090]


###  common-smartgears 3.1.6
Tagged commit: _[d024c129d50e6feae3155dc699637dc9d8fb854c](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/d024c129d50e6feae3155dc699637dc9d8fb854c)_



- Added Linux distribution version [#22933]


###  switch-button-widget 1.2.0
Tagged commit: _[710a070f2e6752d8d07004c8556beef2de1a471b](https://code-repo.d4science.org/gCubeSystem/switch-button-widget/commit/710a070f2e6752d8d07004c8556beef2de1a471b)_

 - 2022-10-28

- [#24053] Fixing issue 
- Moved to maven-portal-bom 3.6.4
- Moved to gwtquery 1.5-beta1
- Moved to gwt.version 2.8.2


###  event-publisher-library 1.1.0
Tagged commit: _[bfcef8b9c3555c8bb578832cdad56c9011b0e9a4](https://code-repo.d4science.org/gCubeSystem/event-publisher-library/commit/bfcef8b9c3555c8bb578832cdad56c9011b0e9a4)_


- Added  to manual send bunch of events and controlling their output status (#23628)


###  smart-executor-api 3.1.0
Tagged commit: _[d2c7b9e0a08dcda55c3175c1c2b27d7e08ce194e](https://code-repo.d4science.org/gCubeSystem/smart-executor-api/commit/d2c7b9e0a08dcda55c3175c1c2b27d7e08ce194e)_



- updated gcube-bom


###  gcat-api 2.3.1
Tagged commit: _[7742300b4224e81bc52783a7a8d9c816e9f9e60e](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/7742300b4224e81bc52783a7a8d9c816e9f9e60e)_



- Added Item Author as system:cm_ metadata to keep track of item author



###  social-networking-library 1.18.0
Tagged commit: _[8ac4057526cc278d4bff8c410eaa3d0f7840742b](https://code-repo.d4science.org/gCubeSystem/social-networking-library/commit/8ac4057526cc278d4bff8c410eaa3d0f7840742b)_

 - 2022-09-20

 - Feature #23891, The (wrongly named) Feed class has been changed to Post, all the methods have been changed accordingly and the old one set as deprecated


###  grsf-common-library 2.0.0
Tagged commit: _[7449490f363ec1168b8b03ca1ad3000f9be7044c](https://code-repo.d4science.org/gCubeSystem/grsf-common-library/commit/7449490f363ec1168b8b03ca1ad3000f9be7044c)_



- Adding support for FAO SDG 14.4.1 Questionnaire source [#23670] 
- Upgraded gcube-bom version to 2.1.0


###  smart-executor-client 3.1.0
Tagged commit: _[67ebbc93da1ed7437532daf4eccb866699ccafd9](https://code-repo.d4science.org/gCubeSystem/smart-executor-client/commit/67ebbc93da1ed7437532daf4eccb866699ccafd9)_



- Ported service to authorization-utils [#22871]


###  social-service-client 1.2.0
Tagged commit: _[1809eec72e73cabf56a17b7fec3a59d526fa1e37](https://code-repo.d4science.org/gCubeSystem/social-service-client/commit/1809eec72e73cabf56a17b7fec3a59d526fa1e37)_

 - 2022-10-20

 - Minor fix on a method name
 - Feature #23887 possibility to get-usernames-by-role in UserClient
 - Feature #23995 Social Service add support for set Message read / unread


###  keycloak-d4science-spi-parent 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  avatar-storage 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  avatar-realm-resource 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  avatar-importer 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  event-listener-provider 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  delete-account 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  identity-provider-mapper 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  keycloak-d4science-script 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  keycloak-d4science-theme 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  ldap-storage-mapper 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  keycloak-d4science-bundle 2.0.0
Tagged commit: _[87a81da0fba49f70b99ff419f2551c6273f32201](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/87a81da0fba49f70b99ff419f2551c6273f32201)_


- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from  repo


###  gcat-client 2.4.0
Tagged commit: _[b2bc868ffc0fe6f8357b6bd7739713e6a7bf045d](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/b2bc868ffc0fe6f8357b6bd7739713e6a7bf045d)_



- Added dependency to be able to compile with JDK 11



###  accounting-summary-access 1.0.3
Tagged commit: _[4bc19bfaec1a4202798648ce101b9cb27180189b](https://code-repo.d4science.org/gCubeSystem/accounting-summary-access/commit/4bc19bfaec1a4202798648ce101b9cb27180189b)_

 - 2020-09-03

Integrated auth-utils

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
###  storagehub-application-persistence 3.2.0
Tagged commit: _[a3ed8a3d8d7691cefcb620a05a3b2a1ad9a4d651](https://code-repo.d4science.org/gCubeSystem/storagehub-application-persistence/commit/a3ed8a3d8d7691cefcb620a05a3b2a1ad9a4d651)_



- Added dependency to be able to compile with JDK 11


###  metadata-profile-discovery 1.1.0
Tagged commit: _[9d0ca3382a33003f55d3c4cd2f6fd3fa5f028c4f](https://code-repo.d4science.org/gCubeSystem/metadata-profile-discovery/commit/9d0ca3382a33003f55d3c4cd2f6fd3fa5f028c4f)_

 - 2022-11-14

#### Enhancement

- [#22890] Extend the MetadataDiscovery logic
- [#23537] Introduce the fieldId in the gCube Metadata Profile 
- Moved to maven-portal-bom 3.6.4
- [#24110] Added dependency required for building with JDK_11


###  geoportal-data-common 2.0.0
Tagged commit: _[265dfaec225b10e9f24e0c6de45e47ff8c09da2f](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/265dfaec225b10e9f24e0c6de45e47ff8c09da2f)_

 - 2022-11-17

#### Enhancements
 
- [#22883] Integrated the configurations exposed by (the new) geoportal-client (>= 1.1.0-SNAPSHOT)
- Passed to UCD/Projects geoportal client/service
- [#23835] Integrated with WORKFLOW_ACTION_LIST_GUI configuration
- [#23909] Declared lombok required for JDK_11
- [#23913] Integrated with GUI presentation configurations read from IS
- [#23927] Integrated with Relationship definition in UCD
- [#23834] Integrated with the create/view/delete Relationship facility
- [#24136] Integrated the temporal dimension on the front-end side


###  common-smartgears-app 2.1.1
Tagged commit: _[4f50539faf18b8043beb539534256a94bd224ab8](https://code-repo.d4science.org/gCubeSystem/common-smartgears-app/commit/4f50539faf18b8043beb539534256a94bd224ab8)_

 - [2022-12-06]

solved a bug on removeContext


###  data-transfer-service 2.0.8
Tagged commit: _[6def156ab9b76a5d365b62f8e3e14b382350a2aa](https://code-repo.d4science.org/gCubeSystem/data-transfer-service.git/commit/6def156ab9b76a5d365b62f8e3e14b382350a2aa)_

 2022-12-06

- Update to integrate last version of common-smartgears-app


###  smart-executor 3.1.0
Tagged commit: _[a7e2fbb31250ab1f3063f6643495a6c65b5bc95c](https://code-repo.d4science.org/gCubeSystem/smart-executor/commit/a7e2fbb31250ab1f3063f6643495a6c65b5bc95c)_



- Ported service to authorization-utils [#22871]


###  accounting-dashboard-harvester-se-plugin 2.2.0
Tagged commit: _[bb3e645932ddc3572fb9ce54c6a30ee7a8850cce](https://code-repo.d4science.org/gCubeSystem/accounting-dashboard-harvester-se-plugin/commit/bb3e645932ddc3572fb9ce54c6a30ee7a8850cce)_



- Switching security to the new IAM [#21904]



###  resource-checker-se-plugin 2.0.1
Tagged commit: _[c3613c9a4502da0756ebd6de150fcce91d32ec82](https://code-repo.d4science.org/gCubeSystem/resource-checker-se-plugin/commit/c3613c9a4502da0756ebd6de150fcce91d32ec82)_



- Removed HomeLibraryWebApp check [#23387]



###  resource-registry 4.1.0
Tagged commit: _[080c849bc040ffa2885e42cd2350b6bf308ed233](https://code-repo.d4science.org/gCubeSystem/resource-registry/commit/080c849bc040ffa2885e42cd2350b6bf308ed233)_

 

- Relation source-target instance types are validated against Relation schema [#7355]
- The instances are validated with the defined schema [#18216]
- Added Types Cache [#18496]
- Superclasses in instances are ordered and does not include internal basic types [#20517]
- Redesigned Sharing REST collection [#20530][#20555] 
- Added the possibility to have a dry run Add/Remove to/from Context [#20530]
- Add/Remove to/from Context return the list of affected instances and not just success or failure code [#20555]
- Generalised the definition of the type of a property [#20516]
- Added type update but the API is not exposed via REST [#20316]
- Prepared Query has been fixed to properly consider polymorphic parameter and provided constraint [#20298]
- Add tests to check the proper behaviour of operation involving Propagation Constraints [#20923]
- The contexts property is not included in header is not requested [#21953]
- When serialising a resource the ConsistsOf does not include source [#22001]
- Getting all the relations of a type it is returned a list of relations and not the list of Resources sources of such relation [#22003]
- Included context names and not only UUIDs in header [#22090]
- Added support for Json Query [#22047]
- Added support for Query Templates [#22091]
- Migrated service to SecretManagerProvider [#22871]
- Fixed bug on results of raw queries [#23662]



###  gcat 2.4.1
Tagged commit: _[f8e6a0cc43b89683b06e0d17f384aaeb0757e086](https://code-repo.d4science.org/gCubeSystem/gcat/commit/f8e6a0cc43b89683b06e0d17f384aaeb0757e086)_



- Integrating Sphinx for documentation [#23833]
- Migrated Social service interaction to social-service-client [#23151]
- Improved author rewrite in case of update [#23851]
- Fixed item listing [#23901]



###  social-networking-library-ws 2.8.0
Tagged commit: _[2b7eb284c53776c8d0a2fbd8e42693635558ec2d](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/2b7eb284c53776c8d0a2fbd8e42693635558ec2d)_

 - 2022-10-20

- Feature #23891 Refactored following updates social lib
- Feature #23847 Social service: temporarily block of notifications for given username(s)
- Feature #23439: Please allow an IAM client to send notifications OPEN
- Feature #23991 Support attachments through notification / message API
- Feature #23995 added support for set Message read / unread
- Feature #24022 added get posts By PostId with range filter parameters and get Comments By PostId


###  grsf-publisher-ws 1.13.3
Tagged commit: _[84272249761feae5a9cb9b60a2990d673b9f9289](https://code-repo.d4science.org/gCubeSystem/grsf-publisher-ws/commit/84272249761feae5a9cb9b60a2990d673b9f9289)_



- Adding support for FAO SDG 14.4.1 Questionnaire source [#23670] 



###  gFeed-Suite 1.0.6
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_


- Pom updates


###  commons 1.0.6
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_


- Pom updates


###  catalogue-plugin-framework 1.0.6
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_


- Pom updates


###  collectors-plugin-framework 1.0.6
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_


- Pom updates


###  test-commons 1.0.6
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_


- Pom updates


###  gCat-Feeder 1.0.6
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_


- Pom updates


###  DataMinerAlgorithmsCrawler 1.0.7
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_

- Lombok version


###  gCat-Controller 1.0.8
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_

- Pom updates

###  oai-harvester 1.0.8
Tagged commit: _[9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/9ed3e1a4546f58aa1da1a5a9e1bd27a148e92dbf)_

 Harvested Object profile update


###  storagehub 1.4.1
Tagged commit: _[0b907277cd6ad433ee4919602cb03995abe3fada](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/0b907277cd6ad433ee4919602cb03995abe3fada)_

 - [2022-11-14]

- added libraries to manage different image format


###  gcube-cms-suite 1.0.2
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-02-24
-Introduced module sdi-plugins
-Introduced module notifications-plugins
-Introduced module dataminer-plugins
-Introduced module images-plugins
-Introduced module ckan-plugin


###  geoportal-common 1.0.9
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

- Minor fixes in model
- Schema and jsonPath support
- Fixes [#22722](https://support.d4science.org/issues/22722)

###  cms-plugin-framework 1.0.1
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-12-07
- Introduced cms-plugin-framework
- Introduced concessioni use case
- Fixed internal group ids


###  cms-test-commons 1.0.2
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-02-24
-Introduced module sdi-plugins
-Introduced module notifications-plugins
-Introduced module dataminer-plugins
-Introduced module images-plugins
-Introduced module ckan-plugin


###  concessioni-model 1.0.1
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-12-07
- Introduced cms-plugin-framework
- Introduced concessioni use case
- Fixed internal group ids


###  concessioni-lifecycle 1.0.1
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-12-07
- Introduced cms-plugin-framework
- Introduced concessioni use case
- Fixed internal group ids


###  sdi-plugins 1.0.0
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-2-11
- First release
###  geoportal-service 1.0.10
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

- Plugin Management
- Profiled Document : FileSet Registration 


###  notifications-plugins 1.0.0
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-2-11
- First release
###  dataminer-plugins 1.0.0
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-2-11
- First release
###  images-plugins 1.0.0
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-2-11
- First release
###  ckan-plugins 1.0.0
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-2-11
- First release
###  use-cases 1.0.2
Tagged commit: _[3fb1b22ff5139b0949d12a661c6e7842d75f6c6a](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/3fb1b22ff5139b0949d12a661c6e7842d75f6c6a)_

 - 2021-02-24
-Introduced module sdi-plugins
-Introduced module notifications-plugins
-Introduced module dataminer-plugins
-Introduced module images-plugins
-Introduced module ckan-plugin


###  whn-manager 2.0.1
Tagged commit: _[15cd6954e399cfeded3a795163b4c0856a140c40](https://code-repo.d4science.org/gCubeSystem/whn-manager/commit/15cd6954e399cfeded3a795163b4c0856a140c40)_

 - 2022-11-29

- First Release with git
- added exclude authorization on gcube-apps.xml
###  ckan-metadata-publisher-widget 2.1.1
Tagged commit: _[af9ffa150b36562fad8732a5bde0df4e53085520](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/af9ffa150b36562fad8732a5bde0df4e53085520)_

 - 2022-10-27

**Enhancements**

- [#24038] Moved to GWT 2.9.0


###  grsf-manage-widget 1.7.0
Tagged commit: _[5c8e230e70633b1a21105486dbd932d2a2c4e3b7](https://code-repo.d4science.org/gCubeSystem/grsf-manage-widget/commit/5c8e230e70633b1a21105486dbd932d2a2c4e3b7)_

 - 2022-09-27

#### Enhancements

- [#23809] Passed to grsf-common-library.2.0.0[-SNAPSHOT]
- [#23811#note-6] Passed to GWT_2.9


###  ckan2zenodo-publisher-widget 1.1.2
Tagged commit: _[03586eb5d67770317931e6e5929fdc24b1038166](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-publisher-widget/commit/03586eb5d67770317931e6e5929fdc24b1038166)_

 - 2022-10-27

**Enhancements**

- [#24038] Moved to GWT 2.9.0
- Moved to maven-portal-bom 3.6.4


###  ckan-content-moderator-widget 1.1.1
Tagged commit: _[f598f29ca72e924cae832f16d4b62b12123e47c7](https://code-repo.d4science.org/gCubeSystem/ckan-content-moderator-widget/commit/f598f29ca72e924cae832f16d4b62b12123e47c7)_

 - 2022-10-27

#### Fixing

- GWT-Servlet at provided


###  catalogue-sharing-widget 1.2.1
Tagged commit: _[1c2f536c59772274d0eac319407f8569515e9426](https://code-repo.d4science.org/gCubeSystem/catalogue-sharing-widget/commit/1c2f536c59772274d0eac319407f8569515e9426)_

 - 2021-10-27

- [#24038] Moved to GWT 2.9.0


###  metadata-profile-form-builder-widget 2.0.0
Tagged commit: _[c1802685b8bb68e97e9fa4047fdee6ca56971bdd](https://code-repo.d4science.org/gCubeSystem/metadata-profile-form-builder-widget/commit/c1802685b8bb68e97e9fa4047fdee6ca56971bdd)_

 - 2022-11-14

#### Enhancements

- [#23188] Overloaded the method getProfilesInTheScope(forName)
- [#22890] Including the set/get currentValue for the Update facility
- [#23188] Advanced the MultipleDilaogUpload with a more complex object
- [#23544] Integrated with the fieldId added to gCube Metadata Profile
- [#24111] Added dependency required for building with JDK_11
- Moved to GWT 2.9
- Moved to maven-portal-bom 3.6.4



###  vre-members 2.4.0
Tagged commit: _[059029453e467106bf31610c1884e7798f17d205](https://code-repo.d4science.org/gCubeSystem/vre-members/commit/059029453e467106bf31610c1884e7798f17d205)_

 - 2022-09-22

- Ported to GWT 2.10.0 and Java 11
- Ported to Git


###  messages 2.6.0
Tagged commit: _[d98116cc37f942ac73bb726f1f317821418110a7](https://code-repo.d4science.org/gCubeSystem/messages/commit/d98116cc37f942ac73bb726f1f317821418110a7)_

 - 2022-09-22

 - Fix Bug #23898:  open with email addresse preset is not working anymore 
 - Moved to OpenJDK11 
 - Moved to GWT 2.9.0


###  gcube-ckan-datacatalog 2.2.4
Tagged commit: _[c4c68de7ba7fdfd3f23c2d5df00d86769ceb9976](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/c4c68de7ba7fdfd3f23c2d5df00d86769ceb9976)_

 - 2022-10-27

#### Enhancements

- [#24038] Moved to GWT 2.9
- [#23811] Just to release the grsf-manage-widget



###  geoportal-data-viewer-app 3.0.0
Tagged commit: _[78cc536f54366213363bdabe0421b7199cea4314](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/78cc536f54366213363bdabe0421b7199cea4314)_

 - 2022-11-28

- [#23940] Passed to CMS-UCD
- [#23941] Implemented and integrated the DTO Project/Document per UCD config and gCube Metadata Profiles
- [#23953] Passed the 'Search facility' to CMS-UCD model
- [#23954] Passed the 'Details Panel facility' to CMS-UCD model
- [#23955] Implemented the Temporal facility to navigate temporal relations among (JSON) Projects
- [#24028] Passed the spatial dimension to CMS-Project model
- [#24136] Integrated the temporal dimension on the front-end side
- [#24195] Integrated the temporal query


###  geoportal-data-entry-app 3.0.0
Tagged commit: _[ac95b4616a22131011edadfa231ecdbb4dfe796d](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/ac95b4616a22131011edadfa231ecdbb4dfe796d)_

 - 2022-11-09

#### Enhancements

- [#22684] Migrated to geoportal-data-entry-app configuration for UCD
- [#23587] GUI model viewer passed to tree data structure
- [#22883] Integrated with (the new) geoportal-client (>= 1.1.0-SNAPSHOT)
- [#22685] Migrated to geoportal-data-list configuration for UCD
- [#23784] Migrated list and reload, searching and ordering functionalities
- [#23785] Migrated the GNA functionalities
- [#23834] Integrated with the create/view/delete Relationship facility
- [#23913] Integrated with GUI presentation configurations read from IS
- [#23926] Integrated a Post Creation Action in the UCD and manage it
- [#24136] Integrated the temporal dimension on the front-end side


###  smartgears-distribution 3.4.7
Tagged commit: _[9ff239d52058a9fac3b3dd62d3818d424a872d8c](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/9ff239d52058a9fac3b3dd62d3818d424a872d8c)_



- upgraded gcube-smartgears-bom
- new common-smartgears version 


###  gcube-portal-bundle 5.1.2
Tagged commit: _[3e52e050b0404e92157176249c28a620e7c9bd67](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/3e52e050b0404e92157176249c28a620e7c9bd67)_

 - 2022-11-04

-  Updated social lib dependencies in Common CP




---
*generated by the gCube-ReleaseNotes pipeline from report 1332*

*last update Wed Dec 07 15:01:36 CET 2022*
