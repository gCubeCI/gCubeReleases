# Release Notes for gCube 5.14.2

---
###  geoportal-data-common 2.0.1
Tagged commit: _[ebdf9714c9cc311a30405ffe5f6ced2e8de115e6](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/ebdf9714c9cc311a30405ffe5f6ced2e8de115e6)_

 - 2022-01-19

#### Bug fixes

- [#24263] Fixing JSON library v20090211
- [#24432] Fixing serialization issue using LinkedHashMap<String, String> instead of LinkedHashMap<String, Object>.
- [#24432] Added a trim for returning value to solve a space as suffix into returned date (fixing the data displayed in the Timeline Widget)


###  sis-geotk-plugin 1.3.1
Tagged commit: _[f031d682244260dfb86e4e284266e13bb124a8e6](https://code-repo.d4science.org/gCubeSystem/sis-geotk-plugin.git/commit/f031d682244260dfb86e4e284266e13bb124a8e6)_

 2021-03-25
Fixes [#20760](https://support.d4science.org/issues/20760) https endpoints



###  gcube-cms-suite 1.0.4
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

 - 2023-01-10
- Updated plugin framework


###  geoportal-common 1.0.11
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates
###  cms-plugin-framework 1.0.3
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- UserUtils in framework


###  cms-test-commons 1.0.4
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

 - 2023-01-10
- Pom updates


###  default-lc-managers 1.0.1
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

 - 2023-01-10
- Pom updates


###  concessioni-model 1.0.3
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

 - 2023-01-10
- Pom updates


###  concessioni-lifecycle 1.0.3
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

  - 2023-01-10
- Pom updates
- Fixes default access


###  sdi-plugins 1.0.2
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates


###  geoportal-service 1.0.12
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Refactored UserUtils into framework plugin

###  geoportal-client 1.1.2
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates

###  notifications-plugins 1.0.2
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates


###  dataminer-plugins 1.0.2
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates


###  images-plugins 1.0.2
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates


###  ckan-plugins 1.0.2
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

- 2023-01-10
- Pom updates


###  use-cases 1.0.4
Tagged commit: _[1eaaf0f176da0226aa8269ba6b7728a57e750136](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1eaaf0f176da0226aa8269ba6b7728a57e750136)_

 - 2023-01-10
- Pom updates


###  geoportal-data-entry-app 3.0.1
Tagged commit: _[1844c32a4b1dd1820a88e56e31fa1ffbf6a9ec99](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/1844c32a4b1dd1820a88e56e31fa1ffbf6a9ec99)_

 - 2023-01-19

#### Fixes

- [#24281] Fixed filtering selection label
- [#24049] Fixed Show on Map facility vs Chrome browser
- [#24432] Fixing serialization issue using LinkedHashMap<String, String> instead of LinkedHashMap<String, Object>


###  geoportal-data-viewer-app 3.1.0
Tagged commit: _[69e5216645125d5507a1c7fe0ad37c46d9078f1e](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/69e5216645125d5507a1c7fe0ad37c46d9078f1e)_

 - 2023-01-12

#### Enhancements
- [#24300] Improved the GUI of the search functionality when multiple collections are available
- [#24404] Improved the handlers of events: Search, Locate
- Improved the user experience providing an animated map and zoom to layers facility

#### Fixes
- Reduced the BBOX when resolving a project by share link
- GNA Project binding with automatically adding of the layers to Map
- [#24390] Read the access policy from the fileset json section
- [#24432] Fixing serialization issue using LinkedHashMap<String, String> instead of LinkedHashMap<String, Object>



---
*generated by the gCube-ReleaseNotes pipeline from report 1368*

*last update Wed Jan 25 09:47:36 CET 2023*
