# Release Notes for gCube 5.15.0

## What's New in gCube 5.15

* Catalogue framework: new features supporting content moderation, namely a catalogue moderator role is supported to explicitly manage submitted items (approve, request for changes or reject) before their publishing; 
* Social Service: REST API enhanced to support access to post by ID, like and comments;
* VLab Display Portlet: new component to list and give access to a selected set of VREs and highlight the VRE access policy;
* SmartGears v 3.5: the set of java libraries turning a servlet-based container and application into a gCube entity have been reinforced to deal with new gCube core services


---
###  gcube-bom 2.2.0
Tagged commit: _[193d2d753f1ed294dbf62fb2a210c7ea14e2ba87](https://code-repo.d4science.org/gCubeSystem/gcube-bom/commit/193d2d753f1ed294dbf62fb2a210c7ea14e2ba87)_



- Enhanced information-system-model version range
- Enhanced gcube-model version lower bound of range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Added common-utility-sg3



###  gcube-smartgears-bom 2.3.0
Tagged commit: _[431e0bbe95f455d2d680a9236f3e355e04043bd6](https://code-repo.d4science.org/gCubeSystem/gcube-smartgears-bom/commit/431e0bbe95f455d2d680a9236f3e355e04043bd6)_



- Enhanced information-system-model version range
- Enhanced gcube-model version lower bound of range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Added common-utility-sg3



###  common-utility-sg3 1.0.0
Tagged commit: _[3ea656f73540f25a50c73618005d69bcacdcb873](https://code-repo.d4science.org/gCubeSystem/common-utility-sg3/commit/3ea656f73540f25a50c73618005d69bcacdcb873)_



- First Version
###  authorization-utils 2.1.0
Tagged commit: _[ae07d71dd14563f9ec66e46528bbfdb24ef73e19](https://code-repo.d4science.org/gCubeSystem/authorization-utils/commit/ae07d71dd14563f9ec66e46528bbfdb24ef73e19)_



- Added remove() method in SecretManagerProvider
- Enhanced gcube-bom version


###  software-versions-processor-lib 1.0.0
Tagged commit: _[bb37c52147583933f2f86f767c83dce8de00442f](https://code-repo.d4science.org/gCubeSystem/software-versions-processor-lib/commit/bb37c52147583933f2f86f767c83dce8de00442f)_



- First Release
###  gcat-api 2.3.2
Tagged commit: _[a0ac2b3b9ccfd6b9569f2eb7578d77532caf0e03](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/a0ac2b3b9ccfd6b9569f2eb7578d77532caf0e03)_



- Library modified to be compliant with both Smartgears 3 and 4
- Added support for JSON:API on listing method of 'items' collection [#24601]
- Added support for JSON:API on listing method of 'licenses' collection [#24601]



###  information-system-model 6.0.0
Tagged commit: _[85f75fff2adc8c27553b20ab286458f67b141349](https://code-repo.d4science.org/gCubeSystem/information-system-model/commit/85f75fff2adc8c27553b20ab286458f67b141349)_



- Modified models discovery to make it easier and more powerful [#24548]
- Discovery create trees of discovered types both globally and model based [#24548]
- Added possibility to declare a type Final which means that it cannot be extended [#24554]
- Fixed default value of propagation constraint of remove action for ConsistsOf to 'cascade' [#24223]
- Added delete propagation constraint property [#24222]
- Removed Encrypted Property Type and added Vault instead [#24655]



###  gcube-model 4.1.0
Tagged commit: _[935080cc2050f9a9dc0a20dfde849639d96c2104](https://code-repo.d4science.org/gCubeSystem/gcube-model/commit/935080cc2050f9a9dc0a20dfde849639d96c2104)_



- Added model name to registration provider



###  resource-registry-api 4.3.0
Tagged commit: _[ee6fb393d3ec0e42a02e825b9b1ec7e530e71e35](https://code-repo.d4science.org/gCubeSystem/resource-registry-api/commit/ee6fb393d3ec0e42a02e825b9b1ec7e530e71e35)_



- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)



###  resource-registry-query-template-client 1.1.0
Tagged commit: _[38b3d0bbb7109cf768ed5da4115797c5c789f6b3](https://code-repo.d4science.org/gCubeSystem/resource-registry-query-template-client/commit/38b3d0bbb7109cf768ed5da4115797c5c789f6b3)_



- Fixed result of run from List<Entity> to List<ERElement>
- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context



###  resource-registry-client 4.3.0
Tagged commit: _[e9611dcafc32d0a4bf17a6fbdbcaff1a3c6ef9f5](https://code-repo.d4science.org/gCubeSystem/resource-registry-client/commit/e9611dcafc32d0a4bf17a6fbdbcaff1a3c6ef9f5)_



- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context



###  resource-registry-context-client 4.1.0
Tagged commit: _[0deeb0a05a0183baeb4eee0fde10daf0b4df874b](https://code-repo.d4science.org/gCubeSystem/resource-registry-context-client/commit/0deeb0a05a0183baeb4eee0fde10daf0b4df874b)_



- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context



###  resource-registry-schema-client 4.2.0
Tagged commit: _[eb60093daf2e91d9834945720568e2dc0a76e9e0](https://code-repo.d4science.org/gCubeSystem/resource-registry-schema-client/commit/eb60093daf2e91d9834945720568e2dc0a76e9e0)_



- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context



###  resource-registry-publisher 4.3.0
Tagged commit: _[8f3c8d21a1f38c74bf56863c0f9d2224c0ca2650](https://code-repo.d4science.org/gCubeSystem/resource-registry-publisher/commit/8f3c8d21a1f38c74bf56863c0f9d2224c0ca2650)_



- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context



###  resource-registry-handlers 2.2.0
Tagged commit: _[e9430f5d93f95fc9c97eb446c7b7c24673fffd0b](https://code-repo.d4science.org/gCubeSystem/resource-registry-handlers/commit/e9430f5d93f95fc9c97eb446c7b7c24673fffd0b)_



- Enhanced gcube-smartgears-bom version



###  gcat-client 2.4.1
Tagged commit: _[54d291a142621944618931a96d13b57606f93f0a](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/54d291a142621944618931a96d13b57606f93f0a)_



- Added support for JSON:API on 'licenses' collection [#24601]
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)



###  catalogue-util-library 1.3.0
Tagged commit: _[b17fb3765a8b71d58c74a59f782ad7cf3f8028b2](https://code-repo.d4science.org/gCubeSystem/catalogue-util-library/commit/b17fb3765a8b71d58c74a59f782ad7cf3f8028b2)_

 - 2023-02-06

**Enhancements**

- [#23903] Catalogue Moderation: allow to send a message to the moderators
- [#23692] Changed interface 'approveItem' adding the parameter socialPost


###  geoportal-data-common 2.0.3
Tagged commit: _[6dd3cb4b79d6e0637af5b6c1d944759b0b545132](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/6dd3cb4b79d6e0637af5b6c1d944759b0b545132)_

 - 2023-03-07

**Enhancements**

- [#24569] Added the phase DRAFT in the enum (also required for #24571)


###  default-lc-managers 1.1.1
Tagged commit: _[b2d696acf3623b3ff80f3a8f70af48e738e12fa8](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/b2d696acf3623b3ff80f3a8f70af48e738e12fa8)_

 - 2023-03-06

- [#24570] Integrated the UnPublish operation 


###  concessioni-lifecycle 1.1.0
Tagged commit: _[b2d696acf3623b3ff80f3a8f70af48e738e12fa8](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/b2d696acf3623b3ff80f3a8f70af48e738e12fa8)_

 - 2023-03-13
- [#24754] Fixed hide/show project ids on the latest node of the relation chain


###  notifications-plugins 1.0.3
Tagged commit: _[b2d696acf3623b3ff80f3a8f70af48e738e12fa8](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/b2d696acf3623b3ff80f3a8f70af48e738e12fa8)_

 - 2023-03-06
- [#24702] Fixed the default-lc-managers dependency


###  sdi-plugins 1.0.4
Tagged commit: _[b2d696acf3623b3ff80f3a8f70af48e738e12fa8](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/b2d696acf3623b3ff80f3a8f70af48e738e12fa8)_

 - 2023-03-06
- Fixed the import from joda-time to java.time
- [#24702] Fixed the default-lc-managers dependency
- Fixed log 


###  social-networking-library-ws 2.9.0
Tagged commit: _[2e6d0c75215b49dcfa33d667b350483c66962a2d](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/2e6d0c75215b49dcfa33d667b350483c66962a2d)_

 - 2023-02-13

 - Feature #24456 social-networking rest api endpoints (comments, Likes, GetPost)


###  gcat 2.5.0
Tagged commit: _[9f14bb3b62c4362a01622577af19e11bfba232d7](https://code-repo.d4science.org/gCubeSystem/gcat/commit/9f14bb3b62c4362a01622577af19e11bfba232d7)_



- Switched from commons-lang3 to commons-lang to avoid duplicates
- Set resource-registry-publisher dependency scope to provided
- Fixed RequestFilter to avoid to remove info to Smartgears
- Switching to Facet Based IS [#20225]
- Delete of item in a moderated catalogue produces a notification [#24305]
- The user performing create/update item in moderated catalogue receive confirmation via notification of the action [#23575]
- Enhanced gcube-smartgears-bom version
- Added support for JSON:API on 'licenses' collection [#24601]



###  resource-registry 4.2.0
Tagged commit: _[f382c91db7bd45d4661e8422cf8e8fdd6f844dc4](https://code-repo.d4science.org/gCubeSystem/resource-registry/commit/f382c91db7bd45d4661e8422cf8e8fdd6f844dc4)_



- Fixed bug on JSONQuery for Facets which does not have any properties to match [#24237]
- Fixed bug on JSONQuery for IsRelatedTo relations indicating both source and target resources [#24264]
- Fixed bug on returned boolean values as string [#24240]  
- Enabled array properties [#24225] 
- Using delete in propagation contraint as action indication for delete operation [#24301]
- Fixed default value of propagation constraint of remove action for ConsistsOf to 'cascade' [#24223]
- Removed Encrypted Property Type and added Vault instead [#24655]
- Enhanced gcube-smartgears-bom version
 


###  geoportal-service 1.0.13
Tagged commit: _[b2d696acf3623b3ff80f3a8f70af48e738e12fa8](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/b2d696acf3623b3ff80f3a8f70af48e738e12fa8)_

 - 2023-02-23
- Integrating new facilities and bug fixing released in the Plugins

###  icproxy 1.4.0
Tagged commit: _[86e9db223027d692c2e7699051f281006979d5ab](https://code-repo.d4science.org/gCubeSystem/ic-proxy/commit/86e9db223027d692c2e7699051f281006979d5ab)_

 - [2023-03-09]

- Feature #24254 simple serviceEndpoint creation 
- add support for UMA token
- Feature #24253 add support for decrypted ServiceEndpoint
- update lombok library to 1.18.4 with scope provided



###  ckan-content-moderator-widget 1.2.0
Tagged commit: _[ea78eafec28f1092d02cf8665181f80d138dc1fe](https://code-repo.d4science.org/gCubeSystem/ckan-content-moderator-widget/commit/ea78eafec28f1092d02cf8665181f80d138dc1fe)_

 - 2023-01-19

**Enhancements**

- [#23903] Catalogue Moderation: allow to send a message to the moderators on reject to pending operation
- [#24519] Added social_post=true to approve request


###  ckan-metadata-publisher-widget 2.1.2
Tagged commit: _[316912b060dcef8f0d5a23fb2bd50fb79437dab2](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/316912b060dcef8f0d5a23fb2bd50fb79437dab2)_

 - 2023-03-10

**Bug fixes**

- [#24744] Error null after dataset created



###  geoportal-data-viewer-app 3.3.0
Tagged commit: _[5ec86d1a8d74e6e6bb2ffcbf266eb511c50e860a](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/5ec86d1a8d74e6e6bb2ffcbf266eb511c50e860a)_

 - 2023-02-20

#### Fixes

- [#24632] Returning the public layer index if the private is missing
- [#24632] No temporal info shown if the temporal extent is missing


###  geoportal-data-entry-app 3.1.0
Tagged commit: _[217df66cb3fdb3b47c056efd1220fc877b437fca](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/217df66cb3fdb3b47c056efd1220fc877b437fca)_

 - 2023-03-06

#### Enhancements

- [#24569] The Edit operation is available only in the DRAFT phase
- [#24571] The Create Relation operation is available only in the DRAFT phase


###  gcube-ckan-datacatalog 2.2.5
Tagged commit: _[9422e63002ef9943c7431c514c5bcd7beabafe40](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/9422e63002ef9943c7431c514c5bcd7beabafe40)_

 - 2023-03-02

#### Enhancements

- [#23903] Catalogue Moderation: allow to send a message to the moderators
- [#24309] Inform the VRE users that the catalogue is controlled/moderated 


###  vlabs-display-portlet 1.0.0
Tagged commit: _[327954f20eeb0a8fd911b196c8defbe9eb54aa60](https://code-repo.d4science.org/gCubeSystem/vlabs-display-portlet/commit/327954f20eeb0a8fd911b196c8defbe9eb54aa60)_

 - 2023-03-08

First Release
###  catalogue-badge-portlet 1.1.2
Tagged commit: _[8516f986be2fac680489334d0f4590a9ecb3d95a](https://code-repo.d4science.org/gCubeSystem/catalogue-badge-portlet/commit/8516f986be2fac680489334d0f4590a9ecb3d95a)_

 - 2023-02-13

- bug fix


###  smartgears-distribution 3.5.0
Tagged commit: _[932f7d4351712bd54a6d1527960092fbd2ee2472](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/932f7d4351712bd54a6d1527960092fbd2ee2472)_



- upgraded gcube-smartgears-bom
- new common-smartgears version
- added resource-registry-handlers to distribution 


###  smartgears-distribution-bundle 3.5.0
Tagged commit: _[3033fbf174402b69c653c2666ef039e4888133bb](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution-bundle/commit/3033fbf174402b69c653c2666ef039e4888133bb)_



- Creating distribution bundle for smartgears-distribution 3.5.0-SNAPSHOT



---
*generated by the gCube-ReleaseNotes pipeline from report 1415*

*last update Tue Mar 21 11:51:21 CET 2023*
