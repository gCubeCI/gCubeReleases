# Release Notes for gCube 5.16.0

## What's New in gCube 5.16

* A new version of Geoportal with several enhancements, e.g., statistics on usage, UX-related improvements
* A revised version of the Catalogue service, e.g., reinforcing the publish to Zenodo facility, the notification mechanism for moderated catalogues
* Resource Registry enhancements and uptake, e.g., E/R format revised, support for retrieving rich metadata

---
###  gcube-bom 3.0.0
Tagged commit: _[26b1c7d7ace8b4ce48da93c6c280a19f89d04124](https://code-repo.d4science.org/gCubeSystem/gcube-bom/commit/26b1c7d7ace8b4ce48da93c6c280a19f89d04124)_



- Moved to smartgears 4.0.0 
- Keycloak client added
- Enhanced ranges of common libraries
- Enhanced ranges of gx-rest libraries
- Enhanced ranges of storage-hub libraries
- Enhanced ranges of authorization libraries
- Added new libraries for authorization
- Replaced common-utility-sg3 with common-utility-sg4
- Enhanced information-system-model version range
- Enhanced gcube-model version version range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Enhanced resource-registry-context-client lower bound of range
- Enhanced resource-registry-query-template-client lower bound of range
- Enhanced resource-registry-schema-client lower bound of range



###  gcube-smartgears-bom 3.0.0
Tagged commit: _[4b25dcf712c5e21dfdeaea8a9d2d2840fbab554d](https://code-repo.d4science.org/gCubeSystem/gcube-smartgears-bom/commit/4b25dcf712c5e21dfdeaea8a9d2d2840fbab554d)_



- Moved to smartgears 4.0.0 
- Enhanced ranges of common libraries
- Enhanced ranges of gx-rest libraries
- Enhanced ranges of authorization libraries
- Replaced common-utility-sg3 with common-utility-sg4
- Enhanced information-system-model version range
- Enhanced gcube-model version version range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range


###  smart-executor-bom 3.2.0
Tagged commit: _[ddb0e70f016da729cd8ce7c6f85e913dafc346cf](https://code-repo.d4science.org/gCubeSystem/smart-executor-bom/commit/ddb0e70f016da729cd8ce7c6f85e913dafc346cf)_



- Upgraded gcube-smartgears-bom to 2.5.0



###  keycloak-d4science-spi-parent 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  avatar-storage 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  avatar-realm-resource 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  avatar-importer 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  event-listener-provider 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  delete-account 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  identity-provider-mapper 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  keycloak-d4science-script 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  keycloak-d4science-theme 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  ldap-storage-mapper 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  protocol-mapper 2.1.0
Tagged commit: _[cfa92d73e50dfc2de06212bcec995f8c8e605f24](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/cfa92d73e50dfc2de06212bcec995f8c8e605f24)_


- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)


###  keycloak-client 2.0.0
Tagged commit: _[86c3887e7650ac00fbb0b9d67a274717efa3472e](https://code-repo.d4science.org/gCubeSystem/keycloak-client/commit/86c3887e7650ac00fbb0b9d67a274717efa3472e)_


- Removed the discovery functionality to be compatible with SmartGears.v4 and moved to the new library  that will provide the backward compatibility. (#23478).
- Fixed typo in  class for  method (#23654)
- Added predictive infrastructure URL support based on context (and on context and realm if the target realm is not the default one) and overloaded all methods that take the URL as argument with the context. (#23655)
- Added support for the use of the custom Keycloak's D4S mapper that maps/shrink the  (and optionally also the resource access) to the value requested via  HTTP header.
- Added support of password grant flow (corresponding to the now deprecated OAuth2 flow: Resource Owner Password Credentials grant) also for specific context/audience by using the specific D4S mapper. (#25291)
- Added new  class to perform token request for user in one shot and without the need to provide the  parameter (#25291). Only ,  and  are required. (#25291)


###  keycloak-client-legacy-is 1.0.0
Tagged commit: _[6b7b9c1f3465cf75f22f262e1c101f7b2cecec4f](https://code-repo.d4science.org/gCubeSystem/keycloak-client-legacy-is/commit/6b7b9c1f3465cf75f22f262e1c101f7b2cecec4f)_


- First release as refactoring of the  library; moved the discovery functionality (to be compatible with SGv4) here to provide the backward compatibility. (#23478)
###  authorization-utils 2.2.0
Tagged commit: _[0f1a5ad24649c442d9189473a2c80d0287fbbf33](https://code-repo.d4science.org/gCubeSystem/authorization-utils/commit/0f1a5ad24649c442d9189473a2c80d0287fbbf33)_



- Switched to the new version of keycloak-client [#25295]


###  information-system-model 7.0.0
Tagged commit: _[f4761c0a6f6d55010bc55bcf83f6ac6dc6264217](https://code-repo.d4science.org/gCubeSystem/information-system-model/commit/f4761c0a6f6d55010bc55bcf83f6ac6dc6264217)_



- Reorganized E/R format [#24992]
- Added missing expected type field when the provided type does not have the exact class in the classpath 



###  common-utility-sg3 1.0.1
Tagged commit: _[d668721d09897d2c3cc65a28eaed58fbae29c9bf](https://code-repo.d4science.org/gCubeSystem/common-utility-sg3/commit/d668721d09897d2c3cc65a28eaed58fbae29c9bf)_



- Fixed issue on getCurrentContextFullName


###  storagehub-application-persistence 3.3.0
Tagged commit: _[76be3d6533280dccd73f8469fd54c35bbc60ae00](https://code-repo.d4science.org/gCubeSystem/storagehub-application-persistence/commit/76be3d6533280dccd73f8469fd54c35bbc60ae00)_



- Added GENERATING_APPLICATION_NAME and GENERATING_APPLICATION_METADATA_VERSION to be used in Metadata



###  geo-utility 1.2.0
Tagged commit: _[c51d61ab7b25d1c2631b153b77b41b791504630e](https://code-repo.d4science.org/gCubeSystem/geo-utility/commit/c51d61ab7b25d1c2631b153b77b41b791504630e)_

 - 2022-02-01

- [#22747] Added WFS utilities


###  gcube-model 5.0.0
Tagged commit: _[a2f7cb8d2acd13f55b04204218460e454b1f8fd8](https://code-repo.d4science.org/gCubeSystem/gcube-model/commit/a2f7cb8d2acd13f55b04204218460e454b1f8fd8)_



- DescriptiveMetadataFacet methods has been renamed to comply with reorganized E/R format [#24992]



###  resource-registry-api 5.0.0
Tagged commit: _[f35c1564b89040832390075678be1f99b088234d](https://code-repo.d4science.org/gCubeSystem/resource-registry-api/commit/f35c1564b89040832390075678be1f99b088234d)_



- Migrate code to reorganized E/R format [#24992]
- Added query parameters to request Metadata [#25040]



###  resource-registry-query-template-client 1.1.1
Tagged commit: _[d8cb6081bf62461272171bfb1d8ff4d799d600f9](https://code-repo.d4science.org/gCubeSystem/resource-registry-query-template-client/commit/d8cb6081bf62461272171bfb1d8ff4d799d600f9)_



- Migrated code to reorganized E/R format [#24992]



###  resource-registry-client 4.4.0
Tagged commit: _[de4791849700d2ba9a209caa8d3563c6bfa726ce](https://code-repo.d4science.org/gCubeSystem/resource-registry-client/commit/de4791849700d2ba9a209caa8d3563c6bfa726ce)_



- Migrated code to reorganized E/R format [#24992]



###  resource-registry-context-client 4.1.1
Tagged commit: _[5a2d5cf4e83759261d3ca8aaa2a19b58648243c1](https://code-repo.d4science.org/gCubeSystem/resource-registry-context-client/commit/5a2d5cf4e83759261d3ca8aaa2a19b58648243c1)_



- Migrated code to reorganized E/R format [#24992]



###  resource-registry-schema-client 4.2.1
Tagged commit: _[fea4d597c11ee2e3880eff0632a52b2709c807d3](https://code-repo.d4science.org/gCubeSystem/resource-registry-schema-client/commit/fea4d597c11ee2e3880eff0632a52b2709c807d3)_



- Migrated code to reorganized E/R format [#24992]



###  resource-registry-publisher 4.4.0
Tagged commit: _[dbfa2901c0f451fc871b28ab9603531fff290336](https://code-repo.d4science.org/gCubeSystem/resource-registry-publisher/commit/dbfa2901c0f451fc871b28ab9603531fff290336)_



- Migrated code to reorganized E/R format [#24992]



###  resource-registry-handlers 2.3.0
Tagged commit: _[660e720771a1fc0b8bc06e100848988a39414ced](https://code-repo.d4science.org/gCubeSystem/resource-registry-handlers/commit/660e720771a1fc0b8bc06e100848988a39414ced)_



- Migrate code to reorganized E/R format [#24992]



###  switch-button-widget 1.3.0
Tagged commit: _[f8c96c17a1ea9f906ea3b7c06f24fc8293eab52d](https://code-repo.d4science.org/gCubeSystem/switch-button-widget/commit/f8c96c17a1ea9f906ea3b7c06f24fc8293eab52d)_

 - 2023-07-25

- Moved to gwt 2.10.0
- Removed the  dependency [#25436]


###  ckan2zenodo-library 1.0.3
Tagged commit: _[6e38258ed0351ee8a6c51f1a31863765ee23aff1](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-library.git/commit/6e38258ed0351ee8a6c51f1a31863765ee23aff1)_

 2023-03-28  
- Extensions evaluated from URL [#22889](https://support.d4science.org/issues/22889)




###  resource-registry 4.3.0
Tagged commit: _[a3a2e3a0a965e600547b5d80daf6f0a20ca1576f](https://code-repo.d4science.org/gCubeSystem/resource-registry/commit/a3a2e3a0a965e600547b5d80daf6f0a20ca1576f)_



- Migrated code to reorganized E/R format [#24992]
- Metadata are added only if requested by the client [#25040]
- Restored Encrypted Property Type and removed Vault [#25041]
- Cleaned RequestInfo in RequestFilter class [#25211]
- Solved bug which allow to create two query template with the same name if a null id was provided [#25650] 



###  gcat 2.5.1
Tagged commit: _[41e88f0c6e7903913ac270774c967d3b10936985](https://code-repo.d4science.org/gCubeSystem/gcat/commit/41e88f0c6e7903913ac270774c967d3b10936985)_



- Migrated code to reorganized E/R format [#24992]
- Moderation notification are now sent as separated thread [#25614]



###  smart-executor 3.2.0
Tagged commit: _[e664ac8388f58b15e11a65112d05f2f4f1f59d26](https://code-repo.d4science.org/gCubeSystem/smart-executor/commit/e664ac8388f58b15e11a65112d05f2f4f1f59d26)_



- Fixed RequestFilter to avoid to remove info to Smartgears
- Migrated code to reorganized E/R format [#24992]
- Force guava version to 23.6-jre to meet requirements of new plugins



###  grsf-publisher 1.0.0
Tagged commit: _[95024980025dcaa7e4b98db7cbc48a359ba2d7b4](https://code-repo.d4science.org/gCubeSystem/grsf-publisher/commit/95024980025dcaa7e4b98db7cbc48a359ba2d7b4)_



- First Version

###  gcube-cms-suite 1.0.5
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_


- Added maven profiles `geoportal-release-profile` and `geoportal-snapshot-profile` [#25570]
- Moved to maven-parent.v1.2.0 [#25570]


###  geoportal-common 1.0.13
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Using parent version range [#25572]

###  cms-plugin-framework 1.0.4
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Updated plugin framework


###  default-lc-managers 1.2.2
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Using parent version range [#25572]

###  concessioni-model 1.0.4
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Updated plugin framework


###  concessioni-lifecycle 1.1.1
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Using parent version range [#25572]

###  sdi-plugins 1.1.2
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Using parent version range [#25572]

###  geoportal-service 1.1.1
Tagged commit: _[1386fec91ccfe3c38b43c761c8dd51fb39d45b63](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/1386fec91ccfe3c38b43c761c8dd51fb39d45b63)_

- Fixed data accounting issue [#25571]
- Moved to maven-parent 1.2.0 and using parent version range [#25572]

###  geoportal-client 1.2.1

- Using parent version range [#25572]


###  geoportal-data-common 2.3.0
Tagged commit: _[aeed289ca5d50c665948aa0f41294a423d634a7e](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/aeed289ca5d50c665948aa0f41294a423d634a7e)_



- Read countByPhase from configuration [#25598]
- Reduced/Optimized some LOGs [#25539]


###  ckan2zenodo-publisher-widget 1.1.3
Tagged commit: _[6dbc1d8e4c193593b2bcaa541abf0a105b491fdf](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-publisher-widget/commit/6dbc1d8e4c193593b2bcaa541abf0a105b491fdf)_

 - 2023-07-25

- Fixing  issue, recompiling this component [#25275]


###  metadata-profile-form-builder-widget 2.1.0
Tagged commit: _[69f0f0e7f762515ae833c24989277496ae24ef89](https://code-repo.d4science.org/gCubeSystem/metadata-profile-form-builder-widget/commit/69f0f0e7f762515ae833c24989277496ae24ef89)_

 - 2023-02-01

#### Enhancements

- [#23188] Improved Legend is required field
- [#24515] Managed the files already uploaded


###  geoportal-data-mapper 1.0.1
Tagged commit: _[975c21b742dc9286bba463b991367f4079e86cc0](https://code-repo.d4science.org/gCubeSystem/geoportal-data-mapper/commit/975c21b742dc9286bba463b991367f4079e86cc0)_



- Update maven-portal-bom version



###  gcube-ckan-datacatalog 2.2.6
Tagged commit: _[857ea04ff16246668b1106baad92198a91e801d8](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/857ea04ff16246668b1106baad92198a91e801d8)_

 - 2023-07-25

- Just rebuilt the Portlet, fixing issue [#25275]
- Moved to GWT 2.10.0


###  geoportal-data-viewer-app 3.6.0
Tagged commit: _[8cf6d178b14fb0fdd0f69a15180ea00f5f550007](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/8cf6d178b14fb0fdd0f69a15180ea00f5f550007)_



- GUI optimization [#25461]
- Using the latest libraries version: gwt-ol.v8.4.1 (with openlayers.v6.6.1) and gwt.v2.10
- Showing basic statistics (i.e. X project published) via 'countByPhase' method [#25598]


###  system-workspace-client-definition-portlet 1.0.0
Tagged commit: _[12d5f899b37b7186c0c0d5edcd119e7ed0bd3944](https://code-repo.d4science.org/gCubeSystem/system-workspace-client-definition-portlet/commit/12d5f899b37b7186c0c0d5edcd119e7ed0bd3944)_

 - 2023-07-12
- First Release [#22514]


###  smartgears-distribution 3.6.0
Tagged commit: _[cc243260b1139f08eed9f638ae13db16f1dd6f3c](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution-legacy/commit/cc243260b1139f08eed9f638ae13db16f1dd6f3c)_

- upgraded gcube-smartgears-bom




---
*generated by the gCube-ReleaseNotes pipeline from report 1593*

*last update Thu Sep 21 11:43:59 CEST 2023*
