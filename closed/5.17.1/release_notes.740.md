# Release Notes for gCube 5.17.1

---
###  keycloak-client 2.1.0
Tagged commit: _[45c52f1d566b498a414975b74823375f5d352e1f](https://code-repo.d4science.org/gCubeSystem/keycloak-client/commit/45c52f1d566b498a414975b74823375f5d352e1f)_


- Added  support, also with  scope, and methods to add extra headers during the OIDC token requests (#27099).
- Added custom base URL set via factory (not automatically working cross environments) [#27234].
- Added JWKS server configuration retrieval, realm's info (as  JSON containing public key) and JWT digital signature verification by using the RSA public key of the realm on server. It uses the  library by  [#27340]


###  oidc-library 1.3.2
Tagged commit: _[ccce7944ee99c0561b9b5ecaf62d6f5861988e38](https://code-repo.d4science.org/gCubeSystem/oidc-library/commit/ccce7944ee99c0561b9b5ecaf62d6f5861988e38)_


- Header X-D4Science-Context in query exchange and refresh
- Token exchange (#27099)
- PerformQueryTokenWithPOST accepts also optional headers
- PerformURLEncodedPOSTSendData accepts also optional headers
- Moved from  to 


###  event-publisher-library 1.2.0
Tagged commit: _[a5dc4633505af8f49b9170710b073cec28fc538d](https://code-repo.d4science.org/gCubeSystem/event-publisher-library/commit/a5dc4633505af8f49b9170710b073cec28fc538d)_


- Restored correct behavior for event publishing with workflow id only sent back, setting input payload in JSON
- Extracted  class for easy subclassing
- Added new outcome check methods to inspect last send and last check actions results and some other facilities
- Better handling of exceptions and retrying behavior in case of read timeout. (Default connection timeout is 10s and read timeout now is 60s)
- Moved from  to 


###  keycloak-d4science-spi-parent 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Now the minor part of the version (the  in the ) shows the compatibility to the specific Keycloak major version, in this case 


###  avatar-storage 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Now the minor part of the version (the  in the ) shows the compatibility to the specific Keycloak major version, in this case 


###  avatar-realm-resource 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Moved code from Resteasy classic to Reateasy reactive since now Quarkus uses this framework. Now the minor part of the version (the 24 in the 2.24.x) shows the compatibility to the specific Keycloak major version, in this case 24.x.x



###  avatar-importer 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_

Removed LinkedIn OAuth2 deprecated provider and added the new OIDC version to supported providers Moved from commons-lang to commons-lang3 artifactId in org.apache.commons groupId. Now the minor part of the version (the 24 in the 2.24.x) shows the compatibility to the specific Keycloak major version, in this case 24.x.x


###  event-listener-provider 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Completely rewrote the implementation and configuration method, now both admin and d4science realms are configured by default as interesting realms. Added specific SPI configurations with spi-events-listener-orchestrator-event-publisher- prefix: include-realms, exclude-realms, include-admin-types, exclude-admin-types, include-events and exclude-events Now the minor part of the version (the 24 in the 2.24.x) shows the compatibility to the specific Keycloak major version, in this case 24.x.x

###  identity-provider-mapper 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Now the minor part of the version (the  in the ) shows the compatibility to the specific Keycloak major version, in this case 


###  keycloak-d4science-script 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Now the minor part of the version (the  in the ) shows the compatibility to the specific Keycloak major version, in this case 


###  keycloak-d4science-theme 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


New theme support for account (Keycloak.v3) and others (Keycloak.v2). d4science.v2 theme has been deprecated (and removed from declarations) and now only d4science will be the theme to be used/extended. Now the minor part of the version (the 24 in the 2.24.x) shows the compatibility to the specific Keycloak major version, in this case 24.x.x



###  ldap-storage-mapper 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Now the minor part of the version (the  in the ) shows the compatibility to the specific Keycloak major version, in this case 


###  protocol-mapper 2.24.0
Tagged commit: _[def6e8e5d0791201cfbc38052be4b7e017ed111f](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/def6e8e5d0791201cfbc38052be4b7e017ed111f)_


Now the minor part of the version (the  in the ) shows the compatibility to the specific Keycloak major version, in this case 


###  accounting-postgresql-utilities 2.0.0
Tagged commit: _[a31c80d82319900682c059a7cd57af6c75c8ed44](https://code-repo.d4science.org/gCubeSystem/accounting-postgresql-utilities/commit/a31c80d82319900682c059a7cd57af6c75c8ed44)_



- RecordToDBMapping is no more singleton 



###  accounting-analytics-persistence-postgresql 2.0.1
Tagged commit: _[c607c36eff70cef3f5413e0fd206474a290de1ae](https://code-repo.d4science.org/gCubeSystem/accounting-analytics-persistence-postgresql/commit/c607c36eff70cef3f5413e0fd206474a290de1ae)_



- Enhanced accounting-postgresql-utilities range 



###  social-networking-library 2.0.1
Tagged commit: _[392ab46bc9d8e62bfe0df4f882da3f317bb2a861](https://code-repo.d4science.org/gCubeSystem/social-networking-library/commit/392ab46bc9d8e62bfe0df4f882da3f317bb2a861)_

 - 2024-04-22

 - Bug 27218 - Null pointer exception getting notifications preferences fixed
 - Feature 27286 - Removed noisy logs


###  social-service-client 2.0.1
Tagged commit: _[2b523df4fbb16aaf6047d2adb368ba4c2ce057c5](https://code-repo.d4science.org/gCubeSystem/social-service-client/commit/2b523df4fbb16aaf6047d2adb368ba4c2ce057c5)_

 - 2024-04-16

- fixed dependency versions



###  social-library-stubs 1.0.1
Tagged commit: _[b0aa6eea9be9c134b15a8c401f045e3d3656ee09](https://code-repo.d4science.org/gCubeSystem/social-library-stubs/commit/b0aa6eea9be9c134b15a8c401f045e3d3656ee09)_

 - 2024-04-16

 - Null pointer exception bug fixed
 - Removed unnecessary logs


###  catalogue-core 1.0.0
Tagged commit: _[cdbc3136776efb7b08969c52473d61a77ecc1900](https://code-repo.d4science.org/gCubeSystem/catalogue-core/commit/cdbc3136776efb7b08969c52473d61a77ecc1900)_



- First Version
###  aslsocial 1.8.1
Tagged commit: _[a857397a24462292a3279ccba56877a1cff3997a](https://code-repo.d4science.org/gCubeSystem/aslsocial/commit/a857397a24462292a3279ccba56877a1cff3997a)_

 - 2023-05-14

 - Bug 27457: get application profile multiple times before settling on sitelandingpath


###  accounting-aggregator-se-plugin 2.0.0
Tagged commit: _[92d313d9c32a0bb65be6a3b7176aa91a5983e54b](https://code-repo.d4science.org/gCubeSystem/accounting-aggregator-se-plugin/commit/92d313d9c32a0bb65be6a3b7176aa91a5983e54b)_



- Switched JSON management to gcube-jackson [#19115]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Fixed workspace folder creation [#19056]
- Using StorageHubClient in place of Home Library Webapp HTTP calls
- Added the generation of a CSV to post analyze the calledMethods
- Fixed distro files and pom according to new release procedure
- Ported plugin to smart-executor v3.X.X [#21615]
- Ported plugin to works with TimescaleDB [#21347]
- Enhanced range of storagehub-client-library to 2.0.0,3.0.0-SNAPSHOT [#22824]


###  gcat 2.5.3
Tagged commit: _[95cdb46f003c5cae51564b4d8de4ee2a97185235](https://code-repo.d4science.org/gCubeSystem/gcat/commit/95cdb46f003c5cae51564b4d8de4ee2a97185235)_

 

- Improved profile schema #26471
- Catalogue core operation has been moved in a dedicated library #27118



###  grsf-publisher 1.1.0
Tagged commit: _[9443a79ccbc957d4bb2b130e2b750ab493f4acb9](https://code-repo.d4science.org/gCubeSystem/grsf-publisher/commit/9443a79ccbc957d4bb2b130e2b750ab493f4acb9)_



- Accepted input has been changed see #25008 
- catalogue-core library is used to interact with Ckan #27118
- Added missing keycloak-client library to set it to provided



###  social-networking-library-ws 3.0.1
Tagged commit: _[97eae29a9393b9f6b2363afd0eddcbe736f566ba](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/97eae29a9393b9f6b2363afd0eddcbe736f566ba)_

 - 2023-04-22

- Feature 27286: removed noisy logs
- Bug 27218: advance social networking library version


###  ckan-metadata-publisher-widget 2.2.2
Tagged commit: _[379826eb8e5f814257e2494262308ffbcdc87b22](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/379826eb8e5f814257e2494262308ffbcdc87b22)_

 - 2024-05-14

**Bug fixes**

- Incident Catalogue edit is leading to duplicate fields [#27455]

**Enhancement**

- The Catalogue should read the (Liferay) Highest Role in the VRE (i.e. the Organization) [#27467]


###  rpt-token-portlet 1.3.0
Tagged commit: _[a20fc7db3b541133590273f388d7fdfa09832616](https://code-repo.d4science.org/gCubeSystem/rpt-token-portlet/commit/a20fc7db3b541133590273f388d7fdfa09832616)_

 = 2024-04-10
Client-exchange configuration for a dedicated client (#27204)


###  accept-invite-portlet 2.1.0
Tagged commit: _[fcf36d16ff9ede6380921b8a3afe79b696249f48](https://code-repo.d4science.org/gCubeSystem/accept-invite-portlet/commit/fcf36d16ff9ede6380921b8a3afe79b696249f48)_

 - 2024-04-22
- Fix for Incident #27306


###  social-mail-servlet 3.0.0
Tagged commit: _[58ea2479ee3b779e75bf4795c61081ffbc8b0124](https://code-repo.d4science.org/gCubeSystem/social-mail-servlet/commit/58ea2479ee3b779e75bf4795c61081ffbc8b0124)_

 - 2024-04-18

- Removed social networking lib dep


###  gcube-ckan-datacatalog 2.3.1
Tagged commit: _[cb814d86881a444b15ef1995c0410c3b5577ccd7](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/cb814d86881a444b15ef1995c0410c3b5577ccd7)_

 - 2024-05-14

- Included  with bug fix [#27455] and new feature [#27467]



---
*generated by the gCube-ReleaseNotes pipeline from report 1664*

*last update Thu Jun 20 12:02:10 CEST 2024*
