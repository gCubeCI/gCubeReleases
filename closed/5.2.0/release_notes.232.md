# Release Notes for gCube 5.2.0

## What's New in gCube 5.2.0

* The gCube Accounting persistence has been migrated from CouchBase to TimeScaleDB. This reduces the costs of operation while keeping performance and scalability;
* The gCube Catalogue framework has been ported to the gCube gCat service making it independent from the CKAN technology;
* The new WEkEO proxy enables interoperability between the D4Science infrastructure and the WEkEO infrastructure.


---
## Details about components in release:


###  maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


###  maven-portal-bom 3.6.1
Tagged commit: _[024b53f9150416cd97d4b04fbee0a2116b1a98a0](https://code-repo.d4science.org/gCubeSystem/maven-portal-bom/commit/024b53f9150416cd97d4b04fbee0a2116b1a98a0)_

 - 2021-03-25

Updated accounting libs version


###  oidc-library 1.2.1
Tagged commit: _[abe7e42937d083b34cb3ee03958c362e2c3e948f](https://code-repo.d4science.org/gCubeSystem/oidc-library/commit/abe7e42937d083b34cb3ee03958c362e2c3e948f)_


- Data-Editor role added (#20896), some logs changed to debug level


###  usermanagement-core 2.5.3
Tagged commit: _[fa81aaab79c66b01de6b4c384abfb5c725963525](https://code-repo.d4science.org/gCubeSystem/usermanagement-core/commit/fa81aaab79c66b01de6b4c384abfb5c725963525)_

 - 2021-04-12

[#20889] UserManagement enhancement: add the roles Data-Editor


###  accounting-postgresql-utilities 1.0.0
Tagged commit: _[d67bca62cd4ed487d4fa06cb58cc67c284a63672](https://code-repo.d4science.org/gCubeSystem/accounting-postgresql-utilities/commit/d67bca62cd4ed487d4fa06cb58cc67c284a63672)_



- First Release
###  usermanagement-core 2.5.3
Tagged commit: _[fa81aaab79c66b01de6b4c384abfb5c725963525](https://code-repo.d4science.org/gCubeSystem/usermanagement-core/commit/fa81aaab79c66b01de6b4c384abfb5c725963525)_

 - 2021-04-12

[#20889] UserManagement enhancement: add the roles Data-Editor


###  gcat-api 2.0.0
Tagged commit: _[ef46b5ca0ac1884df14ae6c90c8619d958301234](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/ef46b5ca0ac1884df14ae6c90c8619d958301234)_



- Changed service class 


###  document-store-lib-postgresql 1.0.0
Tagged commit: _[6cefdba8a7a08ca4cf4fbe1b723b1fdeaa349a95](https://code-repo.d4science.org/gCubeSystem/document-store-lib-postgresql/commit/6cefdba8a7a08ca4cf4fbe1b723b1fdeaa349a95)_



- First Release
###  accounting-manager-theme 1.3.0
Tagged commit: _[bc3f1d07e7b14d38ea7cc6daea2d80ee4c37252a](https://code-repo.d4science.org/gCubeSystem/accounting-manager-theme/commit/bc3f1d07e7b14d38ea7cc6daea2d80ee4c37252a)_

 - 2021-03-23

### Features

- Migrate accounting-manager to postgresql persistence [#21013]




###  uri-resolver-manager 1.4.2
Tagged commit: _[13eee9eb15dcb7a97d55783063f6ed209ea41972](https://code-repo.d4science.org/gCubeSystem/uri-resolver-manager/commit/13eee9eb15dcb7a97d55783063f6ed209ea41972)_

 - 2021-04-21

### Bug fixes

[#21240] Generates broken gis-link


## [1-4-1] - 2020-05-06

### Bug fixes

[#19215] UriResolverManager: request to DL (the shortener) must be encoded
###  grsf-common-library 1.3.1
Tagged commit: _[b2b6489f60020c5944c9ae52f35b7166f9cf4012](https://code-repo.d4science.org/gCubeSystem/grsf-common-library/commit/b2b6489f60020c5944c9ae52f35b7166f9cf4012)_

 - 2021-04-09

### Changed

- Switched dependency management to gcube-bom 2.0.0
[#19500#note-2] Fixing GWT import issue 


###  workspace-explorer 2.2.0
Tagged commit: _[9368baa3cd08f6db1e5275953982e96d1990966d](https://code-repo.d4science.org/gCubeSystem/workspace-explorer/commit/9368baa3cd08f6db1e5275953982e96d1990966d)_

 - 2021-03-26

### Bug fixes

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20991] Conflict with Bootstrap Modal fixed

### Enhancements

[#21045] Added a new behavior: startable on an input folder and 
being able to navigate from the Workspace Root



###  accounting-lib 4.1.0
Tagged commit: _[c7540455d85c090578d6263a5b95c8286861f02b](https://code-repo.d4science.org/gCubeSystem/accounting-lib/commit/c7540455d85c090578d6263a5b95c8286861f02b)_

 

- Deprecated not needed properties to reduce data and improve aggregation
- Improved regex to comply with new version of Thredds called methods [#18053]



###  gcat-client 2.0.0
Tagged commit: _[f782a87604d9eb212867895e2b6e070de7587a37](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/f782a87604d9eb212867895e2b6e070de7587a37)_



- Changed service class 


###  catalogue-util-library 1.0.0
Tagged commit: _[7faf4ff40035e86f7d40a805d19ed7f1ebbcff50](https://code-repo.d4science.org/gCubeSystem/catalogue-util-library/commit/7faf4ff40035e86f7d40a805d19ed7f1ebbcff50)_

 - 2021-02-17

**Bug Fixes**
[#21259] remove the method that validates the URL

**New Features**
[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20828] Revisited title size and format
[#19378] First Release
###  accounting-analytics 3.0.0
Tagged commit: _[1b2bdbfafa22385fc2a8f617ff18c8e72afc6b49](https://code-repo.d4science.org/gCubeSystem/accounting-analytics/commit/1b2bdbfafa22385fc2a8f617ff18c8e72afc6b49)_



- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure



###  accounting-analytics-persistence-postgresql 1.0.0
Tagged commit: _[6edba74b4352ae0238d791b67e9bce9e673e18b0](https://code-repo.d4science.org/gCubeSystem/accounting-analytics-persistence-postgresql/commit/6edba74b4352ae0238d791b67e9bce9e673e18b0)_



- First Release
###  ws-thredds 0.2.5
Tagged commit: _[bdf58af229d83b9a145f1d2a21a1d46d9b559011](https://code-repo.d4science.org/gCubeSystem/ws-thredds.git/commit/bdf58af229d83b9a145f1d2a21a1d46d9b559011)_


Fixes #21265



###  oidc-library-portal 1.3.0
Tagged commit: _[51677353b0b1049649117cbaf31ff4609ab1138a](https://code-repo.d4science.org/gCubeSystem/oidc-library-portal/commit/51677353b0b1049649117cbaf31ff4609ab1138a)_


Extracted the UMA issuing code for logged user from the Valve in the  project to be used also after the login process for UMA issue in the context, since the Valve has already finished its work at that moment. (#20591)


###  event-publisher-portal 1.0.2
Tagged commit: _[4ae6933607070af57aa5f71627a202fd6401e043](https://code-repo.d4science.org/gCubeSystem/event-publisher-portal/commit/4ae6933607070af57aa5f71627a202fd6401e043)_


- OIDC token only is now used to send events to the orchestrator endpoint (UMA token is no more involved).


###  ckan2zenodo-library 1.0.1
Tagged commit: _[d7b05a2b5724017823f87299f22db2a574fedca3](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-library.git/commit/d7b05a2b5724017823f87299f22db2a574fedca3)_

 2021-04-7

-Integration with gCat 2.0.0 [#20751](https://support.d4science.org/issues/20751)


###  oidc-enrollment-hook 1.2.0
Tagged commit: _[69c58ec0a31e22fc7a0b9e71d4f25f980367e275](https://code-repo.d4science.org/gCubeSystem/oidc-enrollment-hook/commit/69c58ec0a31e22fc7a0b9e71d4f25f980367e275)_


- Now an UMA token is issued also after the login in the  to assure token's presence for the context without have to wait the next HTTP call and the Valve's intervention, in the case when it is not necessary the redirect to an origin URI after the login. (#20591)


###  event-publisher-hook 1.1.0
Tagged commit: _[2dd141e1e9bb5010528574876cceadd401c62d3d](https://code-repo.d4science.org/gCubeSystem/event-publisher-hook/commit/2dd141e1e9bb5010528574876cceadd401c62d3d)_


Added new event publisher for new created and removed s having  type (#20896)


###  user-registration-hook 2.0.3
Tagged commit: _[c964fea2b44d25c0fe38f1c6ff725cd1490a2ecb](https://code-repo.d4science.org/gCubeSystem/user-registration-hook/commit/c964fea2b44d25c0fe38f1c6ff725cd1490a2ecb)_

 - 2021-04-12

Just removed obsolete Home library deps from pom which were forgotten there in 6.8.0 release


###  VREFolder-hook 6.8.1
Tagged commit: _[4fc9e7c8f78a50cff43bb0a4afae6143cffdfc6d](https://code-repo.d4science.org/gCubeSystem/VREFolder-hook/commit/4fc9e7c8f78a50cff43bb0a4afae6143cffdfc6d)_

 - 2021-04-12

Just removed obsolete Home library deps from pom which were forgotten there in 6.8.0 release


###  threadlocal-vars-cleaner 2.3.0
Tagged commit: _[58641ac4a4337e8bc86528c48cc4703f086ccc5b](https://code-repo.d4science.org/gCubeSystem/threadlocal-vars-cleaner/commit/58641ac4a4337e8bc86528c48cc4703f086ccc5b)_

 - 2021-05-03

- Fix Bug #20591 Keycloak-LR 6.2 Integration: portlet calls in landing page have not UMA token set


###  ckan2zenodo-publisher-widget 1.0.2
Tagged commit: _[347ab5f764ba410c1a58217bc4298ab07e34bf33](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-publisher-widget/commit/347ab5f764ba410c1a58217bc4298ab07e34bf33)_

 - 2021-04-26

**Bug fixes**
[#21263] manage empty mapping for creators/contributors

**Enhancements**
[#21153] ported to maven-portal-bom 3.6.1
Improved modal height



###  ckan-metadata-publisher-widget 2.0.0
Tagged commit: _[a26228b117e7565b7ce8876d8c8c58d5205daf0e](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget/commit/a26228b117e7565b7ce8876d8c8c58d5205daf0e)_

 - 2021-04-12

**Enhancements**

[#19764] Porting ckan-metadata-publisher-widget to catalogue-util-library
[#20680] Ported to SHUB
[#19568] Unify and extend the tags allowed values 
[#20828] Revisited title size and format
[#20868] Redesigned the Manage Resources user experience
[#21068] Add Resources facility: only HTTPS URLs must be allowed
[#21153] Upgrade the maven-portal-bom to 3.6.1 version


###  grsf-manage-widget 1.5.0
Tagged commit: _[478a4c157ec95b9d4493748f39f71ce6473f9138](https://code-repo.d4science.org/gCubeSystem/grsf-manage-widget/commit/478a4c157ec95b9d4493748f39f71ce6473f9138)_

 - 2021-04-12

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20728] Migrate to catalogue-util-library



###  catalogue-sharing-widget 1.2.0
Tagged commit: _[38c860a22142a6cb6a934886bcf81f651c4ee102](https://code-repo.d4science.org/gCubeSystem/catalogue-sharing-widget/commit/38c860a22142a6cb6a934886bcf81f651c4ee102)_

 - 2021-04-12

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20735] Bug fixes



###  workspace-tree-widget 6.32.0
Tagged commit: _[00c048af95cc35c2ebeca28b12507186cf7314b2](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/00c048af95cc35c2ebeca28b12507186cf7314b2)_

 - 2021-04-12

#### Enhancements

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20762] Moved to ckan-metadata-publisher-widget 2.X


###  gcat 2.0.0
Tagged commit: _[6fad1ba8d16de14b7a4b7625447e7e346505f91d](https://code-repo.d4science.org/gCubeSystem/gcat/commit/6fad1ba8d16de14b7a4b7625447e7e346505f91d)_



- Fixed author and maintainer name and email [#21059] [#21189]
- Improved check on controlled vocabulary to match corner cases [#20742]
- Added PATCH method on Item collection [#19768]
- Switched JSON management to gcube-jackson [#19735]
- Added support to publish an item organizations not matching the current context [#19365]


###  accounting-service 2.0.0
Tagged commit: _[633e302ba5524e36e51e55b75f6012d28a494ff6](https://code-repo.d4science.org/gCubeSystem/accounting-service/commit/633e302ba5524e36e51e55b75f6012d28a494ff6)_

 

- Switched analytics persistence to PostgreSQL [#21182]
- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure



###  uri-resolver 2.5.0
Tagged commit: _[0ac4b3354b3134a05075fc416aea7a83cebed5d8](https://code-repo.d4science.org/gCubeSystem/uri-resolver/commit/0ac4b3354b3134a05075fc416aea7a83cebed5d8)_

 - 2021-04-08

**New features**

[#20993] Supported new resource Wekeo Interface - gettoken.

**Bug fixes**

[#21093] StorageHubResolver HEAD request does not support Content-Length



###  sdi-service 1.4.3
Tagged commit: _[9a465db43feae80a0299829e10c32282ca6bc50f](https://code-repo.d4science.org/gCubeSystem/sdi-service.git/commit/9a465db43feae80a0299829e10c32282ca6bc50f)_

 2020-05-15
changed maven repos
updated bom dependency


###  workspace-widget-portlet 1.5.0
Tagged commit: _[7d43b96e11010a4f894ce1d589f2fba6e663b587](https://code-repo.d4science.org/gCubeSystem/workspace-widget-portlet/commit/7d43b96e11010a4f894ce1d589f2fba6e663b587)_

 - 2021-04-06

Removed legacy auth dependency


###  share-updates 2.8.0
Tagged commit: _[c6ac87564b394c3518500c1bbc1e645f824aa029](https://code-repo.d4science.org/gCubeSystem/share-updates/commit/c6ac87564b394c3518500c1bbc1e645f824aa029)_

 -2021-04-02

- Ported to git

- Fix Bug #21023 attached documents in Posts do not open in Chrome

- Remove HomeLibrary dependency and replace with storage hub one


###  accounting-manager 1.13.0
Tagged commit: _[3ec119c2c3d037b3e6ad340dd1bbe00c9adf90d2](https://code-repo.d4science.org/gCubeSystem/accounting-manager/commit/3ec119c2c3d037b3e6ad340dd1bbe00c9adf90d2)_

 - 2021-03-23

### Features

- Migrate accounting-manager to postgresql persistence [#21013]




###  workspace 6.26.0
Tagged commit: _[5a4081b7e7f70d38787fc6b7a29dc5636f26334f](https://code-repo.d4science.org/gCubeSystem/workspace/commit/5a4081b7e7f70d38787fc6b7a29dc5636f26334f)_

 - 2021-04-12

#### Enhancements

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20762] Moved to ckan-metadata-publisher-widget 2.X


###  gcube-ckan-datacatalog 2.0.0
Tagged commit: _[e51bc1c9b6dba1e3e9b7744e8485b0231550bd7f](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/e51bc1c9b6dba1e3e9b7744e8485b0231550bd7f)_

 - 2021-04-12

#### Enhancements

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20699] Migrate the gcube-ckan-datacatalog to catalogue-util-library



###  rstudio-wrapper-portlet 1.5.0
Tagged commit: _[7b3d4530de696ce9ad86350bfc531d4b209fef8b](https://code-repo.d4science.org/gCubeSystem/rstudio-wrapper-portlet/commit/7b3d4530de696ce9ad86350bfc531d4b209fef8b)_

 - 2021-04-21

-  RStudio instance allocation takes into account the number of instance available in the VO and not in the context and assign the RStudio instance balancing the load. 

- The user is always redirected to the same endpoint, until a gCore Endpoint disappears, if so a new allocation is calculated.


###  gcube-portal-bundle 5.0.1
Tagged commit: _[f90127b0de1cfa39118a617d9621968f24e74409](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/f90127b0de1cfa39118a617d9621968f24e74409)_

 - 2021-03-25

Added support for new Accounting libs


###  smartgears-distribution 3.3.0
Tagged commit: _[a7206d002c243777137444f729acda63fdb02f4b](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/a7206d002c243777137444f729acda63fdb02f4b)_



- Accounting-lib inherited dependency has been upgraded to 4.1.0



---
*generated by the gCube-ReleaseNotes pipeline from report 975*

*last update Wed May 05 17:04:59 CEST 2021*