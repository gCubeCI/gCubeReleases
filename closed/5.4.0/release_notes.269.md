# Release Notes for gCube 5.4.0

## What's New in gCube 5.4.0

* Improved performance on many applications and services using workspace and storage, among which: Messages, Anagraphic & Social service, PerformFish form uploads;
* Released a new major version of the smart-executor framework and all its plugins, resulting in better performance, maintenance, and security;
* Enhancements on the Identity and Access Management related components for better performance;
* Various fixes on Workspace-THREDDS servers syncronization and auditing & accounting framework.


---
###  maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


###  maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


###  smart-executor-bom 3.0.0
Tagged commit: _[3202995a5bcb93519475d0a8d825166a5bce351f](https://code-repo.d4science.org/gCubeSystem/smart-executor-bom/commit/3202995a5bcb93519475d0a8d825166a5bce351f)_



- Switched smart-executor JSON management to gcube-jackson [#19647]



###  maven-portal-bom 3.6.3
Tagged commit: _[4a394685fd99adeb017ba0a8a4c42b7439c115bd](https://code-repo.d4science.org/gCubeSystem/maven-portal-bom/commit/4a394685fd99adeb017ba0a8a4c42b7439c115bd)_

 - 2021-06-24

- Added common-gcube-calls lib 


###  smart-executor-api 3.0.0
Tagged commit: _[cbdf37faa730f4f4b896c8453eb16615ea2b97ed](https://code-repo.d4science.org/gCubeSystem/smart-executor-api/commit/cbdf37faa730f4f4b896c8453eb16615ea2b97ed)_



- Switched smart-executor JSON management to gcube-jackson [#19647]
- Plugins must provide a property file with name <PluginName>.properties [#21596]  



###  common-authorization 2.4.0
Tagged commit: _[9b0ad437a57a0f4a2c75db740e1bcf99381244b6](https://code-repo.d4science.org/gCubeSystem/common-authorization/commit/9b0ad437a57a0f4a2c75db740e1bcf99381244b6)_

 - [2021-05-21]

JWTUmaTokenProvider changed to AccessTokenProvider


###  document-store-lib-no-insert 2.0.0
Tagged commit: _[def834d2d00d63981ee23d7a86050cad1731da35](https://code-repo.d4science.org/gCubeSystem/document-store-lib-no-insert/commit/def834d2d00d63981ee23d7a86050cad1731da35)_

 

- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure



###  document-store-lib-no-insert 2.0.0
Tagged commit: _[def834d2d00d63981ee23d7a86050cad1731da35](https://code-repo.d4science.org/gCubeSystem/document-store-lib-no-insert/commit/def834d2d00d63981ee23d7a86050cad1731da35)_

 

- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure



###  keycloak-d4science-spi-parent 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  avatar-storage 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  avatar-realm-resource 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  avatar-importer 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  event-listener-provider 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  delete-account 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  identity-provider-mapper 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  ldap-storage-mapper 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  keycloak-d4science-bundle 1.1.0
Tagged commit: _[d41d98a68bcbe41d1447d0b4b480902ce49e59d4](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/commit/d41d98a68bcbe41d1447d0b4b480902ce49e59d4)_


Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with  credentials and  audience



###  oidc-library 1.3.0
Tagged commit: _[bbcd35e14fe816d7fc3660137cd3978c40fbe13b](https://code-repo.d4science.org/gCubeSystem/oidc-library/commit/bbcd35e14fe816d7fc3660137cd3978c40fbe13b)_


Added method to retrieve UMA token by using  and  in a specific  (aka context) that can now be provided in both encoded and not encoded form (starts with / check is performed).


###  smart-executor-api 3.0.0
Tagged commit: _[cbdf37faa730f4f4b896c8453eb16615ea2b97ed](https://code-repo.d4science.org/gCubeSystem/smart-executor-api/commit/cbdf37faa730f4f4b896c8453eb16615ea2b97ed)_



- Switched smart-executor JSON management to gcube-jackson [#19647]
- Plugins must provide a property file with name <PluginName>.properties [#21596]  



###  document-store-lib-no-insert 2.0.0
Tagged commit: _[def834d2d00d63981ee23d7a86050cad1731da35](https://code-repo.d4science.org/gCubeSystem/document-store-lib-no-insert/commit/def834d2d00d63981ee23d7a86050cad1731da35)_

 

- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure



###  storagehub-model 1.1.0
Tagged commit: _[dbc2e9e82ec655b880ab5bbf25161a29bf53f98c](https://code-repo.d4science.org/gCubeSystem/storagehub-model/commit/dbc2e9e82ec655b880ab5bbf25161a29bf53f98c)_

 - [2021-04-29]

- jackson version moved to 2.8.11
- model for messages added


###  gxREST 1.2.0
Tagged commit: _[b4bbaf8816b4f13a8ed61288253c0d15f2704f65](https://code-repo.d4science.org/gCubeSystem/gx-rest/commit/b4bbaf8816b4f13a8ed61288253c0d15f2704f65)_



- Managing new UMA token and not only old authz gcube token [#21525]
- Switched JSON management to gcube-jackson [#19737]



###  gxHTTP 1.2.0
Tagged commit: _[b4bbaf8816b4f13a8ed61288253c0d15f2704f65](https://code-repo.d4science.org/gCubeSystem/gx-rest/commit/b4bbaf8816b4f13a8ed61288253c0d15f2704f65)_



- Managing new UMA token and not only old authz gcube token [#21525]
- Switched JSON management to gcube-jackson [#19737]



###  gxJRS 1.2.0
Tagged commit: _[b4bbaf8816b4f13a8ed61288253c0d15f2704f65](https://code-repo.d4science.org/gCubeSystem/gx-rest/commit/b4bbaf8816b4f13a8ed61288253c0d15f2704f65)_



- Managing new UMA token and not only old authz gcube token [#21525]
- Switched JSON management to gcube-jackson [#19737]



###  common-gcube-calls 1.3.0
Tagged commit: _[cb135d554ad90a0d13528a7c3308a74d46796c23](https://code-repo.d4science.org/gCubeSystem/common-gcube-calls/commit/cb135d554ad90a0d13528a7c3308a74d46796c23)_

 - 2020-11-18

- Changed UmaTokenProvider with AccessTokenProvider


###  common-smartgears 3.1.0
Tagged commit: _[406016cd29a0c2996db7a9a6d9894eba0c22e85e](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/406016cd29a0c2996db7a9a6d9894eba0c22e85e)_

 - 2021-05-14

- use of AccessTokenProvider
- use gcube-jackson instead of minimal-json for access token parsing  [#21097]



###  accounting-analytics 3.0.1
Tagged commit: _[4d33bf89fa0f1bdff9bc7674530491c0ba3b52e2](https://code-repo.d4science.org/gCubeSystem/accounting-analytics/commit/4d33bf89fa0f1bdff9bc7674530491c0ba3b52e2)_



- Aligned model



###  storagehub-client-library 1.3.0
Tagged commit: _[6f96bab4ed375f6849a106ead6b3b97ca001f76c](https://code-repo.d4science.org/gCubeSystem/storagehub-client-library/commit/6f96bab4ed375f6849a106ead6b3b97ca001f76c)_

 - [2021-06-18]

### Feature

method for check item existance moved to a new rest call


###  storagehub-script-utils 1.0.0
Tagged commit: _[a7cf9ca75cef8bba26a409fb08aa88ea04b48365](https://code-repo.d4science.org/gCubeSystem/storagehub-script-utils/commit/a7cf9ca75cef8bba26a409fb08aa88ea04b48365)_

 - [2021-05-04]

First commit
###  smart-executor-client 3.0.0
Tagged commit: _[47c4fca044a68e47c82e6e599b6ec34146eb3f96](https://code-repo.d4science.org/gCubeSystem/smart-executor-client/commit/47c4fca044a68e47c82e6e599b6ec34146eb3f96)_

 

- Switched smart-executor JSON management to gcube-jackson [#19647]
- Redesigned HTTP APIs to comply with RESTful architectural style [#12997]
- Added API to retrieve scheduled tasks [#10780]



###  storagehub-client-wrapper 1.1.0
Tagged commit: _[00bdf06ad3ef4316870839cfacc589f41ac3402c](https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper/commit/00bdf06ad3ef4316870839cfacc589f41ac3402c)_

 - 2021-05-12

#### Enhancements

[#21412] Added some methods that were missing



###  ws-thredds 1.0.1
Tagged commit: _[971b2c0f6bd2647ce7044b0a7861e6605715185b](https://code-repo.d4science.org/gCubeSystem/ws-thredds.git/commit/971b2c0f6bd2647ce7044b0a7861e6605715185b)_

 

Security Fixes
Fixes [#21783]


###  accounting-analytics-persistence-postgresql 1.0.1
Tagged commit: _[ca5ddaa61952bae7eed900cc029fcb35f3676441](https://code-repo.d4science.org/gCubeSystem/accounting-analytics-persistence-postgresql/commit/ca5ddaa61952bae7eed900cc029fcb35f3676441)_



- Fixed bug on query


###  email-templates-library 1.5.0
Tagged commit: _[2c23e965228f8de97f56675188caed7fda4b0ddf](https://code-repo.d4science.org/gCubeSystem/email-templates-library/commit/2c23e965228f8de97f56675188caed7fda4b0ddf)_

 - 2021-05-13

Added template for roles assignments and revokation to be sent to VRE Managers to inform them when another VRE Manager operates with roles.


###  storagehub-application-persistence 2.0.0
Tagged commit: _[423d6294cb894a72d6941a0784daac7f15d8ed4f](https://code-repo.d4science.org/gCubeSystem/storagehub-application-persistence/commit/423d6294cb894a72d6941a0784daac7f15d8ed4f)_



- Removed home library [#21435]  
- Switched HTTP requests gxHTTP 2.0.0 [#19283]
- Switched gCube BOM to 2.0.0 [#19283]



###  oidc-library-portal 1.3.1
Tagged commit: _[a107f6b54e413afb6b49e44885f4d5642b58a86e](https://code-repo.d4science.org/gCubeSystem/oidc-library-portal/commit/a107f6b54e413afb6b49e44885f4d5642b58a86e)_


The  has been removed from  and new provider from that library () is used to transport the  only to the client library


###  ws-synchronized-module-library 1.5.1
Tagged commit: _[f2f42f77f0c0a4789f3998124349418c4f692a44](https://code-repo.d4science.org/gCubeSystem/ws-synchronized-module-library/commit/f2f42f77f0c0a4789f3998124349418c4f692a44)_

 - 2021-07-20

Moved to maven-portal-bom 3.6.3
Just to include new version of ws-thredds


###  user-registration-hook 2.0.4
Tagged commit: _[d9c73f679b0ab54e7c16bcb9389959ac83b33cd9](https://code-repo.d4science.org/gCubeSystem/user-registration-hook/commit/d9c73f679b0ab54e7c16bcb9389959ac83b33cd9)_

 - 2021-05-25

- Feature #21506: Updated to support new UMATokensProvider class


###  VREFolder-hook 6.8.2
Tagged commit: _[2f6b6af28c66c3144480382ddd3ecbda4e549530](https://code-repo.d4science.org/gCubeSystem/VREFolder-hook/commit/2f6b6af28c66c3144480382ddd3ecbda4e549530)_

 - 2021-05-25

- Feature #21505: Updated to support new UMATokensProvider class


###  threadlocal-vars-cleaner 2.3.1
Tagged commit: _[110b24282d0472c05fc792d2877bd3898ecb1cce](https://code-repo.d4science.org/gCubeSystem/threadlocal-vars-cleaner/commit/110b24282d0472c05fc792d2877bd3898ecb1cce)_

 - 2021-05-25

- Feature #21503 Update threadlocal-vars-cleaner to support AccessTokenProvider


###  accounting-dashboard-harvester-se-plugin 2.0.0
Tagged commit: _[ab8ad166d86dd1f8ddee8fe204fdac1c659b1e79](https://code-repo.d4science.org/gCubeSystem/accounting-dashboard-harvester-se-plugin/commit/ab8ad166d86dd1f8ddee8fe204fdac1c659b1e79)_



- Ported plugin to smart-executor APIs 3.0.0 [#21616]
- Added RStudio Harvester [#21557]
- Added Jupyter Harvester [#21031]
- Switched accounting JSON management to gcube-jackson [#19115]
- Switched smart-executor JSON management to gcube-jackson [#19647]



###  hello-world-se-plugin 2.0.0
Tagged commit: _[167ea39654f9d50c6b10b45cc0b6f50759ece3eb](https://code-repo.d4science.org/gCubeSystem/hello-world-se-plugin/commit/167ea39654f9d50c6b10b45cc0b6f50759ece3eb)_



- Ported plugin to smart-executor APIs 3.0.0 [#21619]
- Switched smart-executor JSON management to gcube-jackson [#19647]



###  resource-checker-se-plugin 2.0.0
Tagged commit: _[10fa700ad0af8d74b90297b1859c5f050458fb49](https://code-repo.d4science.org/gCubeSystem/resource-checker-se-plugin/commit/10fa700ad0af8d74b90297b1859c5f050458fb49)_



- Contexts listing is performed via rpm-common-library [#21636]
- Ported plugin to smart-executor APIs 3.0.0 [#21618]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Creating uberjar in place of jar-with-dependecies
- Used smart-executor bom for better dependency management



###  resource-checker-se-plugin 2.0.0
Tagged commit: _[10fa700ad0af8d74b90297b1859c5f050458fb49](https://code-repo.d4science.org/gCubeSystem/resource-checker-se-plugin/commit/10fa700ad0af8d74b90297b1859c5f050458fb49)_



- Contexts listing is performed via rpm-common-library [#21636]
- Ported plugin to smart-executor APIs 3.0.0 [#21618]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Creating uberjar in place of jar-with-dependecies
- Used smart-executor bom for better dependency management



###  social-data-indexer-se-plugin 3.0.0
Tagged commit: _[a67a8cddb2491f1facba2f111eb39a82d928f9e1](https://code-repo.d4science.org/gCubeSystem/social-data-indexer-se-plugin/commit/a67a8cddb2491f1facba2f111eb39a82d928f9e1)_



- Ported plugin to smart-executor APIs 3.0.0 [#21570]


###  accounting-insert-storage-se-plugin 2.0.0
Tagged commit: _[1054940ffe55106e52aa3106116763850b5e7c4f](https://code-repo.d4science.org/gCubeSystem/accounting-insert-storage-se-plugin/commit/1054940ffe55106e52aa3106116763850b5e7c4f)_

 

- Ported plugin to smart-executor APIs 3.0.0 [#21368]
- The plugin has been completely rewrote


###  accounting-insert-storage-se-plugin 2.0.0
Tagged commit: _[1054940ffe55106e52aa3106116763850b5e7c4f](https://code-repo.d4science.org/gCubeSystem/accounting-insert-storage-se-plugin/commit/1054940ffe55106e52aa3106116763850b5e7c4f)_

 

- Ported plugin to smart-executor APIs 3.0.0 [#21368]
- The plugin has been completely rewrote


###  resource-checker-se-plugin 2.0.0
Tagged commit: _[10fa700ad0af8d74b90297b1859c5f050458fb49](https://code-repo.d4science.org/gCubeSystem/resource-checker-se-plugin/commit/10fa700ad0af8d74b90297b1859c5f050458fb49)_



- Contexts listing is performed via rpm-common-library [#21636]
- Ported plugin to smart-executor APIs 3.0.0 [#21618]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Creating uberjar in place of jar-with-dependecies
- Used smart-executor bom for better dependency management



###  accounting-insert-storage-se-plugin 2.0.0
Tagged commit: _[1054940ffe55106e52aa3106116763850b5e7c4f](https://code-repo.d4science.org/gCubeSystem/accounting-insert-storage-se-plugin/commit/1054940ffe55106e52aa3106116763850b5e7c4f)_

 

- Ported plugin to smart-executor APIs 3.0.0 [#21368]
- The plugin has been completely rewrote


###  storagehub 1.3.0
Tagged commit: _[f68588e05ce5bb499bb77485f3d03d31fb17e4bf](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/f68588e05ce5bb499bb77485f3d03d31fb17e4bf)_

 - [2021-03-31]

- possibility to impersonate people added


###  social-networking-library-ws 2.4.0
Tagged commit: _[9956a9ecda5f96594fd689e491b4dedcd45bb62f](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/9956a9ecda5f96594fd689e491b4dedcd45bb62f)_

 - 2021-04-30

- Feature #21179 porting to storagehub messages


###  icproxy 1.2.0
Tagged commit: _[d9e6f95837d34b14b21178437481b0bdbe9f585b](https://code-repo.d4science.org/gCubeSystem/ic-proxy/commit/d9e6f95837d34b14b21178437481b0bdbe9f585b)_

 - [2021-06-08]

- Feature #21584 added support for /ServiceEndpoint/{category} REST call


###  smart-executor 3.0.0
Tagged commit: _[7ca5c8ea33761053426372da4ceb534d4ad74423](https://code-repo.d4science.org/gCubeSystem/smart-executor/commit/7ca5c8ea33761053426372da4ceb534d4ad74423)_

 

- Switched smart-executor JSON management to gcube-jackson [#19647]
- Migrated Code from OrientDB 2.2.X APIs OrientDB 3.0.X APIs [#16123]
- Redesigned HTTP APIs to comply with RESTful architectural style [#12997]
- Added API to retrieve scheduled tasks [#10780]


###  ws-thredds-sync-widget 1.5.0
Tagged commit: _[ae3809efbd11eddcef80e2f36502321bd0d308cf](https://code-repo.d4science.org/gCubeSystem/ws-thredds-sync-widget/commit/ae3809efbd11eddcef80e2f36502321bd0d308cf)_

 - 2021-07-20

#### Enhancements

[#21346] Moved to AccessTokenProvider for UMA tokens context switches
[#21576] Adding filtering for gateway to get scopes with THREDDS role for users
Moved to maven-portal-bom 3.6.3
Including new version of ws-thredds


###  workspace-tree-widget 6.33.1
Tagged commit: _[027c05eec850f80ad53113b37352650025121f17](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/027c05eec850f80ad53113b37352650025121f17)_

 - 2021-06-11

#### Fixes

[#21575] Fixed icon associated with simple folders in the grid view
Moved to maven-portal-bom 3.6.3


###  rpt-token-portlet 1.0.0
Tagged commit: _[86b38c24540c170aa1dce649e46883e72a140a17](https://code-repo.d4science.org/gCubeSystem/rpt-token-portlet/commit/86b38c24540c170aa1dce649e46883e72a140a17)_

 - 2021-05-14

First release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
###  UsersManagementPortlet-portlet 4.0.0
Tagged commit: _[444aaa9937d0c2a9f16974c7799ee6623261a356](https://code-repo.d4science.org/gCubeSystem/UsersManagementPortlet-portlet/commit/444aaa9937d0c2a9f16974c7799ee6623261a356)_

 - 2021-05-13

- Feature #21269: make roles either assigned or removed in different operations, not mixed.
- Ported to git, see other changelog.xml for more changelogs.


###  PerformFISH-Apps-portlet 1.4.0
Tagged commit: _[6f1988df6ec584e8ca3d3032a2e112cb01124257](https://code-repo.d4science.org/gCubeSystem/PerformFISH-Apps-portlet/commit/6f1988df6ec584e8ca3d3032a2e112cb01124257)_

 - 2021-06-14

- [Feature #21319](https://support.d4science.org/issues/21319) Removed Home Library dependency and Ported to StorageHub 
 

###  gcube-ckan-datacatalog 2.0.1
Tagged commit: _[219a4c3b4e6f364e93a35b0875f37495842aca61](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/219a4c3b4e6f364e93a35b0875f37495842aca61)_

 - 2021-05-04

#### Enhancements

[#21188] Avoiding the catalogue widget window close if the user clicks outside it 
[#21470] Bug fixed publishing widget uses the orgTitle instead of orgName
[#20193] Switching to GR Catalogue with <AppId>gCat</AppId>. They will be updated by UpdateItemCatalogueResource class. 
Moved to maven-portal-bom 3.6.3 



###  join-vre 3.7.2
Tagged commit: _[1e1bb2e47b3b6ae80748ab7ea85dfe10a707dfc0](https://code-repo.d4science.org/gCubeSystem/join-vre/commit/1e1bb2e47b3b6ae80748ab7ea85dfe10a707dfc0)_

 - 2021-04-30

Removed a forgotten sysout in the code :(


###  workspace 6.27.1
Tagged commit: _[778e0a807eab26852925909900506527b84616f8](https://code-repo.d4science.org/gCubeSystem/workspace/commit/778e0a807eab26852925909900506527b84616f8)_

 - 2021-06-22

[#21575] Including ws-tree bug fix
Moved to maven-portal-bom 3.6.3


###  messages 2.5.0
Tagged commit: _[3e624e687a2d7a4f1a28851e784cac9ddaa5b22e](https://code-repo.d4science.org/gCubeSystem/messages/commit/3e624e687a2d7a4f1a28851e784cac9ddaa5b22e)_

 -2021-07-28

- Ported to git

- Remove HomeLibrary dependency and replace with storage hub one

- Temporarely removed possibility to add attachments 


###  workspace-widget-portlet 1.5.1
Tagged commit: _[80b24443e0222bd4037298c1c1611962d93c0247](https://code-repo.d4science.org/gCubeSystem/workspace-widget-portlet/commit/80b24443e0222bd4037298c1c1611962d93c0247)_

 - 2021-07-13

- Feature #21507 support new UMATokensProvider class
- Bug #21794 folder names were not URI encoded


###  accounting-manager 1.14.0
Tagged commit: _[b829c6bf2ff9e2147f2c6147a6fe72168d849db0](https://code-repo.d4science.org/gCubeSystem/accounting-manager/commit/b829c6bf2ff9e2147f2c6147a6fe72168d849db0)_

 - 2021-06-28

### Fixes

- Fixed display errors generated in Root scope [#21729]




###  create-users-portlet 3.0.1
Tagged commit: _[9ca0963b765eb3b8b1c9d5529c2b171070508997](https://code-repo.d4science.org/gCubeSystem/create-users-portlet/commit/9ca0963b765eb3b8b1c9d5529c2b171070508997)_

 - 2021-07-08

- Feature #21777, migrated to new AccessTokenProvider class
- Bug fix #21803, create-user-portlet may not add the yet created user to all the context at once


###  vre-deploy 4.4.0
Tagged commit: _[c494970967bfc6587de66ca6eff341691ff9739a](https://code-repo.d4science.org/gCubeSystem/vre-deploy/commit/c494970967bfc6587de66ca6eff341691ff9739a)_

 - 2021-07-08

- #21781 migrated to new AccessTokenProvider class

- #21157 removed home library dep, using shub for messaging


###  vre-definition 5.2.0
Tagged commit: _[c6a2a92db362464f11370c62fc852c5c8ece2865](https://code-repo.d4science.org/gCubeSystem/vre-definition/commit/c6a2a92db362464f11370c62fc852c5c8ece2865)_

 - 2021-07-13

- #21804 VRE-Definition portlet porting to git and remove HomeLibrary Dependency



###  social-mail-servlet 2.5.0
Tagged commit: _[f769c3d1ce16e0f5bc9c257bbc52e0546c09a478](https://code-repo.d4science.org/gCubeSystem/social-mail-servlet/commit/f769c3d1ce16e0f5bc9c257bbc52e0546c09a478)_

 - 2021-06-22

- Feature #21689 Social Mail Servlet to StorageHub migration

- Removed HomeLibrary Dependency


###  smartgears-distribution 3.4.1
Tagged commit: _[cf1ff69da0da271d40068b07110ba22e8d807a83](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/cf1ff69da0da271d40068b07110ba22e8d807a83)_

 - 2021-06-25

- Released to have and up-to-date distribution


###  gcube-portal-bundle 5.0.2
Tagged commit: _[5e2f174c8b1bd9a89c6453d2b0b51b5224feae2b](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/5e2f174c8b1bd9a89c6453d2b0b51b5224feae2b)_

 - 2021-06-24

- Added support for authorization common AccessToken provider and common-gcube-calls in CP



---
*generated by the gCube-ReleaseNotes pipeline from report 1030*

*last update Thu Aug 05 16:55:28 CEST 2021*
