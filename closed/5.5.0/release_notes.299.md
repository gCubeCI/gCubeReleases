# Release Notes for gCube 5.5.0

## What's New in gCube 5.5.0

* Integration of mediators and workflows for Blue-Cloud Data Discovery & Access Service https://data.blue-cloud.org/search
* New features and enhancements for the publication of geospatial projects (including documents, images and layers)
* Extension of StorageHub FUSE component to support UMA tokens 
* Release of DataMiner and NLPHUB to replace legacy parts (HomeLibrary related)

---
###  maven-parent 1.1.0
Tagged commit: _[8a77d953ab3460ec871ff06da53e6af06c748df0](https://code-repo.d4science.org/gCubeSystem/maven-parent/commit/8a77d953ab3460ec871ff06da53e6af06c748df0)_

 - 2020-01-27

### Features 

* New build profiles to support CI/CD
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+


###  storage-manager-core 3.0.0
Tagged commit: _[db61504f66ce71829a4bf120e8e2d27cd0127abd](https://code-repo.d4science.org/gCubeSystem/storage-manager-core/commit/db61504f66ce71829a4bf120e8e2d27cd0127abd)_

 2021-09-10
  * fix #22164
  * fix  #21980
  * update gcube-bom version
  * add close operation on IClient interface
  * add check on transport layer instance: if the memory type is not the same, a new transportLayer is instatiated
  * move memoryType var from super class TransportManager
  * convert BasicDBObject to DBObject the return type used for metadata collections
  * One pool for every operation: static Operation class; no mongo close operation
  * upgrade mongo-java-driver to 3.12.0
  * added input parameter to getSize method in order to be compatible with the needed of s3 client
  * moved from version 2.13.1 to 3.0.0-SNAPSHOT


###  storage-manager-wrapper 3.0.0
Tagged commit: _[a6d6c0535b8fec4805334d75d9e89cad26e443d6](https://code-repo.d4science.org/gCubeSystem/storage-manager-wrapper/commit/a6d6c0535b8fec4805334d75d9e89cad26e443d6)_

 2021-09-10
* bug fix #20505
* update gcub-bom version
* Change serviceEndpoint Category from DataStorage to Storage
* bugfix #20505
* adding new constructor with the backendType as input parameter
* retrieving specific backend credentials if a specific backend si specified as input parameter
* moved from 2.6.1 to 3.0.0-SNAPSHOT


###  keycloak-client 1.0.1
Tagged commit: _[21774d9a9103168fae956ddf1393cb65c3477b2b](https://code-repo.d4science.org/gCubeSystem/keycloak-client/commit/21774d9a9103168fae956ddf1393cb65c3477b2b)_


- First release (#21389 #22155) provides the basic helper classes for Keycloak tokens retrieve and functions for the gCube framework integration (automatic service discovery).
###  ckan2zenodo-library 1.0.2
Tagged commit: _[3d3034fa4e56cb90fb400c5e7578f104d5fcdd70](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-library.git/commit/3d3034fa4e56cb90fb400c5e7578f104d5fcdd70)_

 2021-07-30
- Introduced environemnt check [#19990](https://support.d4science.org/issues/19990) 



###  data-miner-manager-cl 1.9.0
Tagged commit: _[bd4239554673e7d16bd7f4d3ad2a4ccacaf86e68](https://code-repo.d4science.org/gCubeSystem/data-miner-manager-cl/commit/bd4239554673e7d16bd7f4d3ad2a4ccacaf86e68)_

 - 2021-10-06

### Features

 - Added cluster description in Service Info [#19213]




###  ecological-engine-smart-executor 1.6.6
Tagged commit: _[6a8cb56a756048e5057ee195701e0addcbd505fb](https://code-repo.d4science.org/gCubeSystem/ecological-engine-smart-executor/commit/6a8cb56a756048e5057ee195701e0addcbd505fb)_

 - 2021-10-06

### Fixes

- Fixed obsolete short urls [#20971]




###  ecological-engine-geospatial-extensions 1.5.3
Tagged commit: _[b5622f59f3eeb0da79e049faabe2387b859943cb](https://code-repo.d4science.org/gCubeSystem/ecological-engine-geospatial-extensions/commit/b5622f59f3eeb0da79e049faabe2387b859943cb)_

 - 2021-05-24

### Fixes

- Fixed obsolete short urls [#20971]



###  seadatanet-connector 1.2.3
Tagged commit: _[89ebf8c412f541f165f1ac39b91222e4ddf9a809](https://code-repo.d4science.org/gCubeSystem/seadatanet-connector/commit/89ebf8c412f541f165f1ac39b91222e4ddf9a809)_

 - 2021-04-26

### Features

- update to dismiss log4j [#21268]



###  dataminer 1.7.1
Tagged commit: _[fbade3e930a1b157006427e6b78626c86545b24d](https://code-repo.d4science.org/gCubeSystem/dataminer/commit/fbade3e930a1b157006427e6b78626c86545b24d)_

 - 2021-05-24

### Fixes

- Fixed obsolete short urls [#20971]



###  sh-fuse-integration 2.0.0
Tagged commit: _[7bb47dc616d5fc343ccd15676da68e4ca85c36ae](https://code-repo.d4science.org/gCubeSystem/sh-fuse-integration/commit/7bb47dc616d5fc343ccd15676da68e4ca85c36ae)_

 2021-05-19

### Feature
	
-  porting to uma token [#21441]



###  wps 1.1.8
Tagged commit: _[16e38c3496fd1beae6e46d38e01ebf82e43fca75](https://code-repo.d4science.org/gCubeSystem/wps/commit/16e38c3496fd1beae6e46d38e01ebf82e43fca75)_

 - 2021-05-17

### Features

- Updated pom.xml to add to the war the new libraries released to solve slf4j-over-log4j issue [#11956]




###  gcube-cms-suite 1.0.0
Tagged commit: _[86c1fed26327361155ac251a848ca2db5c5a1fbe](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/86c1fed26327361155ac251a848ca2db5c5a1fbe)_

 - 2021-2-11
First release
###  geoportal-common 1.0.7
Tagged commit: _[86c1fed26327361155ac251a848ca2db5c5a1fbe](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/86c1fed26327361155ac251a848ca2db5c5a1fbe)_

Tags 1.0.7 / 5.5.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  cms-test-commons 1.0.0
Tagged commit: _[86c1fed26327361155ac251a848ca2db5c5a1fbe](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/86c1fed26327361155ac251a848ca2db5c5a1fbe)_

 - 2021-2-11
First release
###  geoportal-service 1.0.6
Tagged commit: _[86c1fed26327361155ac251a848ca2db5c5a1fbe](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/86c1fed26327361155ac251a848ca2db5c5a1fbe)_

Tags 1.0.6 / 5.5.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  geoportal-client 1.0.5
Tagged commit: _[86c1fed26327361155ac251a848ca2db5c5a1fbe](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/86c1fed26327361155ac251a848ca2db5c5a1fbe)_

Tags 1.0.5 / 5.5.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  use-cases 1.0.0
Tagged commit: _[86c1fed26327361155ac251a848ca2db5c5a1fbe](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/86c1fed26327361155ac251a848ca2db5c5a1fbe)_

 - 2021-2-11
First release
###  gCat-Controller 1.0.6
Tagged commit: _[faf6e263f9236ba963f5466ccb84545142a8a689](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/faf6e263f9236ba963f5466ccb84545142a8a689)_

gCat client coordinates

###  nlphub 1.1.0
Tagged commit: _[97498c259d1e48db79133707c6961b3ed61394ad](https://code-repo.d4science.org/gCubeSystem/nlphub/commit/97498c259d1e48db79133707c6961b3ed61394ad)_

 - 2021-09-08

### Features

- Updated to StorageHub [#21970]




###  storagehub 1.4.0
Tagged commit: _[647dbb651abacbafc41749f57a81f71f90c74e7d](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/647dbb651abacbafc41749f57a81f71f90c74e7d)_

 - [2021-10-07]

- slow query removed from VRE retrieving and recents
- incident #22184 solved


###  openlayer-basic-widgets 1.4.0
Tagged commit: _[f2ddcf3915e97b068f6b785ba7746d0f1c1087da](https://code-repo.d4science.org/gCubeSystem/openlayer-basic-widgets/commit/f2ddcf3915e97b068f6b785ba7746d0f1c1087da)_

 - 2021-10-06

### Features

- Updated dependencies




###  geoportal-data-common 1.2.0
Tagged commit: _[9e500a23d0f14ad7ac71e10215f5e3a12a69038e](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/9e500a23d0f14ad7ac71e10215f5e3a12a69038e)_

 - 2021-09-29

#### Enhancements

- [#20595] Porting common model
- [#21890] Passed to mongoID
- [#22002] Integrated with ValidationReport bean
- [#22040] Revisited the Abstract Relazione di Scavo
- Passed to gcube-bom 2.0.1



###  event-publisher-hook 1.1.1
Tagged commit: _[8a2a31c0f4f07ef76fdf39fa357e3a523160d38e](https://code-repo.d4science.org/gCubeSystem/event-publisher-hook/commit/8a2a31c0f4f07ef76fdf39fa357e3a523160d38e)_


 don't send events if the group is not enabled (#21925)


###  netcdf-basic-widgets 1.2.0
Tagged commit: _[41aa76880be2d42751659d589db0be6a289570f1](https://code-repo.d4science.org/gCubeSystem/netcdf-basic-widgets/commit/41aa76880be2d42751659d589db0be6a289570f1)_

 - 2021-10-06

### Fixed

- Updated dependencies




###  ckan2zenodo-publisher-widget 1.1.0
Tagged commit: _[18ae6c2fc17db9eb0d2f5b416e773dd6d92c7c9f](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-publisher-widget/commit/18ae6c2fc17db9eb0d2f5b416e773dd6d92c7c9f)_

 - 2021-10-05

**New Features**
[#19988] Integrated with  reporting the status of Upload to Zenodo facility
Moved to  3.6.3


###  statistical-algorithms-importer 1.15.0
Tagged commit: _[10e1dcc0cc2ae10ef646e22bc40f1e7aefc4e5bf](https://code-repo.d4science.org/gCubeSystem/statistical-algorithms-importer/commit/10e1dcc0cc2ae10ef646e22bc40f1e7aefc4e5bf)_

 - 2021-10-06

### Features

 - Added cluster description in Service Info [#19213]



###  gcube-ckan-datacatalog 2.1.0
Tagged commit: _[9172838d87e70bbe631ed91e571921bc2f3df992](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/9172838d87e70bbe631ed91e571921bc2f3df992)_

 - 2021-10-05

#### Enhancements

[#19988] Integrated with  to show or not the Upload to Zenodo facility


###  data-miner-manager 1.12.0
Tagged commit: _[8e73591b8d2f570ef2e7fbd00377eb1968f1bdbd](https://code-repo.d4science.org/gCubeSystem/data-miner-manager/commit/8e73591b8d2f570ef2e7fbd00377eb1968f1bdbd)_

 - 2021-10-06

### Features

 - Added cluster description in Service Info [#19213]




###  data-miner-executor 1.3.0
Tagged commit: _[ccc1e1bf613c38061ae44b4caaa78173b05f4789](https://code-repo.d4science.org/gCubeSystem/data-miner-executor/commit/ccc1e1bf613c38061ae44b4caaa78173b05f4789)_

 - 2021-10-06

### Features

 - Added cluster description in Service Info [#19213]




###  ddas-vre-servlet 1.0.0
Tagged commit: _[259ed7bd6a3dd6556e0db2e033d7c6dd71cdfbb7](https://code-repo.d4science.org/gCubeSystem/ddas-vre-servlet/commit/259ed7bd6a3dd6556e0db2e033d7c6dd71cdfbb7)_

 - 2021-09-07

- First release
###  VREApp-Integration-portlet 1.1.0
Tagged commit: _[af432c083fa391e7c09d6baa7fe8b50c888b01be](https://code-repo.d4science.org/gCubeSystem/VREApp-Integration-portlet/commit/af432c083fa391e7c09d6baa7fe8b50c888b01be)_

 - 2021-07-13

- Ported to git



###  vre-manager-portlet 6.1.0
Tagged commit: _[a99f657f2bb663693e30f565bdb14beb52ba4064](https://code-repo.d4science.org/gCubeSystem/vre-manager-portlet/commit/a99f657f2bb663693e30f565bdb14beb52ba4064)_

 - 2021-10-13

- Added possibility to postpone VRE expiration time of 5 years


###  catalogue-badge-portlet 1.1.1
Tagged commit: _[2fed7bccf5a6499f6dcf0f163524df55de237c8f](https://code-repo.d4science.org/gCubeSystem/catalogue-badge-portlet/commit/2fed7bccf5a6499f6dcf0f163524df55de237c8f)_

 - 2021-10-13

- Feature #22177, Modify Catalogue widget portlet to display the types and the number when in root VO



###  geoportal-data-entry-app 2.0.0
Tagged commit: _[eb8dceb8ff77611092b0f516645f2b1ff0c70a1d](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/eb8dceb8ff77611092b0f516645f2b1ff0c70a1d)_

 - 2021-09-29

#### Enhancements

- [#20435] Client integration with MongoConcessioni 
- Moved to maven-portal-bom 3.6.3
- [#21856] Implemented new user feedback
- [#21890] Passed to mongoID
- [#20599] Get List of Records
- [#22002] Integrated with ValidationReport and status
- [#21990] Provide the (first version of) edit mode
- [#22040] Revisited the Abstract Relazione di Scavo



###  geoportal-data-viewer-app 2.0.0
Tagged commit: _[17099e9a8f478466c2c4f2ea6e8f60a080ad9ac9](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/17099e9a8f478466c2c4f2ea6e8f60a080ad9ac9)_

 - 2021-07-30

#### Enhancements

- [#21890] Porting to ConcessioniManagerI and pass to mongoID
- [#20595] Porting and using the model view provided by geoportal-common
- [#21946] Show layers of a concessione automatically on map according to zoom level
- [#21976] Access policies checked on server-side
- [#22042] Implemented the public access
- [#22040] Revisited the Abstract and Relazione di Scavo
- [#22041] Files have been ported to FileSet model
- [#21991] Implemented the Layer Switcher



---
*generated by the gCube-ReleaseNotes pipeline from report 1069*

*last update Tue Oct 26 18:03:33 CEST 2021*
