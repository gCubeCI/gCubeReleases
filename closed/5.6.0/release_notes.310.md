# Release Notes for gCube 5.6.0


## What's New in gCube 5.6.0

* Release of several portlets to replace legacy parts (HomeLibrary related)
* Grsf-service doesn't publish time-dependent metadata anymore
* Fix minimal privilege issue on smartgears distribution
* Improvements on algorithms pubblication in BlueCloud context


---
###  common-jaxrs-client 1.0.4
Tagged commit: _[eb39d4fad0d7b862691b99a7615b5901dfc93a9b](https://code-repo.d4science.org/gCubeSystem/common-jaxrs-clients/commit/eb39d4fad0d7b862691b99a7615b5901dfc93a9b)_

 - [2021-10-20]

- commit for gxHTTP problem
###  common-smartgears 3.1.1
Tagged commit: _[8455825bb1b8265d8b1e782e07167b818a51473e](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/8455825bb1b8265d8b1e782e07167b818a51473e)_

 - 2021-09-29

- minimal privilege granted also on empty resource_access in JWT token


###  uri-resolver-manager 1.5.0
Tagged commit: _[f124c6ea3640317128ae03e9c9a831d08225807d](https://code-repo.d4science.org/gCubeSystem/uri-resolver-manager/commit/f124c6ea3640317128ae03e9c9a831d08225807d)_

 - 2021-11-05

#### Enhancement

* [22385] Integrated with Catalogue Resolver and SHUB Resolver


###  spd-client-library 4.1.0
Tagged commit: _[21d16362b3889de017b1a264038e749318eb22e2](https://code-repo.d4science.org/gCubeSystem/spd-client-library/commit/21d16362b3889de017b1a264038e749318eb22e2)_

 - [2021-10-20]

- commit for first build on jenkins
###  resource-updater-cli 1.2.0
Tagged commit: _[421862bde8979291c94026e98f3b8f82201b40bb](https://code-repo.d4science.org/gCubeSystem/resource-updater-cli/commit/421862bde8979291c94026e98f3b8f82201b40bb)_

 2021-11-04
 * add new mainclass in order to retrieve backends informations from haproxy
 * migrated to git
 * new feauture: read some package versions from system #22053
 * bugfix: #22068


###  DataMinerAlgorithmsCrawler 1.0.5
Tagged commit: _[6e8ea70e1cd7630cb6b72118c91712d288ff2ee8](https://code-repo.d4science.org/gCubeSystem/gFeed/commit/6e8ea70e1cd7630cb6b72118c91712d288ff2ee8)_

* Fixes #22344 : publish DM algorithms as Methods

###  grsf-publisher-ws 1.13.0
Tagged commit: _[8e0bdf0df3967757c146a51760ebb46100d0b9ab](https://code-repo.d4science.org/gCubeSystem/grsf-publisher-ws/commit/8e0bdf0df3967757c146a51760ebb46100d0b9ab)_



### Added

- Added support to not include in records time dependant metadata [#21995]

### Changed

- Switched dependency management to gcube-bom 2.0.0
- Migrated code to storagehub [#21432]



###  workspace-tree-widget 6.34.0
Tagged commit: _[d4f64e35ed57838a3cf8ed7fdeda59d991935b25](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/d4f64e35ed57838a3cf8ed7fdeda59d991935b25)_

 - 2021-11-05

#### Enhancements

- [#22251] Make workspace file size field smart


###  system-service-definition-portlet 1.0.0
Tagged commit: _[cde604b19a42444a3aed1e97da92a54f426e20b3](https://code-repo.d4science.org/gCubeSystem/system-service-definition-portlet/commit/cde604b19a42444a3aed1e97da92a54f426e20b3)_

 - 2021-10-20

### Fixes

- First Release [#21362]
###  vmereports-manager-portlet 6.3.0
Tagged commit: _[d797adc8218a95a52ced442d3e5e590e819cf513](https://code-repo.d4science.org/gCubeSystem/vmereports-manager-portlet/commit/d797adc8218a95a52ced442d3e5e590e819cf513)_

 - 2021-10-13

- Ported to git

- Removed HomeLibrary dependency



###  user-statistics 2.3.1
Tagged commit: _[3b827f3004768de0bdc4f7d2f1c97f6362527634](https://code-repo.d4science.org/gCubeSystem/user-statistics/commit/3b827f3004768de0bdc4f7d2f1c97f6362527634)_

 - 2021-10-28

- Removed HomeLibrary
- Updated GWT version to latest 


###  aquamapsspeciesview 1.3.9
Tagged commit: _[29831363ea2784004dfe652d7a4da053e48cab7e](https://code-repo.d4science.org/gCubeSystem/aquamapsspeciesview/commit/29831363ea2784004dfe652d7a4da053e48cab7e)_

 - 2021-2-11
Import from SVN http://svn.d4science.org/public/d4science/gcube/branches/application/AquaMaps/AquaMapsSpeciesView/1.3
Removed dependency toward HL
###  species-discovery 3.11.0
Tagged commit: _[b48cc6efb2b6dbe00540062136a0ab05304a947a](https://code-repo.d4science.org/gCubeSystem/species-discovery/commit/b48cc6efb2b6dbe00540062136a0ab05304a947a)_

 - 27-10-2021

- [#21969] Removed HL dependency
- Ported to maven-portal-bom v3.6.3
- Ported to workspace-explorer v2.X.Y
- Ported to geonetwork [3.4.5,4.0.0-SNAPSHOT)
- Ported to storagehub-client-wrapper [1.0.0, 2.0.0-SNAPSHOT)
- Ported to  spd-client-library [4.1.0-SNAPSHOT, 5.0.0-SNAPSHOT)


###  workspace 6.28.0
Tagged commit: _[b614f368b0cd6e9e386f9a15be5e2d7960565db4](https://code-repo.d4science.org/gCubeSystem/workspace/commit/b614f368b0cd6e9e386f9a15be5e2d7960565db4)_

 - 2021-11-05

#### Enhancements

- [#22251] Make workspace file size field smart


###  geoportal-data-viewer-app 2.0.1
Tagged commit: _[baf5777f72590e4717cb485a22ec72fea845de9b](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/baf5777f72590e4717cb485a22ec72fea845de9b)_

 - 2021-11-10

#### Fixes

- [#22370] Use a max-height for preview image shown in the pop-up


###  smartgears-distribution 3.4.2
Tagged commit: _[1e253171979b51102e347314287abad7b75c1a19](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/1e253171979b51102e347314287abad7b75c1a19)_

 - 2021-11-08

- common-smartgrears library updated
    



---
*generated by the gCube-ReleaseNotes pipeline from report 1085*

*last update Thu Nov 25 11:53:42 CET 2021*
