# Release Notes for gCube 5.7.0

## What's New in gCube 5.7

* gCat: added support for the moderated publication process and for the management of the trash;
* Accounting-Manager portlet enhanced to exploit a new persistent storage;
* Geoportal enhanced for the management of roles: Data-Member (as default), Data-Manager, Data-Editor;
* Join-vre: process enhanced to require a mandatory Motivation when someone asks to join a VRE


---
###  keycloak-client 1.1.0
Tagged commit: _[7d3f50895485735fdc2d62f18073198c12584039](https://code-repo.d4science.org/gCubeSystem/keycloak-client/commit/7d3f50895485735fdc2d62f18073198c12584039)_


- Added refresh token facilities for expired tokens (#22515) and some helper methods added.


###  authorization-utils 1.0.0
Tagged commit: _[464515262ff40cdf36bc207d23ac595788889df3](https://code-repo.d4science.org/gCubeSystem/authorization-utils/commit/464515262ff40cdf36bc207d23ac595788889df3)_

 

- First Release
###  gcat-api 2.1.0
Tagged commit: _[b5dacfbfc4d37b4db39a8033fc3b4b17dd2de735](https://code-repo.d4science.org/gCubeSystem/gcat-api/commit/b5dacfbfc4d37b4db39a8033fc3b4b17dd2de735)_



- Added query parameter social_post_notification to override default VRE behaviour [#21345]
- Added support for moderation [#21342]
- Added items bulk delete/purge [#21685]
- Added empty trash API [#13322]



###  gcat-client 2.1.0
Tagged commit: _[7f3ea5da1403f817102cefe45323f1f83f80f0f8](https://code-repo.d4science.org/gCubeSystem/gcat-client/commit/7f3ea5da1403f817102cefe45323f1f83f80f0f8)_



- Added query parameter social_post_notification to override default VRE behaviour [#21345]
- Added support for moderation [#21342]
- Added items bulk delete/purge [#21685]
- Added empty trash API [#13322]



###  accounting-analytics 4.0.0
Tagged commit: _[7856800f23459c6c257191352b988f9d35d3e154](https://code-repo.d4science.org/gCubeSystem/accounting-analytics/commit/7856800f23459c6c257191352b988f9d35d3e154)_



- Supporting zero or multiple contexts in all queries [#21353]
- Query parameters are now provided via setter and not as functions parameters [#21353]
- Removed getSpaceProvidersIds() and added getDataType() [#21353]


###  accounting-analytics-persistence-postgresql 2.0.0
Tagged commit: _[6e0d1c81e36fba6172c3079b2c129d2cd9caf199](https://code-repo.d4science.org/gCubeSystem/accounting-analytics-persistence-postgresql/commit/6e0d1c81e36fba6172c3079b2c129d2cd9caf199)_



- Added possibility of having multiple values for the same key which will be used in OR [#21353]


###  storagehub-application-persistence 3.0.0
Tagged commit: _[3d8b28b5aaf88f9615e8ce122b940bf1a1bc04a3](https://code-repo.d4science.org/gCubeSystem/storagehub-application-persistence/commit/3d8b28b5aaf88f9615e8ce122b940bf1a1bc04a3)_



- Removed code which is now part of authorization-utils [#22472]



###  geoportal-data-common 1.3.0
Tagged commit: _[29609dc7e08cb394bd1828cbc2f016f3ecb97619](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/29609dc7e08cb394bd1828cbc2f016f3ecb97619)_

 - 2021-12-03

#### Enhancements

- [#22506] Added classes to read configurations and common classes from the GNA components: DataEntry and DataViewer


###  catalogue-util-library 1.0.3
Tagged commit: _[82d73232516687e01cee509da7a8109a9774c3c7](https://code-repo.d4science.org/gCubeSystem/catalogue-util-library/commit/82d73232516687e01cee509da7a8109a9774c3c7)_

 - 2022-01-21

**Fixes**
- [#22691] Share Link on private items does not work


###  gcat 2.1.0
Tagged commit: _[5ac1039d6088d97a4b390a957fbf050dbb376729](https://code-repo.d4science.org/gCubeSystem/gcat/commit/5ac1039d6088d97a4b390a957fbf050dbb376729)_



- Added query parameter social_post_notification to override default VRE behaviour [#21345]
- Users are created/referenced in the form <Surname Name> and not vice-versa [#21479]
- Added support for moderation [#21342]
- Added support for IAM authz [#21628] 
- Added items bulk delete/purge [#22299]
- Using UriResolverManager to get item URL in place of direct HTTP call [#22549]
- Added empty trash API [#13322]



###  workspace-explorer-app 1.4.0
Tagged commit: _[fc719424f560db11800f74c1d84b268541fc2869](https://code-repo.d4science.org/gCubeSystem/workspace-explorer-app/commit/fc719424f560db11800f74c1d84b268541fc2869)_

 - 2021-12-03

[#19786] Integrated items of type URL


###  ckan-connector 1.3.0
Tagged commit: _[ba55624d4f896e5b39e1030c8e72b292686f75d1](https://code-repo.d4science.org/gCubeSystem/ckan-connector/commit/ba55624d4f896e5b39e1030c8e72b292686f75d1)_



- updated gcat-client version [#21530]
- adoption of gcube-smartgears-bom.2.1.0
###  gcube-cms-suite 1.0.1
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-12-07
- Introduced cms-plugin-framework
- Introduced concessioni use case
- Fixed internal group ids


###  geoportal-common 1.0.8
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-11-10
- Fixes [#22369](https://support.d4science.org/issues/22369)
- Fixes [#22338](https://support.d4science.org/issues/22338)
- Profiled Documents
- Changed group id 

###  cms-plugin-framework 1.0.0
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-09-20
- First release
###  cms-test-commons 1.0.1
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-09-11
- Introduced profiled documents

###  concessioni-model 1.0.0
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-12-15
- First release
###  concessioni-lifecycle 1.0.0
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-12-15
- First release
###  geoportal-service 1.0.8
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-09-20
- Logging
- Fixes [#22193](https://support.d4science.org/issues/22193)
- Fixes [#22280](https://support.d4science.org/issues/22280)
- Fixes [#20755](https://support.d4science.org/issues/20755)
- Profiled Documents

###  geoportal-client 1.0.6
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-09-20
- Changed artifact dependencies
- Default Profiled Documents client
###  use-cases 1.0.1
Tagged commit: _[ee2f72041e7ddee17d0faf333cdd8e2faf39fe88](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/ee2f72041e7ddee17d0faf333cdd8e2faf39fe88)_

 - 2021-09-20
- Updated parent

###  uri-resolver 2.6.1
Tagged commit: _[51c40079f7d819bc29eb1fb533c241d854964c1c](https://code-repo.d4science.org/gCubeSystem/uri-resolver/commit/51c40079f7d819bc29eb1fb533c241d854964c1c)_

 - 2022-01-17

**New features**

- [#21411] add forceClose method from storage-manager, used for closing mongodb connections related to the old https urls (managed by storage-manager)
- migrated to storage-manager3

**Bug fixes**

- [#21560] Checking the bug fix done
- Moved to gcube-smartgears-bom.2.1.0


###  workspace-tree-widget 6.34.1
Tagged commit: _[751e9b9b5e6efb6db043bbb532fd320c2d679a9e](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget/commit/751e9b9b5e6efb6db043bbb532fd320c2d679a9e)_

 - 2021-12-20

#### Fixes

- [#22578] GetInfo facility invoked by Tree View does not display properly the Owner field


###  workspace 6.28.1
Tagged commit: _[51cd46d50e9067529b9355a6e23670150453c451](https://code-repo.d4science.org/gCubeSystem/workspace/commit/51cd46d50e9067529b9355a6e23670150453c451)_

 - 2021-12-20

#### Fixes

- [#22578] Including workspace-tree-widget bug fixing


###  accounting-manager 1.15.0
Tagged commit: _[0650a1ed5e5750a1c22e2a55f9df68b71ab16502](https://code-repo.d4science.org/gCubeSystem/accounting-manager/commit/0650a1ed5e5750a1c22e2a55f9df68b71ab16502)_

 - 2021-11-30

### Features

-  Improve query support of the new persistence [#21354]



###  resource-management-portlet 7.1.0
Tagged commit: _[15564d13546859375d79ec718d8013f075a1c087](https://code-repo.d4science.org/gCubeSystem/resource-management-portlet/commit/15564d13546859375d79ec718d8013f075a1c087)_

 - 2021-12-27

- New Feature: implemented the locate funcion to indicate the infra gateway through which a VRE is accessible [#22340] 


###  join-vre 3.7.3
Tagged commit: _[1f80c0ee15ea664395ea357e4e1dabca649de902](https://code-repo.d4science.org/gCubeSystem/join-vre/commit/1f80c0ee15ea664395ea357e4e1dabca649de902)_

 - 2022-01-03

- Make the motivation mandatory when someone asks to join a VRE


###  sbd-uploadshare-portlet 1.2.0
Tagged commit: _[cca716aa6e0f20848de27962549abed5bd79f344](https://code-repo.d4science.org/gCubeSystem/sbd-uploadshare-portlet/commit/cca716aa6e0f20848de27962549abed5bd79f344)_

 - 2021-12-17

- Task #22596 Ported to git and removed Home Library dep.



###  geoportal-data-entry-app 2.1.0
Tagged commit: _[0cd6880a54702958ffb59410b4a05b079ee45e63](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/0cd6880a54702958ffb59410b4a05b079ee45e63)_

 - 2021-11-24

#### Enhancements

- [#22455] Integrated with roles: (Data-Member as default), Data-Manager, Data-Editor
- [#22287] Integrated with base search, ordering and filtering facility provided by MongoDB
- [#22506] Re-engineered the common utilities



###  geoportal-data-viewer-app 2.2.0
Tagged commit: _[03702b7148b2e1a512c309f5ff163b5f0a42e233](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/03702b7148b2e1a512c309f5ff163b5f0a42e233)_

 - 2021-12-07

#### Enhancements

- [#22518] Added the search for fields facility


###  gcube-ckan-datacatalog 2.1.1
Tagged commit: _[9bce1d0647914de0e472742bc0e385e13794ec54](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/9bce1d0647914de0e472742bc0e385e13794ec54)_

 - 2022-01-21

- Just to release the fix #22691



---
*generated by the gCube-ReleaseNotes pipeline from report 1101*

*last update Thu Jan 27 16:35:03 CET 2022*

