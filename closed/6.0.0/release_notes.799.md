# Release Notes for gCube 6.0.0

## What's New in gCube 6.0.0

* New version of StorageHub service;

---
###  gcube-bom 4.0.0
Tagged commit: _[301d8ec6ced67ef4e66b119b9360f425189167f2](https://code-repo.d4science.org/gCubeSystem/gcube-bom/commit/301d8ec6ced67ef4e66b119b9360f425189167f2)_



 - Moved to jakarta
 - Moved to java11 



###  smart-executor-bom 3.3.0
Tagged commit: _[d054921c934f834804fd5b3c7666e3e9050751fc](https://code-repo.d4science.org/gCubeSystem/smart-executor-bom/commit/d054921c934f834804fd5b3c7666e3e9050751fc)_



- Upgraded gcube-smartgears-bom to 2.5.1 [#27999]
- Updated maven-parent version to 1.2.0



###  oidc-library 1.3.3
Tagged commit: _[57f590f068c6f99f01bfaf5d4cd40a34d14f8054](https://code-repo.d4science.org/gCubeSystem/oidc-library/commit/57f590f068c6f99f01bfaf5d4cd40a34d14f8054)_


- Added JSON parsed instance of the refresh token to  class, the getter and methods to checks its presence and expiration


###  gcube-smartgears-bom 4.0.0
Tagged commit: _[9ecb7a6ebd73600df36332699381c4a69351e36e](https://code-repo.d4science.org/gCubeSystem/gcube-smartgears-bom/commit/9ecb7a6ebd73600df36332699381c4a69351e36e)_



- Added common-scope 
- Moved to jakarta libraries
- Moved to java11 libraries



###  maven-portal-bom 4.0.0
Tagged commit: _[11682332eef2279895908728bff05fdece751e80](https://code-repo.d4science.org/gCubeSystem/maven-portal-bom/commit/11682332eef2279895908728bff05fdece751e80)_



- removed deps for social networking library
- added social service client dep


###  common-security 1.0.0
Tagged commit: _[08914cd887c7b7cfb1872cbe892f36ac1f2a9913](https://code-repo.d4science.org/gCubeSystem/common-security/commit/08914cd887c7b7cfb1872cbe892f36ac1f2a9913)_




- First Release


###  discovery-client 2.0.0
Tagged commit: _[f82c3737e14b53e8093a3a45dedbfd6dc42cb9d6](https://code-repo.d4science.org/gCubeSystem/discovery-client/commit/f82c3737e14b53e8093a3a45dedbfd6dc42cb9d6)_



- Moved to jakarta


###  common-gcore-resources 2.0.0
Tagged commit: _[348c734b62b7a62b1039a36fe2ad6e47cb52aeff](https://code-repo.d4science.org/gCubeSystem/common-gcore-resources/commit/348c734b62b7a62b1039a36fe2ad6e47cb52aeff)_



- Moved to jakarta 



###  geo-utility 1.3.0
Tagged commit: _[528b6a4131f757e1de42f273508da5e67feb1b77](https://code-repo.d4science.org/gCubeSystem/geo-utility/commit/528b6a4131f757e1de42f273508da5e67feb1b77)_



- Improved WFS builder [#26026]


###  storagehub-model 2.0.0
Tagged commit: _[a618b55cecbae8af77e0e4344752c8bd12823737](https://code-repo.d4science.org/gCubeSystem/storagehub-model/commit/a618b55cecbae8af77e0e4344752c8bd12823737)_



 - changes in the item model


###  common-scope 2.0.0
Tagged commit: _[6a2d5435dbd81520dc969d0e8524e85d9528992c](https://code-repo.d4science.org/gCubeSystem/common-scope/commit/6a2d5435dbd81520dc969d0e8524e85d9528992c)_



- Maps scanned with reflection
- Dependency to common-configuration-scanner removed
- Removed ScopePorvider and ScopeBean


###  common-authorization 3.0.0
Tagged commit: _[c5ac47b3e825d4106c3d12e2caaf467d075d4de0](https://code-repo.d4science.org/gCubeSystem/common-authorization/commit/c5ac47b3e825d4106c3d12e2caaf467d075d4de0)_

 

- Authorization endpoints configuration retrieving moved from common scanner to reflection library
- Removed common-scope from the dependencies



###  common-clients 3.0.0
Tagged commit: _[d13af3b04469895df985a788ae6ab460ce74e19a](https://code-repo.d4science.org/gCubeSystem/common-clients/commit/d13af3b04469895df985a788ae6ab460ce74e19a)_



- Porting to gcube bom 4.0.0


###  common-gcube-calls 2.0.0
Tagged commit: _[7b6877c0105caae5f2cdb5e9201ee472f728ee09](https://code-repo.d4science.org/gCubeSystem/common-gcube-calls/commit/7b6877c0105caae5f2cdb5e9201ee472f728ee09)_



- Porting to new Secret Manager


###  common-gcore-stubs 2.0.0
Tagged commit: _[04945da3ee20f05478d0456976a75bade6297b0e](https://code-repo.d4science.org/gCubeSystem/common-gcore-stubs/commit/04945da3ee20f05478d0456976a75bade6297b0e)_



- Porting to new Secret Manager
###  gxREST 2.0.0
Tagged commit: _[c29aee215a32489bf616b8aa84b3f1da1da77c0d](https://code-repo.d4science.org/gCubeSystem/gx-rest/commit/c29aee215a32489bf616b8aa84b3f1da1da77c0d)_



- Removed all the old providers
- Removed set of gcube headers
- Removed isExtCall because authorization headers are not added automatically anymore


###  authorization-client 3.0.0
Tagged commit: _[9f3914dd5b5078b27aaf0f324ba74969badad662](https://code-repo.d4science.org/gCubeSystem/authorization-client/commit/9f3914dd5b5078b27aaf0f324ba74969badad662)_

 

- Moved to jakarta


###  gxHTTP 2.0.0
Tagged commit: _[c29aee215a32489bf616b8aa84b3f1da1da77c0d](https://code-repo.d4science.org/gCubeSystem/gx-rest/commit/c29aee215a32489bf616b8aa84b3f1da1da77c0d)_



- Removed all the old providers
- Removed set of gcube headers
- Removed isExtCall because authorization headers are not added automatically anymore


###  gxJRS 2.0.0
Tagged commit: _[c29aee215a32489bf616b8aa84b3f1da1da77c0d](https://code-repo.d4science.org/gCubeSystem/gx-rest/commit/c29aee215a32489bf616b8aa84b3f1da1da77c0d)_



- Removed all the old providers
- Removed set of gcube headers
- Removed isExtCall because authorization headers are not added automatically anymore


###  common-encryption 2.0.0
Tagged commit: _[f67685184e7fe78dbdf2ad76bbe67ba1aeb08b95](https://code-repo.d4science.org/gCubeSystem/common-encryption/commit/f67685184e7fe78dbdf2ad76bbe67ba1aeb08b95)_



- Removed old Providers


###  common-jaxws-calls 2.0.0
Tagged commit: _[b17f0ac413def290b3f887c1b6b7405aa5387327](https://code-repo.d4science.org/gCubeSystem/common-jaxws-calls/commit/b17f0ac413def290b3f887c1b6b7405aa5387327)_



- Porting to new Secret Manager


###  common-jaxrs-client 2.0.0
Tagged commit: _[d31e9af21678d9680cebca78e689199877c0f09a](https://code-repo.d4science.org/gCubeSystem/common-jaxrs-clients/commit/d31e9af21678d9680cebca78e689199877c0f09a)_



- Porting to new Secret Manager


###  ic-client 2.0.0
Tagged commit: _[82b51afff33641a9a623c81fa77dcf9936d2e8f0](https://code-repo.d4science.org/gCubeSystem/ic-client/commit/82b51afff33641a9a623c81fa77dcf9936d2e8f0)_



- Porting to Secret Manager
###  health-api 1.0.0
Tagged commit: _[7d0288404a38f3dffa2ee0514f1782ea12725bc7](https://code-repo.d4science.org/gCubeSystem/health-api/commit/7d0288404a38f3dffa2ee0514f1782ea12725bc7)_

 

- First release


###  common-clients 3.0.0
Tagged commit: _[d13af3b04469895df985a788ae6ab460ce74e19a](https://code-repo.d4science.org/gCubeSystem/common-clients/commit/d13af3b04469895df985a788ae6ab460ce74e19a)_



- Porting to gcube bom 4.0.0


###  gcube-secrets 1.0.0
Tagged commit: _[5c81a206d53a665a23ebb2472aef89c66362b86d](https://code-repo.d4science.org/gCubeSystem/gcube-secrets/commit/5c81a206d53a665a23ebb2472aef89c66362b86d)_

 

- First Release


###  social-service-model 1.3.0
Tagged commit: _[79a725b43a27b1ec7f9cf2a46b40f3726fc49042](https://code-repo.d4science.org/gCubeSystem/social-service-model/commit/79a725b43a27b1ec7f9cf2a46b40f3726fc49042)_



- Feature #27999 [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0
- moved to gcube-smartgears-bom 2.5.1
- removed gcube-bom dependency


###  social-networking-library 2.1.0
Tagged commit: _[a786cc7eb1f9895ee79d8ae7f02df48083227019](https://code-repo.d4science.org/gCubeSystem/social-networking-library/commit/a786cc7eb1f9895ee79d8ae7f02df48083227019)_



 - maven-portal-bom 4.0.0
 - StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]


###  accounting-lib 5.0.0
Tagged commit: _[43a65b5ffe86bfd608bc3c9db5de88391272a0e5](https://code-repo.d4science.org/gCubeSystem/accounting-lib/commit/43a65b5ffe86bfd608bc3c9db5de88391272a0e5)_



- Removed old Providers 
- Added compatibility between smartgears 3 and smartgears 4 component



###  common-generic-clients 2.0.0
Tagged commit: _[c0527778aebd2b7b99190214c3b7ccad84c07b2c](https://code-repo.d4science.org/gCubeSystem/common-generic-clients/commit/c0527778aebd2b7b99190214c3b7ccad84c07b2c)_



- Porting to new Secret Manager


###  common-smartgears 4.0.0
Tagged commit: _[73e4cc4cbbdec43cb3875d8e68865950b2e2e937](https://code-repo.d4science.org/gCubeSystem/common-smartgears/commit/73e4cc4cbbdec43cb3875d8e68865950b2e2e937)_



- support ticket [#28304]
- porting to keycloak
- moved to jakarta and servlet6
- added token expiration 



###  social-service-client 2.1.0
Tagged commit: _[d3a9899ec14930a2135c53e6e516915dd5ebeba4](https://code-repo.d4science.org/gCubeSystem/social-service-client/commit/d3a9899ec14930a2135c53e6e516915dd5ebeba4)_



- [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]
- moved to gcube-smartgears-bom 2.5.1



###  authorization-control-library 2.0.0
Tagged commit: _[82c5d5e25d2a26cba254f36793a7cff0a2f26459](https://code-repo.d4science.org/gCubeSystem/authorization-control-library/commit/82c5d5e25d2a26cba254f36793a7cff0a2f26459)_



- Moved to smartgears 4.0.0


###  storagehub-client-library 2.0.0
Tagged commit: _[0a9f9bdb9ecbdd555f773bc4a2b30d14311dd945](https://code-repo.d4science.org/gCubeSystem/storagehub-client-library/commit/0a9f9bdb9ecbdd555f773bc4a2b30d14311dd945)_



- Porting to model 2.0


###  storagehub-script-utils 2.0.0
Tagged commit: _[f462702a0762ef8c8c5d87ab562e95bf3a920dd9](https://code-repo.d4science.org/gCubeSystem/storagehub-script-utils/commit/f462702a0762ef8c8c5d87ab562e95bf3a920dd9)_



- Added new methods


###  storage-manager-core 4.1.0
Tagged commit: _[c0f8ac30b499d027ae9c120e14534f5f21322d81](https://code-repo.d4science.org/gCubeSystem/storage-manager-core/commit/c0f8ac30b499d027ae9c120e14534f5f21322d81)_


- Compatible with s3 storage backend
- Remove retry mechanism to exist method
- Add try/catch on exist method [#24215]
- Update to gcube-bom 4


###  authorization-client-legacy-jdk8 2.0.8
Tagged commit: _[16b2092d6e895c5e699f9a2d3484b56fb253dfd8](https://code-repo.d4science.org/gCubeSystem/authorization-client-legacy-jdk8/commit/16b2092d6e895c5e699f9a2d3484b56fb253dfd8)_



- Updated to gcube-bom 2.4.1


###  dataminer 1.9.1
Tagged commit: _[c2bf4e3d2ced8c95c3508c4139ee0d7ea99afaaa](https://code-repo.d4science.org/gCubeSystem/dataminer/commit/c2bf4e3d2ced8c95c3508c4139ee0d7ea99afaaa)_



- Updated gcube-bom to 2.4.1



###  storagehub-application-persistence 3.5.0
Tagged commit: _[16821344ac194180e633f43e45457fc275679016](https://code-repo.d4science.org/gCubeSystem/storagehub-application-persistence/commit/16821344ac194180e633f43e45457fc275679016)_



- Enhanced gcube-bom version to 3.4.1 [#27999]
- Updated maven-parent version to 1.2.0 



###  storage-manager-wrapper 4.0.0
Tagged commit: _[ae9bc3ba23aa40fcc5e609fe9c06b9abbdcb9624](https://code-repo.d4science.org/gCubeSystem/storage-manager-wrapper/commit/ae9bc3ba23aa40fcc5e609fe9c06b9abbdcb9624)_


- Removed slf4j implementations, added slf4j-simple;
- Discovering serviceEndpoint with category Storage
- Moved version from 3.1.0 to 4.0.0
- Upgrade to gcube-bom 3 and ScopeProvider removed
- Removed environment variable scope
- Passed to jdk11 and gcube-bom 4.0


###  storagehub-client-wrapper 1.2.2
Tagged commit: _[ff0fe9005b3031fcc7487ea745adb0ba785b885f](https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper/commit/ff0fe9005b3031fcc7487ea745adb0ba785b885f)_


- Updated  and  methods. They include the  parameter required by SHUB.2.X [#27898]
- Moved to [#28026] 


###  common-smartgears-app 4.0.0
Tagged commit: _[c11ab4cce733bae9a74336f0e021187be016b2fb](https://code-repo.d4science.org/gCubeSystem/common-smartgears-app/commit/c11ab4cce733bae9a74336f0e021187be016b2fb)_

 

- Moved to jakarta and servelt6


###  legacy-is-publisher-connector 1.0.0
Tagged commit: _[baebcc043dadeae7d56764f297c164e530bc87fa](https://code-repo.d4science.org/gCubeSystem/legacy-is-publisher-connector/commit/baebcc043dadeae7d56764f297c164e530bc87fa)_

 

- First Release


###  catalogue-core 1.0.2
Tagged commit: _[15ff7f14df541485a6c3a50bfb0531c1950aad37](https://code-repo.d4science.org/gCubeSystem/catalogue-core/commit/15ff7f14df541485a6c3a50bfb0531c1950aad37)_



- Enhanced gcube-smartgears-bom version to 2.5.1 [#27999]



###  uri-resolver-manager 1.8.0
Tagged commit: _[79ac65f18b5d2349e02232252a8182a37ac14a63](https://code-repo.d4science.org/gCubeSystem/uri-resolver-manager/commit/79ac65f18b5d2349e02232252a8182a37ac14a63)_



- Enhanced the Geoportal-Resolver. Supported share links to Geoportal Data-Entry app [#27160]
- Provided the new endpoint: GeoportalExporter [#27308]


###  geoportal-client 1.2.2
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_

Tags 1.2.2 / 6.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite

###  oidc-library-portal 1.4.0
Tagged commit: _[96b2489fbe1640c4787da6919fd6be3e65043b5a](https://code-repo.d4science.org/gCubeSystem/oidc-library-portal/commit/96b2489fbe1640c4787da6919fd6be3e65043b5a)_


- New handling for missing refresh token and refresh token expired in OIDC token when refreshing it.
- Changed OIDC related exceptions: the cause exception can be an OpenIdConnectRESTHelperException only.
- Changed OIDC/UMA refreshing errors log to DEBUG to avoid logs flood (#27536)


###  catalogue-util-library 1.4.1
Tagged commit: _[db836e071f06a110023d16ae6d3edef76977b941](https://code-repo.d4science.org/gCubeSystem/catalogue-util-library/commit/db836e071f06a110023d16ae6d3edef76977b941)_



- Exposed the methods required by uri-resolver [#27424]


###  geoportal-data-common 2.4.0
Tagged commit: _[f447964e79468d9b8a27f79fa67c7842c87a719d](https://code-repo.d4science.org/gCubeSystem/geoportal-data-common/commit/f447964e79468d9b8a27f79fa67c7842c87a719d)_



- Search Filter for profileId and projectID [#27120]
- Integrated new Uri-Resolver-Manager [#27160]
- Added new operation Share project
- Added optional message when performing lifecycle step [#27192]


###  workspace-uploader 2.1.1
Tagged commit: _[97e58fcf91a48f38e6f009069f0c2526b0bc0001](https://code-repo.d4science.org/gCubeSystem/workspace-uploader/commit/97e58fcf91a48f38e6f009069f0c2526b0bc0001)_



- Updated  and  methods. They include the  parameter [#27898]
- Updated the data structure to handle asynchronous uploading of multiple files [#27934]


###  workspace-explorer 2.2.1
Tagged commit: _[574de3ff6953a2ae9600aaff66402cd3399a792c](https://code-repo.d4science.org/gCubeSystem/workspace-explorer/commit/574de3ff6953a2ae9600aaff66402cd3399a792c)_



- Fixing typo  [#28452]
- Ported to SHUB 2.X


###  data-miner-manager-cl 1.11.0
Tagged commit: _[7be4d74f98edea3e0934d4f1854c016d1c0dc7c8](https://code-repo.d4science.org/gCubeSystem/data-miner-manager-cl/commit/7be4d74f98edea3e0934d4f1854c016d1c0dc7c8)_



 - Updated maven-parent 1.2.0
 - Updated gcube-bom 2.4.1




###  oidc-enrollment-hook 1.2.1
Tagged commit: _[cd6598688e51569f4c4776b9e706c6f81d50e83d](https://code-repo.d4science.org/gCubeSystem/oidc-enrollment-hook/commit/cd6598688e51569f4c4776b9e706c6f81d50e83d)_


- Better handling of tokens remove from proxy in the  statement in the  class
- Avatar for the user at login is now downloaded and saved/removed in a separate thread
- Redirect after the login to the original requested page is performed when response has not already commited due to redirect to another URI due to tokens expiration or errors


###  user-registration-hook 2.1.0
Tagged commit: _[349db5106902440e61621ef82548d22253510bae](https://code-repo.d4science.org/gCubeSystem/user-registration-hook/commit/349db5106902440e61621ef82548d22253510bae)_



- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]


###  VREFolder-hook 6.8.4
Tagged commit: _[302db98dcf49b0187fd1cfd43fef65905578b65e](https://code-repo.d4science.org/gCubeSystem/VREFolder-hook/commit/302db98dcf49b0187fd1cfd43fef65905578b65e)_



- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]


###  workspace-sharing-widget 1.12.0
Tagged commit: _[5eb595ff9afb46d98f191919caf097f80c5ca107](https://code-repo.d4science.org/gCubeSystem/workspace-sharing-widget/commit/5eb595ff9afb46d98f191919caf097f80c5ca107)_



- Moved to maven-portal-bom 4.0.0


###  data-miner-manager-widget 1.6.0
Tagged commit: _[710e315afc83cedababe67164b2e604e10700d9f](https://code-repo.d4science.org/gCubeSystem/data-miner-manager-widget/commit/710e315afc83cedababe67164b2e604e10700d9f)_

 

- Updated to maven-portal-bom-4.0.0



###  geoportal-data-mapper 1.1.0
Tagged commit: _[3a742e780fcbdeb000e8112caa864c60baab3d55](https://code-repo.d4science.org/gCubeSystem/geoportal-data-mapper/commit/3a742e780fcbdeb000e8112caa864c60baab3d55)_



- Added Export as PDF facility [#26026]


###  geoportal-common 1.1.1
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_

Tags 1.1.1 / 6.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  cms-plugin-framework 1.0.6
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_



- Integrated an EventManager centrally [#26321]
- sdi-plugins: add the logic to apply a regex to the value that must be added to index [#26322]
- notifications-plugins: integrated [#26453]
- catalogue-binding-plugins: integrated [#26454]
- Moved to  [#28026]


###  default-lc-managers 1.3.0
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_

Tags 1.3.0 / 6.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  cms-test-commons 1.0.5
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_



- Added maven profiles  and  [#25570]
- Moved to maven-parent.v1.2.0 [#25570]


###  concessioni-lifecycle 1.1.2
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_

Tags 1.1.2 / 6.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  sdi-plugins 1.1.3
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_

Tags 1.1.3 / 6.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  geoportal-service 1.2.2
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_

Tags 1.2.2 / 6.0.0 not found in CHANGELOG.md at https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite
###  catalogue-binding-plugins 1.0.0
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_


- First release


###  notifications-plugins 1.0.5
Tagged commit: _[bbfd828909efffe134a5b23b059c3303c21603c5](https://code-repo.d4science.org/gCubeSystem/gcube-cms-suite/commit/bbfd828909efffe134a5b23b059c3303c21603c5)_



- Added maven profiles  and  [#25570]
- Moved to maven-parent.v1.2.0 [#25570]


###  dataminer-algorithms-importer 1.3.0
Tagged commit: _[da8be4930624a046936edb2bc0f6d30fa736b1de](https://code-repo.d4science.org/gCubeSystem/dataminer-algorithms-importer/commit/da8be4930624a046936edb2bc0f6d30fa736b1de)_



- Updated maven-parent-1.2.0
- Updated to dataminer-1.9.1



###  accounting-aggregator-se-plugin 2.1.0
Tagged commit: _[ae9246d4aafd42d43ab5043a0cb4b54399040084](https://code-repo.d4science.org/gCubeSystem/accounting-aggregator-se-plugin/commit/ae9246d4aafd42d43ab5043a0cb4b54399040084)_



- Upgraded smart-executor-bom to 3.3.0 [#27999]
- Updated maven-parent version to 1.2.0



###  accounting-dashboard-harvester-se-plugin 2.5.0
Tagged commit: _[c138a3fbd286ee9caa35e669d9cc075424c5bd34](https://code-repo.d4science.org/gCubeSystem/accounting-dashboard-harvester-se-plugin/commit/c138a3fbd286ee9caa35e669d9cc075424c5bd34)_



- Upgraded smart-executor-bom to 3.3.0 [#27999]



###  whn-manager 3.0.0
Tagged commit: _[43c5642cc94b378b4277dcab2a2c0707711417ba](https://code-repo.d4science.org/gCubeSystem/whn-manager/commit/43c5642cc94b378b4277dcab2a2c0707711417ba)_



- Porting to smartgears 4


###  grsf-publisher 1.2.0
Tagged commit: _[005791f3c1a8a0c3b8933a7a42c8aa07541821ba](https://code-repo.d4science.org/gCubeSystem/grsf-publisher/commit/005791f3c1a8a0c3b8933a7a42c8aa07541821ba)_



- Enhanced gcube-smartgears-bom version to 2.5.1 [#27999]



###  gcat 2.6.0
Tagged commit: _[2207e87a09696c6c7d04637cd6cb724d0cc658c1](https://code-repo.d4science.org/gCubeSystem/gcat/commit/2207e87a09696c6c7d04637cd6cb724d0cc658c1)_



- Enhanced gcube-smartgears-bom version to 2.5.1 [#27999]



###  uri-resolver 2.10.0
Tagged commit: _[eb41add25e47e81756784800bea686d112817ee0](https://code-repo.d4science.org/gCubeSystem/uri-resolver/commit/eb41add25e47e81756784800bea686d112817ee0)_



- GeoPortal-Resolver enhancement: implemented share link towards Geoportal Data-Entry facility [#27135]
- Added Geoportal Exporter as PDF [#27274]
- Migrated to  [#27423]
- Included new SHUB client library [#27944]
- Moved to  [#28026]
- Bug fixing StorageID resolver [#28276]
- Added HealthCheckResponse via  (see https://microprofile.io/specifications/microprofile-health/)


###  storagehub 2.0.0
Tagged commit: _[1b724a8d8ba5577eea689aa5af3ab7f8a3c5f83b](https://code-repo.d4science.org/gCubeSystem/storagehub/commit/1b724a8d8ba5577eea689aa5af3ab7f8a3c5f83b)_



 - Ceph as default storage
 - Vre folders can define specific bucket as backend
 - Enunciate docs
 - Dockerization of the service 


###  social-networking-library-ws 3.1.0
Tagged commit: _[a944514c8f81647eb9c841fde6af64a2afd98aea](https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/commit/a944514c8f81647eb9c841fde6af64a2afd98aea)_



- gcube-smartgears-bom to 2.5.1



###  wps 1.5.0
Tagged commit: _[7de237d2900bc98aced729afde04b953c9a11c5d](https://code-repo.d4science.org/gCubeSystem/wps/commit/7de237d2900bc98aced729afde04b953c9a11c5d)_



- Updated gcube-smartgears-bom to 2.5.1



###  workspace 6.30.2
Tagged commit: _[dbd88fdfda888ea3d72a99ab4aaa58a976688967](https://code-repo.d4science.org/gCubeSystem/workspace/commit/dbd88fdfda888ea3d72a99ab4aaa58a976688967)_



- Included upload of files/archives with  parameter [#27898]
- Moved to  4.0.0 [#28026]


###  accounting-manager 1.16.0
Tagged commit: _[c4c4313e31f40c63208d5781d70aac284f618031](https://code-repo.d4science.org/gCubeSystem/accounting-manager/commit/c4c4313e31f40c63208d5781d70aac284f618031)_



-  Updated to maven-portal-bom 4.0.0



###  data-miner-manager 1.14.0
Tagged commit: _[cca951041b2d5c2934d955c371e927c9f2d5768e](https://code-repo.d4science.org/gCubeSystem/data-miner-manager/commit/cca951041b2d5c2934d955c371e927c9f2d5768e)_



 - Updated maven-portal-bom to 4.0.0





###  data-miner-executor 1.4.0
Tagged commit: _[7b89a9a218a67796317478ee121f924cd5e1d50e](https://code-repo.d4science.org/gCubeSystem/data-miner-executor/commit/7b89a9a218a67796317478ee121f924cd5e1d50e)_



- Updated to maven-portal-bom-4.0.0
- Added support to default value for ListParameter [#24026]



###  statistical-algorithms-importer 1.16.0
Tagged commit: _[c6af5498d41c8465efb7d7eea0188791aa2a9b80](https://code-repo.d4science.org/gCubeSystem/statistical-algorithms-importer/commit/c6af5498d41c8465efb7d7eea0188791aa2a9b80)_



 - Updated to maven-portal-bom-4.0.0 
 



###  workspace-widget-portlet 1.5.3
Tagged commit: _[2e23abaa1ae9d2cd8ba1120c7d424f01cbb0bfe3](https://code-repo.d4science.org/gCubeSystem/workspace-widget-portlet/commit/2e23abaa1ae9d2cd8ba1120c7d424f01cbb0bfe3)_



- maven parent 1.2.0
- updated to shub 2


###  share-updates 3.0.0
Tagged commit: _[fb82c27ccdf30a8dcf20a1d5a6333b92465d5770](https://code-repo.d4science.org/gCubeSystem/share-updates/commit/fb82c27ccdf30a8dcf20a1d5a6333b92465d5770)_



- removed cassandra 2 client
- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]


###  vre-definition 5.3.0
Tagged commit: _[b608974b54545ea16cfd4b0b4269e8ffa8c86e0a](https://code-repo.d4science.org/gCubeSystem/vre-definition/commit/b608974b54545ea16cfd4b0b4269e8ffa8c86e0a)_



- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]


###  vre-deploy 4.6.0
Tagged commit: _[754f3ef2aed68a6bb683699f572f353f0308b877](https://code-repo.d4science.org/gCubeSystem/vre-deploy/commit/754f3ef2aed68a6bb683699f572f353f0308b877)_



- maven-parent 1.2.0
- maven-portal-bom 4.0.0


###  geoportal-data-viewer-app 3.7.0
Tagged commit: _[6532754ef1e8485ab655aab67f6e266ac13d1c4a](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/commit/6532754ef1e8485ab655aab67f6e266ac13d1c4a)_



- Provided the Export as PDF facility [#26026]
- Integrated the GeoportalExporter as service [#27321]
- Passed to the new KC-client [#27398]


###  gcube-ckan-datacatalog 2.3.4
Tagged commit: _[dd00a97802f48fe19eac22340db95a33a02040ef](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/commit/dd00a97802f48fe19eac22340db95a33a02040ef)_



- Moved to maven-portal-bom 4.0.0 [#28026]


###  sbd-uploadshare-portlet 1.3.0
Tagged commit: _[7c7ebb616b4619ca2a8331c5499fc22e090d0e5f](https://code-repo.d4science.org/gCubeSystem/sbd-uploadshare-portlet/commit/7c7ebb616b4619ca2a8331c5499fc22e090d0e5f)_



- maven-portal-bom 4.0.0
- maven-parent 1.2.0
- replaced social-networking-library with social-library-stubs


###  geoportal-data-entry-app 3.3.0
Tagged commit: _[b28647ec784f7e83e406fdcfa8c0c55b6f564263](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/commit/b28647ec784f7e83e406fdcfa8c0c55b6f564263)_



- Implemented the init facility to resolve a public link on an item [#27120]
- Integrated new Uri-Resolver-Manager [#27160]
- Added Get Shareable Link facility [#27120]
- Added optional message when performing lifecycle step [#27192]
- Enforced deleteProject method/UX
- The save operation is now monitored asynchronously [#28268]
- Integrated in the report UI the link of project just saved


###  messages 2.6.1
Tagged commit: _[3573be713dedfd735437853747743f6c3a6b2baa](https://code-repo.d4science.org/gCubeSystem/messages/commit/3573be713dedfd735437853747743f6c3a6b2baa)_


- maven-portal-bom 4.0.0



###  create-users-portlet 3.1.0
Tagged commit: _[643b9a25cfcba30e010d83676841d5ff56dbd683](https://code-repo.d4science.org/gCubeSystem/create-users-portlet/commit/643b9a25cfcba30e010d83676841d5ff56dbd683)_

 

- Updated storage hub dependency
- Removed social-networking-library
- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]


###  news-feed 3.1.0
Tagged commit: _[5599bdb83ad9f9b7e833cf48d34aefaa1f94fa2c](https://code-repo.d4science.org/gCubeSystem/news-feed/commit/5599bdb83ad9f9b7e833cf48d34aefaa1f94fa2c)_



- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]
- maven-parent 1.2.0
- maven-portal-bom 4.0.0


###  register-vre-users 2.4.0
Tagged commit: _[0ba7ffa9ef80bf683ff9eab057480df703c9fb91](https://code-repo.d4science.org/gCubeSystem/register-vre-users/commit/0ba7ffa9ef80bf683ff9eab057480df703c9fb91)_



- maven-portal-bom 4.0.0
- maven-parent 1.2.0


###  smartgears-distribution 4.0.0
Tagged commit: _[d27adf217c1d5f430187a77201aabe490816c1da](https://code-repo.d4science.org/gCubeSystem/smartgears-distribution/commit/d27adf217c1d5f430187a77201aabe490816c1da)_



- Moving to smartgears-4.0.0


###  gcube-portal-bundle 5.4.0
Tagged commit: _[9dee21d6edbdb28027c1efa614010ede53816839](https://code-repo.d4science.org/gCubeSystem/gcube-portal-bundle/commit/9dee21d6edbdb28027c1efa614010ede53816839)_



- updated pom, maven parent and BOM
- authorization-client-legacy-jdk8 instead of authorization-client (fix patch problem)



---
*generated by the gCube-ReleaseNotes pipeline from report 1796*

*last update Mon Dec 02 19:09:56 CET 2024*
